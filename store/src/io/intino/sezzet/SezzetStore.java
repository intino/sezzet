package io.intino.sezzet;

import io.intino.konos.TripleStore;
import io.intino.sezzet.model.graph.InstantIterator;
import io.intino.sezzet.model.graph.rules.Scale;
import io.intino.sezzet.operators.FileReader;
import io.intino.sezzet.operators.LongStream;
import io.intino.sezzet.operators.SetStream;
import io.intino.sezzet.operators.Union;
import io.intino.sezzet.session.SessionFileWriter;
import io.intino.sezzet.session.SessionSealer;
import sun.misc.IOUtils;

import java.io.*;
import java.nio.file.Files;
import java.time.Instant;
import java.util.*;
import java.util.Set;
import java.util.stream.Collectors;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class SezzetStore implements SetStore {

	public static final String SessionExt = ".session";
	public static final String SegmentExt = ".segment";
	public static final String InfoExt = ".info";
	public static final String SetExt = ".set";
	public static final String TempExt = ".temp";
	public static final String PartExt = ".part";
	private static final int MAX_ENTRIES = 1000;

	private final File store;
	private Scale scale;
	private Map<String, TripleStore> variables = new LinkedHashMap<String, TripleStore>(MAX_ENTRIES + 1, .75F, true) {
		public boolean removeEldestEntry(Map.Entry eldest) {
			return size() > MAX_ENTRIES;
		}
	};

	public SezzetStore(File store, Scale scale) {
		this.store = store;
		this.scale = scale;
	}

	public static void write(SetStream stream, File file) throws IOException {
		file.getParentFile().mkdirs();
		DataOutputStream dataStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
		while (stream.hasNext()) dataStream.writeLong(stream.next());
		dataStream.close();
	}

	@Override
	public Scale scale() {
		return scale;
	}

	@Override
	public SessionFileWriter createSession(Instant instant) {
		return new SessionFileWriter(sessionFile(instant), instant, false);
	}

	@Override
	public void seal() {
		SessionSealer.seal(stageFolder());
	}

	@Override
	public File storeSegment(Instant instant, String segment, SetStream stream) {
		try {
			String instantTag = Instant.now().toString().substring(0, 19).replace(":", "").replace("-", "");
			File file = new File(segmentFolder(), instantTag + "/" + segment + SegmentExt);
			write(stream, file);
			return file;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void append(String tank, String set, Instant instant, long... ids) {
		try {
			File file = fileOf(tank, set, instant);
			SetStream toWrite = file.exists() ? new Union(asList(new FileReader(file), new LongStream(ids))) : new LongStream(ids);
			File tempFile = new File(file + TempExt);
			write(toWrite, tempFile);
			Files.move(tempFile.toPath(), file.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<String> tanks() {
		String[] tanks = store.list((dir, name) -> !name.equals("stage"));
		return tanks != null ? asList(tanks) : emptyList();
	}

	@Override
	public List<String> setsOf(String tank, Instant instant) {
		String[] files = folderOf(tank, instant).list((f, n) -> n.endsWith(SetExt));
		return files != null ? stream(files).sorted().map(f -> f.replace(SetExt, "")).collect(toList()) : emptyList();
	}

	@Override
	public List<String> setsOf(String tank, Instant from, Instant to) {
		Set<String> sets = new LinkedHashSet<>();
		for (Instant instant : new InstantIterator(from, to, scale))
			sets.addAll(setsOf(tank, instant));
		return new ArrayList<>(sets);
	}

	@Override
	public List<String> setsOf(String tank, String regex, Instant instant) {
		return setsOf(tank, instant).stream()
				.filter(v -> matches(v, regex))
				.collect(toList());
	}

	@Override
	public List<String> setsOf(String tank, String regex, Instant from, Instant to) {
		return setsOf(tank, from, to).stream()
				.filter(v -> matches(v, regex))
				.collect(toList());
	}

	@Override
	public List<String> setsOf(String tank, double lowBound, double highBound, Instant instant) {
		return setsOf(tank, instant).stream()
				.filter(v -> isInRange(v, lowBound, highBound))
				.collect(toList());
	}

	@Override
	public List<String> setsOf(String tank, double lowBound, double highBound, Instant from, Instant to) {
		return setsOf(tank, from, to).stream()
				.filter(v -> isInRange(v, lowBound, highBound))
				.collect(toList());
	}

	@Override
	public File folderOf(String tank, Instant instant) {
		return new File(store, tank + "/" + scale.tag(instant));
	}

	@Override
	public List<File> foldersOf(String tank, Instant from, Instant to) {
		List<File> folders = new ArrayList<>();
		for (Instant instant : new InstantIterator(from, to, scale))
			folders.add(folderOf(tank, instant));
		return folders;
	}

	@Override
	public File fileOf(String tank, String set, Instant instant) {
		return new File(folderOf(tank, instant), set + SetExt);
	}

	@Override
	public List<File> filesOf(String tank, String set, Instant from, Instant to) {
		List<File> files = new ArrayList<>();
		for (Instant instant : new InstantIterator(from, to, scale))
			files.add(fileOf(tank, set, instant));
		return files;
	}

	@Override
	public String valueOf(String tank, String set, String variable, Instant instant) {
		TripleStore tripleStore = getVariablesOf(tank, instant);
		if (tripleStore == null) return null;
		List<String[]> list = tripleStore.matches(set, variable).collect(Collectors.toList());
		return !list.isEmpty() ? list.get(0)[2] : null;
	}

	@Override
	public void append(String tank, String set, String variable, String value, Instant instant) {
		TripleStore tripleStore = getVariablesOf(tank, instant);
		if (tripleStore != null) tripleStore.put(set, variable, value);
		File file = new File(folderOf(tank, instant), tank + InfoExt);
		file.getParentFile().mkdirs();
		try {
			Files.write(file.toPath(), (set + ";" + variable + ";" + value + "\n").getBytes(), APPEND, CREATE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void append(String tank, Instant instant, InputStream stream) {
		File file = new File(folderOf(tank, instant), tank + InfoExt);
		file.getParentFile().mkdirs();
		try {
			Files.write(file.toPath(), IOUtils.readFully(stream, -1, true), APPEND, CREATE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		variables.remove(tank + scale.tag(instant));
	}

	private TripleStore getVariablesOf(String tank, Instant instant) {
		String id = tank + scale.tag(instant);
		if (!variables.containsKey(id)) {
			File file = new File(folderOf(tank, instant), tank + InfoExt);
			if (!file.exists()) return null;
			variables.put(id, new TripleStore(file));
		}
		return variables.get(id);
	}

	private File sessionFile(Instant instant) {
		int count = -1;
		while (true) {
			File result = new File(stageFolder(), scale.tag(instant) + PartExt + ++count);
			if (!new File(result + TempExt).exists() && !new File(result + SessionExt).exists())
				return new File(result + TempExt);
		}
	}

	private boolean isInRange(String set, double lowBound, double highBound) {
		try {
			double aDouble = Double.parseDouble(set);
			return aDouble >= lowBound && aDouble <= highBound;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private boolean matches(String set, String regex) {
		return set.matches(regex.replace("*", ".*"));
	}

	private File segmentFolder() {
		return new File(store, "segments/");
	}

	private File stageFolder() {
		return new File(store, "stage/");
	}
}
