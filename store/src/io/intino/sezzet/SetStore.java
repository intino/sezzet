package io.intino.sezzet;

import io.intino.sezzet.model.graph.rules.Scale;
import io.intino.sezzet.operators.SetStream;
import io.intino.sezzet.session.SessionFileWriter;

import java.io.File;
import java.io.InputStream;
import java.time.Instant;
import java.util.List;

public interface SetStore {

	Scale scale();

	SessionFileWriter createSession(Instant instant);

	void seal();

	File storeSegment(Instant instant, String segment, SetStream stream);

	void append(String tank, String set, Instant instant, long... ids);

	List<String> tanks();

	List<String> setsOf(String tank, Instant instant);

	List<String> setsOf(String tank, Instant from, Instant to);

	List<String> setsOf(String tank, String regex, Instant instant);

	List<String> setsOf(String tank, String regex, Instant from, Instant to);

	List<String> setsOf(String tank, double lowBound, double highBound, Instant instant);

	List<String> setsOf(String tank, double lowBound, double highBound, Instant from, Instant to);

	File folderOf(String tank, Instant instant);

	List<File> foldersOf(String tank, Instant from, Instant to);

	File fileOf(String tank, String set, Instant instant);

	List<File> filesOf(String tank, String set, Instant from, Instant to);

	String valueOf(String tank, String set, String variable, Instant instant);

	void append(String tank, String set, String variable, String value, Instant instant);

	void append(String tank, Instant instant, InputStream stream);

}
