package io.intino.sezzet.editor.box.actions;

import io.intino.sezzet.editor.box.displays.*;

public class HomeAction extends AbstractHomeAction {
	public io.intino.alexandria.ui.displays.Soul prepareSoul(io.intino.alexandria.ui.services.push.UIClient client) {
	    return new io.intino.alexandria.ui.displays.Soul(session) {
			@Override
			public void personify() {
				SezzetEditorHome component = new SezzetEditorHome(box);
				register(component);
				component.personify();
			}
		};
	}
}