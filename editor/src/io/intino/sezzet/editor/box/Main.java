package io.intino.sezzet.editor.box;

import io.intino.alexandria.core.Box;

public class Main {
	public static void main(String[] args) {
		Box box = new EditorBox(args);
		Runtime.getRuntime().addShutdownHook(new Thread(box::close));
	}
}