package io.intino.sezzet.editor.box.displays;

import io.intino.sezzet.model.graph.Category;
import io.intino.sezzet.model.graph.Feature;
import io.intino.sezzet.model.graph.Group;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.MessageProvider;
import io.intino.sezzet.setql.graph.rules.Operator;
import io.intino.tara.magritte.Layer;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

class CodeSuggester {
	private final SezzetGraph sezzetGraph;
	private final List<Feature> features;
	private final Locale locale;

	CodeSuggester(SezzetGraph sezzetGraph, List<Feature> features, Locale locale) {
		this.sezzetGraph = sezzetGraph;
		this.features = features;
		this.locale = locale;
	}

	List<String> suggest(String line, int cursor) {
		List<String> options = new ArrayList<>();
		if (isInArgumentContext(line, cursor)) {
			options = arguments(featureIn(line, cursor).toLowerCase(), lastCharacter(line, cursor));
		} else if (startsWithOperator(line) && !line.contains("::") || line.indexOf("::") > cursor)
			options = features(line, cursor);
		else if (line.contains("::")) {
			final String temporal = line.substring(line.indexOf("::"));
			if (!temporal.contains(textFromKey("period"))) options.add(textFromKey("period"));
			if (!temporal.contains(textFromKey("recency"))) options.add(textFromKey("recency"));
			if (!temporal.contains(textFromKey("frequency"))) options.add(textFromKey("frequency"));
		}
		return options;
	}

	private String lastCharacter(String line, int cursor) {
		String trim = line.substring(0, cursor).trim();
		return String.valueOf(trim.charAt(trim.length() - 1));
	}

	private List<String> features(String line, int cursor) {
		String feature = featureIn(line, cursor);
		String prefix = feature.contains(".") ? feature.substring(0, feature.lastIndexOf(".")) : feature;
		String suffix = feature.contains(".") ? feature.substring(feature.lastIndexOf(".") + 1) : feature;
		String[] names = prefix.split("\\.");
		if (names.length == 1) {
			return names[0].equals(sezzetGraph.subject().value()) ?
					map(sezzetGraph.groupList(), ".", suffix) :
					singletonList(sezzetGraph.subject().value() + ".");
		}
		Group group = findGroup(Arrays.copyOfRange(names, 1, names.length));
		if (group == null) return Collections.emptyList();
		List<String> values = map(group.groupList(), ".", suffix);
		values.addAll(map(group.featureList(), "", suffix));
		return values;
	}

	private Group findGroup(String[] names) {
		Group currentGroup = null;
		for (String name : names) {
			if (currentGroup == null) {
				currentGroup = findGroup(sezzetGraph.groupList(), name);
				if (currentGroup == null) return null;
			} else currentGroup = findGroup(currentGroup.groupList(), name);
		}
		return currentGroup;
	}

	private List<String> map(List<? extends Layer> layers, String prefix, String suffix) {
		return layers.stream().map(g -> g.name$() + prefix).filter(g2 -> g2.startsWith(suffix)).collect(Collectors.toList());
	}

	private Group findGroup(List<Group> groupList, String name) {
		for (Group g : groupList) if (g.name$().equals(name)) return g;
		return null;
	}

	private String textFromKey(String key) {
		return MessageProvider.message(locale, key);
	}

	private boolean isInArgumentContext(String line, int cursor) {
		int rightParenthesis = line.indexOf(")", cursor);
		int leftParenthesis = line.indexOf("(");
		return leftParenthesis > 0 && cursor > leftParenthesis && (cursor <= rightParenthesis || !line.contains(")"));
	}

	private List<String> arguments(String featureName, String anchor) {
		if (!anchor.equals("(") && !anchor.equals(",")) return Collections.emptyList();
		Feature feature = features.stream().filter(f -> f.label().equalsIgnoreCase(featureName)).findFirst().orElse(null);
		if (feature == null) return Collections.emptyList();
		List<String> arguments = new ArrayList<>();
		if (feature.isEnumerate())
			arguments.addAll(sezzetGraph.valuesOf(feature).stream().map(Category::label).collect(toList()));
		else if (feature.isText())
			arguments.addAll(feature.asText().suggestions().stream().map(s -> "\"" + s + "\"").collect(toList()));
		else if (feature.isNumeric()) arguments.addAll(feature.asNumeric().suggestions());
		if (feature.isAllowEmpty()) arguments.add(textFromKey("undefined"));
		if (feature.isAllowNaS()) arguments.add(textFromKey("invalid"));
		return arguments;
	}

	private String featureIn(String line, int cursor) {
		String text = line.indexOf(")", cursor) > 0 ? line.substring(0, line.indexOf(")", cursor)) : line;
		text = !text.contains("(") ? text : text.substring(0, text.lastIndexOf("("));
		if (text.contains(" ")) text = text.substring(text.lastIndexOf(" ") + 1);
		return text.substring(text.lastIndexOf("\t") + 1);
	}

	private boolean startsWithOperator(String line) {
		char start = line.trim().isEmpty() ? 0 : line.trim().charAt(0);
		return start != 0 && (start == '*' || Operator.fromText(start + "") != null) && hasIndent(line);
	}

	private boolean hasIndent(String line) {
		return (line.trim().length() > 1 && line.trim().charAt(1) == '\t') || (line.length() == 2 && line.charAt(1) == '\t');
	}
}
