package io.intino.sezzet.editor.box.displays;

import io.intino.alexandria.ui.displays.AlexandriaStamp;
import io.intino.sezzet.editor.box.SezzetExpressionValidator;
import io.intino.sezzet.editor.box.displays.notifiers.SezzetEditorNotifier;
import io.intino.sezzet.editor.box.schemas.ChangeParameters;
import io.intino.sezzet.editor.box.schemas.Setup;
import io.intino.sezzet.editor.box.schemas.SuggestParameters;
import io.intino.sezzet.editor.box.schemas.ValidationItem;
import io.intino.sezzet.model.graph.Feature;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.MessageProvider;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.tara.magritte.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class SezzetEditor extends AlexandriaStamp<SezzetEditorNotifier> {
	private CodeSuggester codeSuggester;
	private List<Consumer<ChangeContext>> listeners = new ArrayList<>();
	private SetqlGraph setql;
	private SezzetGraph sezzetGraph;
	private final List<Feature> features;
	private String expression;
	private boolean readonly = false;
	private boolean isExpressionValid = false;
	private Locale locale = null;

	public SezzetEditor(String dsl) {
		this.setql = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		this.sezzetGraph = new Graph().loadStashes("Sezzet", dsl).as(SezzetGraph.class);
		this.features = sezzetGraph.allFeatures();
	}

	public String expression() {
		return this.expression;
	}

	public SezzetEditor expression(String expression) {
		this.expression = expression;
		return this;
	}

	public SezzetEditor locale(Locale locale) {
		this.locale = locale;
		return this;
	}

	public SezzetEditor readonly(boolean value) {
		this.readonly = value;
		return this;
	}

	public SezzetEditor addListener(Consumer<ChangeContext> listener) {
		listeners.add(listener);
		return this;
	}

	@Override
	protected void init() {
		super.init();
		this.codeSuggester = new CodeSuggester(sezzetGraph, features, locale());
		notifier.render(new Setup().
				readonly(readonly).
				features(features()).
				constants(asList(textFromKey("period"), textFromKey("frequency"), textFromKey("recency"), textFromKey("consecutives"),
						textFromKey("commons"), textFromKey("uncommons"), textFromKey("old"), textFromKey("new"))));
	}

	@Override
	public void refresh() {
		notifier.setExpression(expression);
		validate();
	}

	public void onChange(ChangeParameters params) {
		this.expression = params.expression();
		validate();
		listeners.forEach(l -> l.accept(changeContext(params, graph())));
	}

	public boolean isValid() {
		return isExpressionValid;
	}

	private ChangeContext changeContext(ChangeParameters params, SetqlGraph graph) {
		String line = lineOf(params);
		return new ChangeContext() {
			public boolean existsOperator() {
				return !line.startsWith("*");
			}

			public String operand() {
				return line;
			}

			public Expression expression() {
				return graph == null ? null : graph.expression();
			}

			public String rawExpression() {
				return params.expression();
			}

			public int lineNumber() {
				return params.cursor().row();
			}
		};
	}

	private String lineOf(ChangeParameters parameters) {
		String[] split = parameters.expression().split("\n");
		int row = parameters.cursor().row();
		if (row >= split.length) return "";
		return split[row].trim();
	}

	private void validate() {
		SezzetExpressionValidator validator = new SezzetExpressionValidator(graph(), sezzetGraph);
		List<ValidationItem> validationItems = validator.validationItems(expression, locale());
		isExpressionValid = validationItems.isEmpty();
		notifier.validationResult(validationItems);
	}

	public void onSuggest(SuggestParameters params) {
		notifier.suggestion(codeSuggester.suggest(params.line(), params.cursor()));
	}

	private String textFromKey(String key) {
		return MessageProvider.message(locale(), key);
	}

	private Locale locale() {
		if (locale != null) return locale;
		String language = session().browser().languageFromMetadata();
		return language.equals("es") || language.equals("mx") ? new Locale("es", "ES") : Locale.ENGLISH;
	}

	private List<String> features() {
		return features.stream().map(Feature::label).collect(toList());
	}

	private SetqlGraph graph() {
		return setql.core$().clone().as(SetqlGraph.class);
	}

	public interface ChangeContext {
		boolean existsOperator();

		String operand();

		Expression expression();

		String rawExpression();

		int lineNumber();
	}
}