package io.intino.sezzet.editor.box.displays;

import io.intino.alexandria.ui.displays.AlexandriaDisplay;
import io.intino.alexandria.ui.displays.AlexandriaDisplayNotifier;
import io.intino.sezzet.editor.box.EditorBox;

import java.util.List;
import java.util.function.Consumer;

public abstract class SezzetDialog<N extends AlexandriaDisplayNotifier> extends AlexandriaDisplay<N> {
	protected EditorBox box;
	protected List<Consumer<Object>> changeListeners;

	public SezzetDialog(EditorBox box) {
		super();
		this.box = box;
	}

	public void onChange(Consumer<Object> listener) {
		changeListeners.add(listener);
	}

}