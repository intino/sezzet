package io.intino.sezzet.editor.box.displays;

import io.intino.alexandria.ui.services.push.UISession;
import io.intino.sezzet.editor.box.EditorBox;

public class SezzetEditorMold extends AbstractSezzetEditorMold {

	public SezzetEditorMold(EditorBox box) {
		super(box);
	}

	public static class Stamps {
		public static class Editor {
			public static io.intino.alexandria.ui.displays.AlexandriaStamp buildDisplay(EditorBox box, String name, UISession session) {
				SezzetEditor editor = new SezzetEditor("asemed");
				editor.expression("");
				return editor;
			}
		}
	}

	public static class Blocks {



	}
}