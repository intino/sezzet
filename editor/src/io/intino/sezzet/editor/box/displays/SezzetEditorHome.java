package io.intino.sezzet.editor.box.displays;

import io.intino.sezzet.editor.box.EditorBox;

public class SezzetEditorHome extends AbstractSezzetEditorHome {

	public SezzetEditorHome(EditorBox box) {
		super(box);
	}

	public static class Toolbar {

	}

	public static class Views {

	}
}