package io.intino.sezzet.editor.box;

import io.intino.alexandria.ui.services.AuthService;
import io.intino.alexandria.ui.services.EditorService;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class EditorBox extends AbstractBox {


	public EditorBox(String[] args) {
		super(args);
	}

	public EditorBox(EditorConfiguration configuration) {
		super(configuration);

	}

	@Override
	public io.intino.alexandria.core.Box put(Object o) {
		super.put(o);
		return this;
	}

	public io.intino.alexandria.core.Box open() {
		return super.open();
	}

	public void close() {
		super.close();
	}

	public String dsl() {
		return configuration.get("dsl");
	}

	public String expression() {
		String expression = configuration.get("expression");
		return (expression == null) ? "*\t" : expression;
	}

	public File setStore() {
		final String setStore = configuration.get("set_store");
		try {
			return setStore == null ? null : new File(setStore).getCanonicalFile();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	protected AuthService authService(URL authServiceUrl) {
		return null;
	}

	@Override
	protected EditorService editorService(URL editorServiceUrl) {
		return null;
	}
}