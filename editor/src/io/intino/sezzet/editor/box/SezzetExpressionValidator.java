package io.intino.sezzet.editor.box;

import io.intino.sezzet.editor.box.schemas.ValidationItem;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.SetqlChecker;
import io.intino.sezzet.setql.SetqlParser;
import io.intino.sezzet.setql.exceptions.SemanticException;
import io.intino.sezzet.setql.exceptions.SetqlError;
import io.intino.sezzet.setql.exceptions.SyntaxException;
import io.intino.sezzet.setql.graph.SetqlGraph;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SezzetExpressionValidator {
	private final SetqlGraph setqlGraph;
	private final SezzetGraph sezzetGraph;

	public SezzetExpressionValidator(SetqlGraph setqlGraph, SezzetGraph sezzetGraph) {
		this.setqlGraph = setqlGraph;
		this.sezzetGraph = sezzetGraph;
	}

	public boolean isValid(String expression, Locale locale) {
		return validationItems(expression, locale).isEmpty();
	}

	public List<ValidationItem> validationItems(String expression, Locale locale) {
		List<ValidationItem> items = new ArrayList<>();

		if (expression == null) return items;
		if (expression.isEmpty()) return items;

		try {
			new SetqlParser(expression, locale).check(setqlGraph);
			new SetqlChecker(sezzetGraph, locale).check(setqlGraph);
		} catch (SyntaxException e) {
			for (SetqlError error : e.errors())
				items.add(new ValidationItem().line(error.line()).message(((SyntaxException.SyntaxError) error).lineMessage()));
		} catch (SemanticException e) {
			for (SetqlError error : e.errors())
				items.add(new ValidationItem().line(error.line()).message(error.message()));
		}

		return items;
	}

}
