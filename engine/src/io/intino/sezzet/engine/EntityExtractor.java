package io.intino.sezzet.engine;

import io.intino.alexandria.zet.ZetReader;
import io.intino.alexandria.zet.ZetStream;

import java.util.ArrayList;
import java.util.List;

public class EntityExtractor implements EntityStream {

	private Entity current = null;
	private Entity next = null;
	private ZetStream stream;
	private List<ZetStreamStruct> featureStreams = new ArrayList<>();

	private EntityExtractor(ZetStream stream) {
		this.stream = stream;
	}

	private EntityExtractor(long[] ids) {
		this.stream = new ZetReader(ids);
	}

	public static EntityExtractorBuilder create(ZetStream stream) {
		return new EntityExtractorBuilder(new EntityExtractor(stream));
	}

	public static EntityExtractorBuilder create(long[] ids) {
		return new EntityExtractorBuilder(new EntityExtractor(ids));
	}

	@Override
	public Entity current() {
		return current;
	}

	@Override
	public Entity next() {
		if (current == next) hasNext();
		current = next;
		return current;
	}

	@Override
	public boolean hasNext() {
		if (current != next) return true;
		if (!stream.hasNext()) {
			next = null;
			return false;
		}
		long id = stream.next();
		advanceStreamsUntil(id);
		next = new Entity(id);
		for (ZetStreamStruct featureStream : featureStreams)
			if (featureStream.stream.current() == id) next.add(featureStream.feature, featureStream.value);
		return true;
	}


	private void advanceStreamsUntil(long value) {
		for (ZetStreamStruct featureStream : featureStreams)
			while (featureStream.stream.current() < value && featureStream.stream.hasNext())
				featureStream.stream.next();
	}

	public static class EntityExtractorBuilder {

		private EntityExtractor entityExtractor;

		EntityExtractorBuilder(EntityExtractor entityExtractor) {
			this.entityExtractor = entityExtractor;
		}

		public EntityExtractorBuilder add(ZetStream stream, String feature, String value) {
			entityExtractor.featureStreams.add(new ZetStreamStruct(stream, feature, value));
			return this;
		}

		public EntityExtractor commit() {
			return entityExtractor;
		}
	}

	private static class ZetStreamStruct {

		private final ZetStream stream;
		private final String feature;
		private final String value;

		ZetStreamStruct(ZetStream stream, String feature, String value) {
			this.stream = stream;
			this.feature = feature;
			this.value = value;
		}

	}
}
