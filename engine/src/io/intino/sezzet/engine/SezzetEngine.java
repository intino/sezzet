package io.intino.sezzet.engine;

import io.intino.alexandria.Scale;
import io.intino.alexandria.Timetag;
import io.intino.alexandria.logger.Logger;
import io.intino.alexandria.zet.ZetReader;
import io.intino.alexandria.zet.ZetStream;
import io.intino.alexandria.zet.ZetStream.Difference;
import io.intino.alexandria.zet.ZetStream.Intersection;
import io.intino.alexandria.zet.ZetStream.SymmetricDifference;
import io.intino.alexandria.zet.ZetStream.Union;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.SetQL;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.Expression.Predicate;
import io.intino.sezzet.setql.graph.Expression.Predicate.*;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.graph.rules.Operator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import static io.intino.sezzet.setql.graph.rules.Operator.*;
import static java.lang.Double.parseDouble;
import static java.time.LocalDateTime.ofInstant;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

public class SezzetEngine {
	private final SezzetGraph sezzetGraph;
	private SetProvider setProvider;
	private Instant reference;

	public SezzetEngine(SetProvider setProvider, SezzetGraph sezzetGraph) {
		this.setProvider = setProvider;
		this.sezzetGraph = sezzetGraph;
	}

	public void setReference(Instant reference) {
		this.reference = reference;
	}

	public ZetStream process(String expression, Locale locale) {
		SetqlGraph graph = SetQL.parseAndResolve(expression, locale, sezzetGraph);
		if (graph == null) return null;
		return process(graph.expression(), present());
	}

	ZetStream process(Expression expression, Instant reference) {
		this.reference = reference;
		return processExpression(expression);
	}

	private ZetStream processExpression(Expression expression) {
		return doProcessExpression(expression);
	}

	private ZetStream doProcessExpression(Expression expression) {
		List<ZetStream> streams = expression.operandList().stream().map(this::process).collect(toList());
		ZetStream result = streams.get(0);
		for (int i = 1; i < streams.size(); i++)
			result = doOperation(asList(result, streams.get(i)), expression.operand(i).operator());
		return result;
	}

	private ZetStream process(Expression.Operand operand) {
		return operand.i$(Predicate.class) ? process(operand.a$(Predicate.class)) :
				doProcessExpression(operand.a$(Expression.InnerExpression.class).expression());
	}

	private ZetStream process(Predicate predicate) {
		List<ZetStream> maps = !predicate.argumentList().isEmpty() ?
				predicate.argumentList().stream().map(a -> process(a, predicate)).collect(toList()) :
				singletonList(process(null, predicate));
		return doOperation(maps, predicate.argumentOperator());
	}

	private ZetStream doOperation(List<ZetStream> maps, Operator operator) {
		return maps.size() == 1 ? maps.get(0) :
				operator == OR ? processOR(maps) :
						operator == AND ? processAnd(maps) :
								operator == DIFF ? processDiff(maps) : processSymmetricDiff(maps);
	}

	private ZetStream processOR(List<ZetStream> list) {
		return new Union(list);
	}

	private ZetStream processAnd(List<ZetStream> list) {
		return new Intersection(list);
	}

	private ZetStream processDiff(List<ZetStream> list) {
		return new Difference(list);
	}

	private ZetStream processSymmetricDiff(List<ZetStream> list) {
		return new SymmetricDifference(list);
	}

	private Instant present() {
		return reference == null ? Instant.now() : reference;
	}

	private ZetStream process(Argument argument, Predicate predicate) {
		Scale scale = scaleOf(predicate.property());
		if (scale == null) return new ZetReader();
		return new Union(zetStreams(argument, predicate),
				predicate.frequency() != null ? predicate.frequency().lowBound() : 0,
				predicate.frequency() != null ? predicate.frequency().highBound() : Integer.MAX_VALUE,
				predicate.frequency() != null && predicate.frequency().consecutives());
	}

	private List<ZetStream> zetStreams(Argument argument, Predicate predicate) {
		return stream(from(predicate).iterateTo(to(predicate)).spliterator(), false)
				.flatMap(t -> zetStreams(t, predicate, argument))
				.collect(toList());
	}

	private Timetag timetag(Instant from, Scale scale) {
		return new Timetag(ofInstant(from, ZoneOffset.UTC), scale);
	}

	private Scale scaleOf(String feature) {
		return setProvider.scaleOf(feature);
	}

	private Timetag from(Predicate predicate) {
		Period period = predicate.period();
		return period == null ? timetag(present(), scaleOf(predicate.property())).previous() :
				period.i$(FromNow.class) ? fromNowTimetag(period.a$(FromNow.class)) :
						timeRangeFromTimetag(predicate, period);
	}

	private Timetag to(Predicate predicate) {
		Period period = predicate.period();
		return period == null ? timetag(present(), scaleOf(predicate.property())).previous() :
				period.i$(FromNow.class) ? new Timetag(ofInstant(present(), ZoneOffset.UTC), scaleOf(predicate.property())) :
						timeRangeToTimetag(predicate, period);
	}

	private Timetag timeRangeFromTimetag(Predicate predicate, Period period) {
		return timetag(period.a$(TimeRange.class).fromInstant(), scaleOf(predicate.property()));
	}

	private Timetag fromNowTimetag(FromNow fromNow) {
		Timetag timetag = new Timetag(ofInstant(present(), ZoneOffset.UTC), Scale.valueOf(fromNow.scale().name()));
		for (int i = 0; i < fromNow.amount(); i++) timetag = timetag.previous();
		return new Timetag(timetag.datetime(), scaleOf(fromNow.core$().ownerAs(Predicate.class).property()));
	}

	private Timetag timeRangeToTimetag(Predicate predicate, Period period) {
		return timetag(period.a$(TimeRange.class).toInstant(), scaleOf(predicate.property()));
	}

	private Stream<ZetStream> zetStreams(Timetag timetag, Predicate predicate, Argument argument) {
		return argument == null ? zetStreams(predicate.property(), timetag) :
				argument.i$(SingleValue.class) ?
						zetStreams(predicate.property(), timetag, argument.a$(SingleValue.class)) :
						zetStreams(timetag, predicate.property(), argument.a$(Range.class).lowBound(), argument.a$(Range.class).highBound());
	}

	private Stream<ZetStream> zetStreams(String property, Timetag timetag) {
		return Stream.of(setProvider.setOf(property, timetag).content());
	}

	private Stream<ZetStream> zetStreams(String feature, Timetag timetag, SingleValue value) {
		if (value instanceof VariableOperation) return zetStreams(feature, timetag, value.a$(VariableOperation.class));
		String regex = value.value().replace("*", ".*");
		return setProvider.setsOf(feature, timetag, s -> s.name().matches(regex)).map(SetProvider.Set::content);
	}

	private Stream<ZetStream> zetStreams(String feature, Timetag timetag, VariableOperation value) {
		return setProvider.setsOf(feature, timetag, s -> {
			boolean test = evaluate(s, value.comparison(0));
			for (int i = 1; i < value.comparisonList().size(); i++) {
				test = value.operators().get(i - 1).equals(AND) ?
						test & evaluate(s, value.comparison(i)) :
						test || evaluate(s, value.comparison(i));
			}
			return test;
		}).map(SetProvider.Set::content);
	}

	private boolean evaluate(SetProvider.Set set, VariableOperation.Comparison comparison) {
		String expression = set.valueOf(comparison.variable()) + comparison.comparator() + comparison.value();
		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		try {
			return (boolean) engine.eval(expression);
		} catch (ScriptException e) {
			Logger.error(e);
			return false;
		}
	}

	private Stream<ZetStream> zetStreams(Timetag timetag, String feature, double lowBound, double highBound) {
		return setProvider.setsOf(feature, timetag,
				s -> {
					if (!isDouble(s.name())) return false;
					double value = parseDouble(s.name());
					return value >= lowBound && value <= highBound;
				})
				.map(SetProvider.Set::content);
	}

	private boolean isDouble(String name) {
		try {
			parseDouble(name);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
