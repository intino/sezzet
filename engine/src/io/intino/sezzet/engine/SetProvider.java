package io.intino.sezzet.engine;

import io.intino.alexandria.Scale;
import io.intino.alexandria.Timetag;
import io.intino.alexandria.zet.ZetStream;

import java.util.function.Predicate;
import java.util.stream.Stream;

public interface SetProvider {

	Set setOf(String feature, Timetag timetag);

	Stream<Set> setsOf(String feature, Timetag timetag, Predicate<Set> filter);

	Scale scaleOf(String feature);

	interface Set {
		String name();

		Timetag timetag();

		int size();

		ZetStream content();

		String valueOf(String variableName);
	}
}
