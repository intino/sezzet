package io.intino.sezzet.engine;

public interface EntityStream {

	Entity current();

	Entity next();

	boolean hasNext();

}
