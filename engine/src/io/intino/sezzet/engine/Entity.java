package io.intino.sezzet.engine;

import java.util.ArrayList;
import java.util.List;

public class Entity {

	private long id;
	private List<Feature> features = new ArrayList<>();

	public Entity(long id) {
		this.id = id;
	}

	public long id() {
		return id;
	}

	public void add(String feature, String value) {
		features.add(new Feature(feature, value));
	}

	public List<Feature> features() {
		return features;
	}

	public static class Feature {

		String feature;
		String value;

		Feature(String feature, String value) {
			this.feature = feature;
			this.value = value;
		}

		public String feature() {
			return feature;
		}

		public String value() {
			return value;
		}

		@Override
		public String toString() {
			return feature + ':' + value;
		}
	}
}
