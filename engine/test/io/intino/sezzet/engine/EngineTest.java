package io.intino.sezzet.engine;

import io.intino.alexandria.Scale;
import io.intino.alexandria.Timetag;
import io.intino.alexandria.datalake.file.FS;
import io.intino.alexandria.zet.Zet;
import io.intino.alexandria.zet.ZetReader;
import io.intino.alexandria.zet.ZetStream;
import io.intino.magritte.framework.Graph;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.SetQL;
import io.intino.sezzet.setql.SetqlParser;
import io.intino.sezzet.setql.exceptions.SemanticException;
import io.intino.sezzet.setql.exceptions.SyntaxException;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.SetqlGraph;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.time.Instant.parse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Ignore
public class EngineTest {

	private static final String SetExtension = ".zet";
	private Locale locale = Locale.ENGLISH;

	private static void deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		directoryToBeDeleted.delete();
	}

	@Test
	public void processPredicateA() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createAExpression()));
		assertEquals(8, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
		assertTrue(zet.isIn(3L));
		assertTrue(zet.isIn(4L));
		assertTrue(zet.isIn(5L));
		assertTrue(zet.isIn(6L));
		assertTrue(zet.isIn(7L));
		assertTrue(zet.isIn(8L));
	}

	@Test
	public void processPredicateB() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createBExpression()));
		assertEquals(2, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
	}

	@Test
	public void processPredicateC() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createCExpression()));

		assertEquals(3, zet.size());
		assertTrue(zet.isIn(3L));
		assertTrue(zet.isIn(4L));
		assertTrue(zet.isIn(6L));
	}

	@Test
	public void processPredicateD() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createDExpression()));
		assertEquals(5, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
		assertTrue(zet.isIn(3L));
		assertTrue(zet.isIn(9L));
		assertTrue(zet.isIn(10L));
	}

	@Test
	public void processPredicateE() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createEExpression()));
		assertEquals(2, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
	}

	@Test
	public void processPredicateF() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createFExpression()));
		assertEquals(0, zet.size());
	}

	@Test
	public void processPredicateI() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createIExpression()));

		assertEquals(9, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
		assertTrue(zet.isIn(3L));
		assertTrue(zet.isIn(4L));
		assertTrue(zet.isIn(5L));
		assertTrue(zet.isIn(6L));
		assertTrue(zet.isIn(8L));
		assertTrue(zet.isIn(8L));
		assertTrue(zet.isIn(9L));
	}

	@Test
	public void processPredicateJ() throws SyntaxException, SemanticException {
		Zet zet = new Zet(process(createJExpression()));

		assertEquals(3, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
		assertTrue(zet.isIn(3L));
	}


	@Test
	@Ignore
	public void processPredicateK() {
		Zet zet = new Zet(process(createKExpression()));
//		assertEquals(1, zet.size());
//		assertTrue(zet.isIn(1L));
//		assertTrue(zet.isIn(2L));
//		assertTrue(zet.isIn(3L));TODO
	}

	@Test
	public void processPredicateL() throws SyntaxException, SemanticException {
		Zet zet = new Zet(engine().process(createLExpression(), parse("2018-07-04T00:00:00Z")));
		assertEquals(8, zet.size());
		assertTrue(zet.isIn(1L));
		assertTrue(zet.isIn(2L));
		assertTrue(zet.isIn(3L));
		assertTrue(zet.isIn(4L));
		assertTrue(zet.isIn(5L));
		assertTrue(zet.isIn(6L));
		assertTrue(zet.isIn(7L));
		assertTrue(zet.isIn(8L));
	}

	private ZetStream process(Expression expression) {
		return engine().process(expression, parse("2018-07-05T00:00:00Z"));
	}

	private SezzetEngine engine() {
		return new SezzetEngine(createZetProvider(), new Graph().loadStashes("Sezzet").as(SezzetGraph.class));
	}

	private SetProvider createZetProvider() {
		return new SetProvider() {

			@Override
			public Set setOf(String feature, Timetag timetag) {
				return null;
			}

			@Override
			public Stream<Set> setsOf(String feature, Timetag timetag, Predicate<Set> filter) {
				File folder = new File("test-res/" + feature + "/" + timetag + "/");
				return FS.filesIn(folder, f -> f.getName().endsWith(SetExtension))
						.map(this::setOf)
						.filter(filter);
			}

			private Set setOf(File file) {
				return new Set() {
					@Override
					public String name() {
						return file.getName().replace(SetExtension, "");
					}

					@Override
					public Timetag timetag() {
						return new Timetag(file.getParentFile().getName());
					}

					@Override
					public int size() {
						return (int) (file.length() / Long.BYTES);
					}

					@Override
					public ZetStream content() {
						return new ZetReader(file);
					}

					@Override
					public String valueOf(String variableName) {
						return null;
					}
				};
			}

			@Override
			public Scale scaleOf(String feature) {
				return Scale.Day;
			}
		};
	}

	private Expression createAExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\tBrowser(Chrome, Safari) :: period=1d\n";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	private Expression createBExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\tBrowser(Chrome) :: period=1d\n" +
				"&\tBrowser(Safari) :: period=1d";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();

	}

	private Expression createCExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\tBrowser(Chrome) :: period=1d\n" +
				"-\tBrowser(Safari) :: period=1d";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();

	}

	private Expression createDExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\n" +
				"\t*\tBrowser(Chrome) :: period=1d\n" +
				"\t&\tBrowser(Safari) :: period=1d\n" +
				"|\tCountry(Germany) :: period=1d";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	private Expression createEExpression() throws SyntaxException, SemanticException {
		String input = "*\n" +
				"\t*\tBrowser(Chrome) :: period=1d\n" +
				"\t&\tBrowser(Safari) :: period=1d\n" +
				"&\tCountry(Germany) :: period=1d";
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	private Expression createFExpression() throws SyntaxException, SemanticException {
		String input = "*\n" +
				"\t*\tBrowser(Chrome) :: period=1d\n" +
				"\t&\tBrowser(Safari) :: period=1d\n" +
				"-\tCountry(Germany) :: period=1d";
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	private Expression createIExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\tZipcode(\"35*\") :: period=1d\n";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	private Expression createJExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\tAge(10..20) :: period=1d\n";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	private Expression createKExpression() {
		String input = "*\tRPU.Consumo.PromedioDiario(<=1) :: period=2m";
		SezzetGraph sezzet = new Graph().loadStashes("Sezzet", "asemed").as(SezzetGraph.class);
		SetqlGraph graph = SetQL.parseAndResolve(input, locale, sezzet);
		return graph.expression();
	}

	private Expression createLExpression() throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		String input = "*\tBrowser(Chrome, Safari)\n";
		new SetqlParser(input, locale).parse(graph);
		return graph.expression();
	}

	@After
	public void tearDown() {
		deleteDirectory(new File("test-res/segments"));
	}
}
