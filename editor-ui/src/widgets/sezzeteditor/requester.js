var SezzetEditorBehaviors = SezzetEditorBehaviors || {};

SezzetEditorBehaviors.Requester = {

    onSuggest : function(value) {
    	this.carry("onSuggest", { "value" : value });
    },
    onChange : function(value) {
    	this.carry("onChange", { "value" : value });
    }

};