var SezzetEditorBehaviors = SezzetEditorBehaviors || {};

SezzetEditorBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("render").toSelf().execute(function(parameters) {
        	widget._render(parameters.value);
        });
        this.when("setExpression").toSelf().execute(function(parameters) {
        	widget._setExpression(parameters.value);
        });
        this.when("validationResult").toSelf().execute(function(parameters) {
        	widget._validationResult(parameters.value);
        });
        this.when("suggestion").toSelf().execute(function(parameters) {
        	widget._suggestion(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};