package io.intino.sezzet.model.graph.functions;

import java.util.List;

@FunctionalInterface
public interface SearchSpaceValuesLoader {
	List<String> values(String... prefixes);
}
