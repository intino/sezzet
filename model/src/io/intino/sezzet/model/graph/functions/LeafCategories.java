package io.intino.sezzet.model.graph.functions;

import io.intino.sezzet.model.graph.Category;

@FunctionalInterface
public interface LeafCategories {
	Category[] leafCategories();
}