package io.intino.sezzet.model.graph;

import io.intino.alexandria.logger.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.*;

import static java.util.stream.Collectors.toList;

public class Model {
	private static Map<String, List<String>> searchSpaceMap = new HashMap<>();

	public static Category[] leafCategories(Enumeration self) {
		List<Category> categories = new ArrayList<>();
		for (Category category : self.categoryList)
			if (category.isComposite()) categories.addAll(leafCategories(category));
			else categories.add(category);
		return categories.toArray(new Category[0]);
	}

	private static List<Category> leafCategories(Category category) {
		List<Category> categories = new ArrayList<>();
		for (Category c : category.asComposite().categoryList())
			if (c.isComposite()) categories.addAll(leafCategories(c));
			else categories.add(c);
		return categories;
	}

	public static List<String> loadSearchSpacesValues(Feature.Text self, String... prefixes) {
		List<String> prefixList = Arrays.asList(prefixes);
		List<SearchSpace> searchSpaces = self.spaces().stream().filter(s -> s.prefix() == null || prefixList.contains(s.prefix())).collect(toList());
		HashSet<String> result = new HashSet<>();
		searchSpaces.forEach(s -> result.addAll(s.values()));
		return new ArrayList<>(result);
	}

	public static List<String> loadSearchSpaceValues(SearchSpace self) {
		try {
			if (self == null || self.file() == null) return Collections.emptyList();

			String key = self.name$();
			if (!searchSpaceMap.containsKey(key))
				searchSpaceMap.put(key, readLines(new BufferedReader(new InputStreamReader(self.file().openStream()))));

			return searchSpaceMap.get(key);
		} catch (IOException e) {
			Logger.error(e);
			return null;
		}
	}

	private static List<String> readLines(Reader input) throws IOException {
		BufferedReader reader = toBufferedReader(input);
		List<String> list = new ArrayList<>();
		for (String line = reader.readLine(); line != null; line = reader.readLine()) list.add(line);

		return list;
	}

	private static BufferedReader toBufferedReader(Reader reader) {
		return reader instanceof BufferedReader ? (BufferedReader) reader : new BufferedReader(reader);
	}

	public static Integer decimals(Range self) {
		String stepSize = String.valueOf(self.stepSize()).replace(".0", "");
		return stepSize.contains(".") ? stepSize.substring(stepSize.indexOf(".") + 1).length() : 0;
	}

	private static class Charsets {
		/**
		 * @deprecated
		 */
		@Deprecated
		public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
		/**
		 * @deprecated
		 */
		@Deprecated
		public static final Charset US_ASCII = Charset.forName("US-ASCII");
		/**
		 * @deprecated
		 */
		@Deprecated
		public static final Charset UTF_16 = Charset.forName("UTF-16");
		/**
		 * @deprecated
		 */
		@Deprecated
		public static final Charset UTF_16BE = Charset.forName("UTF-16BE");
		/**
		 * @deprecated
		 */
		@Deprecated
		public static final Charset UTF_16LE = Charset.forName("UTF-16LE");
		/**
		 * @deprecated
		 */
		@Deprecated
		public static final Charset UTF_8 = Charset.forName("UTF-8");

		public Charsets() {
		}

		public static Charset toCharset(Charset charset) {
			return charset == null ? Charset.defaultCharset() : charset;
		}

		public static Charset toCharset(String charset) {
			return charset == null ? Charset.defaultCharset() : Charset.forName(charset);
		}
	}

}
