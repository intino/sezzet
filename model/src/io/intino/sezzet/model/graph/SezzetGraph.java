package io.intino.sezzet.model.graph;


import io.intino.magritte.framework.Graph;

import java.util.ArrayList;
import java.util.List;

public class SezzetGraph extends io.intino.sezzet.model.graph.AbstractGraph {

	public SezzetGraph(Graph graph) {
		super(graph);
	}

	public SezzetGraph(io.intino.magritte.framework.Graph graph, SezzetGraph wrapper) {
		super(graph, wrapper);
	}

	public Feature find(String feature) {
		return this.allFeatures().stream().filter(f -> f.label().equals(feature)).findFirst().orElse(null);
	}

	public List<Feature> allFeatures() {
		List<Feature> features = new ArrayList<>(this.featureList());
		features.addAll(featuresOf(this.groupList()));
		return features;
	}

	private List<Feature> featuresOf(List<Group> groups) {
		List<Feature> features = new ArrayList<>();
		for (Group group : groups) {
			features.addAll(group.featureList);
			features.addAll(featuresOf(group.groupList));
		}
		return features;
	}

	public List<Category> leafCategoriesOf(Feature feature) {
		return leafCategoriesOf(feature.asEnumerate().enumeration().categoryList());
	}

	public List<Category> leafCategoriesOf(List<Category> values) {
		List<Category> list = new ArrayList<>();
		for (Category value : values)
			if (value.isComposite()) list.addAll(leafCategoriesOf(value.asComposite().categoryList()));
			else list.add(value);
		return list;
	}

	public List<Category> compositeCategoriesOf(Feature feature) {
		return compositeCategoriesOf(feature.asEnumerate().enumeration().categoryList());
	}

	private List<Category> compositeCategoriesOf(List<Category> values) {
		List<Category> list = new ArrayList<>();
		for (Category value : values)
			if (value.isComposite()) {
				list.add(value);
				list.addAll(compositeCategoriesOf(value.asComposite().categoryList()));
			}
		return list;
	}

	public List<Category> valuesOf(Feature feature) {
		return valuesOf(feature.asEnumerate().enumeration().categoryList());
	}

	public List<Category> valuesOf(List<Category> values) {
		List<Category> list = new ArrayList<>();
		for (Category value : values) {
			if (value.isComposite()) list.addAll(valuesOf(value.asComposite().categoryList()));
			list.add(value);
		}
		return list;
	}
}