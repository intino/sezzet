package io.intino.sezzet.setql;

import io.intino.magritte.framework.Graph;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.exceptions.SemanticException;
import io.intino.sezzet.setql.exceptions.SyntaxException;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.Expression.Predicate;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.graph.rules.Operator;
import io.intino.sezzet.setql.graph.rules.Scale;
import org.junit.Test;

import java.time.Instant;
import java.util.Locale;

import static io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.Modifier.All;
import static io.intino.sezzet.setql.graph.Expression.InnerExpression;
import static io.intino.sezzet.setql.graph.Expression.Predicate.*;
import static io.intino.sezzet.setql.graph.rules.Scale.Month;
import static java.lang.System.out;
import static org.junit.Assert.*;

public class ModelGenerationTest {

	private String test0 = "*\tBrowser(Chrome, Safari) :: " + translate("period") + "=2018-08..2018-09\n" +
			"&\tCountry(Spain) :: " + translate("period") + "=50d " + translate("recency") + "=10m\n";
	private String test1 = "*\tBrowser(Chrome, Safari) :: " + translate("period") + "=2018-08..2018-09\n" +
			"&\n" +
			"\t*\tDevice(mobile) :: " + translate("period") + " = 60m\n" +
			"\t|\tImpression(banner) :: " + translate("period") + "=20d " + translate("frequency") + ">20 " + translate("period") + " = 2h\n\n";
	private String test2 = "*\tArea(DU)\n" +
			"&\n" +
			"\t*\tSolicitudLire(CristalRoto, Diablillo) :: " + translate("period") + " = 15m \n" +
			"\t|\tAnomalia(FM) :: " + translate("period") + "=20d " + translate("frequency") + ">2\n\n";
	private String test3 = "*\tNumerico(>20) :: " + translate("period") + "=20d " + translate("frequency") + "=0..10\n"
			+ "|\tValorExacto(20000000.5666) :: " + translate("period") + "=20d " + translate("frequency") + "=0..10\n"
			+ "|\tRango(1..15) :: " + translate("period") + "=20d " + translate("frequency") + "<10\n";

	private String test4 = "*\tRPU.Poblacion.Estado(San Luis Potosi) :: " + translate("commons") + " " + translate("period") + "=2m ";
	private String test5 = "*\tRPU.Poblacion.Estado(San Luis Potosi) :: " + translate("commons") + " " + translate("period") + "=2m " + translate("recency") + "=1m " + translate("new");
	private String test6 = "*\tRPU.Poblacion.Estado('scoring > 0.2') :: " + translate("period") + "=2m ";
	private String test7 = "*\tRPU.Poblacion.Estado('scoring > 0.2 & scoring < 0.8') ::" + translate("period") + "=2m ";
	private String test8 = "*\tRPU.Consumo.PromedioDiario(0..1) :: " + translate("period") + "=2m";
	private String test9 = "*\tRPU.Consumo.PromedioDiario(<1) :: " + translate("period") + "=2m";
	private String test11 = "*\tRPU.Conexion.Ruta(\"DU0000\", \"DU1000\") :: commons";
	private String test10 = "*\tRPU.Giro(Industrial)";
	private String test12 = "*\tRPU.Conexion.Multiplicador(3..840000)";
	private String test13 = "*\tRPU.Consumo.PromedioDiario(<6.0) :: " + translate("period") + "=1m";
	private String test14 = "*\tRPU.Consumo.PromedioDiario(<1) :: " + translate("period") + "=2016..2017";
	private String test17 = "*\tRPU.Consumo.PromedioDiario(>= 8.0) :: " + translate("period") + "=1y " + translate("frequency") + "=6..10 " + translate("consecutive") + "\n";
	private String test18 = "*\tRPU.Lectura.AnomaliasFasores(\"4\") :: periodo = 2meses\n" + "&\tRPU.Medidor.Multiplicador(2..840000)\n" + "&\tRPU.Adscripcion.Agencia(\"DW03D\")";
	private String test19 = "*\tRPU.Adscripcion.Zona(\"DC01\")\n" + "&\tRPU.Adscripcion.Agencia(\"DC01A\")\n" + "&\tRPU.Tarifa.Tipo(Media Tension)\n" + "&\tRPU.Consumo.Plano(90..100) :: periodo = 3meses";

	@Test
	public void test0() throws SyntaxException, SemanticException {
		SetqlGraph graph = parse(test0);
		assertEquals("Browser", graph.expression().predicate(0).property());
		for (Predicate predicate : graph.core$().find(Predicate.class)) {
//			out.println(predicate.toString(locale()));
			out.println(predicate.description(locale(), "RPU"));
		}
	}

	@Test
	public void test1() throws SyntaxException, SemanticException {
		SetqlGraph graph = parse(test1);
		assertEquals("Browser", graph.expression().predicate(0).property());
		assertEquals(2, graph.expression().operandList().size());
		assertTrue(graph.expression().operandList().get(0) instanceof Predicate);
		Predicate predicate = graph.expression().operandList().get(0).a$(Predicate.class);
		assertEquals(predicate.period().a$(TimeRange.class).from(), "2018-08");
		assertEquals(predicate.period().a$(TimeRange.class).to(), "2018-09");
		assertTrue(graph.expression().operandList().get(1) instanceof InnerExpression);
		assertTrue(graph.expression().operandList().get(1).a$(InnerExpression.class).expression().operandList().get(0) instanceof Predicate);
		assertEquals("Device", graph.expression().operandList().get(1).a$(InnerExpression.class).expression().operandList().get(0).a$(Predicate.class).property());
		assertEquals("Impression", graph.expression().operandList().get(1).a$(InnerExpression.class).expression().operandList().get(1).a$(Predicate.class).property());
		for (Predicate p : graph.core$().find(Predicate.class)) out.println(p.toString(locale()));
	}

	@Test
	public void test2() throws SyntaxException, SemanticException {
		SetqlGraph graph = parse(test2);
		assertEquals("Area", graph.expression().predicate(0).property());
		assertEquals(2, graph.expression().operandList().size());
		assertTrue(graph.expression().operandList().get(0) instanceof Predicate);
		assertTrue(graph.expression().operandList().get(1) instanceof InnerExpression);
		assertTrue(graph.expression().operandList().get(1).a$(InnerExpression.class).expression().operandList().get(0) instanceof Predicate);
		assertEquals("SolicitudLire", graph.expression().operandList().get(1).a$(InnerExpression.class).expression().operandList().get(0).a$(Predicate.class).property());
		assertEquals("Anomalia", graph.expression().operandList().get(1).a$(InnerExpression.class).expression().operandList().get(1).a$(Predicate.class).property());
		for (Predicate predicate : graph.core$().find(Predicate.class))
			out.println(predicate.toString(locale()));
	}


	@Test
	public void test3() throws SyntaxException, SemanticException {
		SetqlGraph graph = parse(test3);
		assertEquals("Numerico", graph.expression().predicate(0).property());
		assertEquals(3, graph.expression().operandList().size());
		assertTrue(graph.expression().operandList().get(0) instanceof Predicate);
		assertTrue(graph.expression().operandList().get(1) instanceof Predicate);

		Predicate predicate = graph.expression().operandList().get(0).a$(Predicate.class);
		assertEquals("Numerico", predicate.property());
		assertEquals(1, predicate.argumentList().size());
		assertTrue(predicate.argumentList().get(0).i$(Range.class));
		assertEquals(20.0, predicate.argumentList().get(0).a$(Range.class).lowBound(), 0);
		assertEquals(Double.MAX_VALUE, predicate.argumentList().get(0).a$(Range.class).highBound(), 0);

		predicate = graph.expression().operandList().get(1).a$(Predicate.class);
		assertEquals("ValorExacto", predicate.property());
		assertEquals(1, predicate.argumentList().size());
		assertTrue(predicate.argumentList().get(0).i$(Numeric.class));
//		assertEquals("20", predicate.argumentList().get(0).a$(Predicate.Numeric.class).value());

		predicate = graph.expression().operandList().get(2).a$(Predicate.class);
		assertEquals("Rango", predicate.property());
		assertEquals(1, predicate.argumentList().size());
		assertTrue(predicate.argumentList().get(0).i$(Range.class));
		assertEquals(1, predicate.argumentList().get(0).a$(Range.class).lowBound(), 0);
		assertEquals(15, predicate.argumentList().get(0).a$(Range.class).highBound(), 0);
		assertEquals(0, predicate.frequency().lowBound());
		assertEquals(9, predicate.frequency().highBound());
		graph.core$().find(Predicate.class).stream().map(p -> p.description(locale(), "RPU")).forEach(out::println);
	}

	@Test
	public void test4() {
		SetqlGraph graph = parseAndResolve(test4);
		assertFalse(graph.expression().predicateList().isEmpty());
		assertEquals(All, graph.expression().predicateList().get(0).modifier());
		assertEquals(Operator.AND, graph.expression().predicateList().get(0).argumentOperator());
		graph.core$().find(Predicate.class).stream().map(predicate -> predicate.toString(locale())).forEach(out::println);
	}

	@Test
	public void test5() {
		SetqlGraph graph = parseAndResolve(test5);
		assertEquals(graph.expression().predicateList().size(), 0);
		assertEquals(graph.expression().innerExpressionList().size(), 1);
		Expression expression = graph.expression().innerExpressionList().get(0).expression();
		assertNotNull(expression);
		assertEquals(expression.predicateList().size(), 2);
		Predicate p1 = expression.predicateList().get(0);
		Predicate p2 = expression.predicateList().get(1);
		assertEquals("RPU.Poblacion.Estado", p1.property());
		assertEquals("RPU.Poblacion.Estado", p2.property());
		assertEquals(Operator.OR, p1.operator());
		assertEquals(Operator.DIFF, p2.operator());
		assertTrue(p1.period().i$(TimeRange.class));
//		assertEquals(Month.label(minus(2, Month)), p1.period().a$(TimeRange.class).from());
//		assertEquals(Month.label(minus(1, Month)), p1.period().a$(TimeRange.class).to());
		assertTrue(p2.period().i$(TimeRange.class));
//		assertEquals(Month.label(minus(2, Month)), p2.period().a$(TimeRange.class).from());
//		assertEquals(Month.label(minus(2, Month)), p2.period().a$(TimeRange.class).to());
		graph.core$().find(Predicate.class).stream().map(p -> p.description(locale(), "RPU")).forEach(out::println);
	}


	private Instant minus(int amount, Scale scale) {
		Instant now = Instant.now();
		for (int i = 0; i <= amount; i++) now = scale.minus(now);
		return now;
	}

	@Test
	public void test6() {
		SetqlGraph graph = parseAndResolve(test6);
		Argument argument = graph.expression().predicateList().get(0).argument(0);
		assertNotNull(argument);
		assertTrue(argument.i$(VariableOperation.class));
		assertNotNull(argument.a$(VariableOperation.class).comparison(0));
		VariableOperation.Comparison comparision = argument.a$(VariableOperation.class).comparison(0);
		assertEquals(comparision.variable(), "scoring");
		assertEquals(comparision.comparator(), ">");
		assertEquals(comparision.value(), "0.2");
		graph.core$().find(Predicate.class).stream().map(predicate -> predicate.toString(locale())).forEach(out::println);
	}


	@Test
	public void test7() {
		SetqlGraph graph = parseAndResolve(test7);
		Argument argument = graph.expression().predicateList().get(0).argument(0);
		assertNotNull(argument);
		assertTrue(argument.i$(VariableOperation.class));
		VariableOperation operation = argument.a$(VariableOperation.class);
		assertNotNull(operation.comparison(0));
		VariableOperation.Comparison c1 = operation.comparison(0);
		assertEquals(c1.variable(), "scoring");
		assertEquals(c1.comparator(), ">");
		assertEquals(c1.value(), "0.2");
		assertEquals(operation.operators().get(0), Operator.AND);
		VariableOperation.Comparison c2 = operation.comparison(1);
		assertEquals(c2.variable(), "scoring");
		assertEquals(c2.comparator(), "<");
		assertEquals(c2.value(), "0.8");
		graph.core$().find(Predicate.class).stream().map(predicate -> predicate.toString(locale())).forEach(out::println);
	}

	@Test
	public void test8() {
		SetqlGraph graph = parseAndResolve(test8);
		assertEquals("RPU.Consumo.PromedioDiario", graph.expression().predicate(0).property());
		for (Predicate predicate : graph.core$().find(Predicate.class))
			out.println(predicate.description(locale(), "RPU"));
	}

	@Test
	public void test9() {
		SetqlGraph graph = parseAndResolve(test9);
		assertEquals("RPU.Consumo.PromedioDiario", graph.expression().predicate(0).property());
		for (Predicate predicate : graph.core$().find(Predicate.class))
			out.println(predicate.toString(locale()));
	}

	@Test
	public void test10() {
		SetqlGraph graph = parseAndResolve(test10);
		assertEquals("RPU.Giro", graph.expression().predicate(0).property());
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
		assertEquals("RPU.Giro(Industrial.Agropecuaria, Industrial.Extractiva, Industrial.Transformacion, Industrial.Minera, Industrial.Construccion, Industrial.Energia)", predicate.toString(locale()));
		out.println(predicate.toString(locale()));
	}

	@Test
	public void test11() throws SyntaxException, SemanticException {
		SetqlGraph graph = parse(test11);
		assertEquals("RPU.Conexion.Ruta", graph.expression().predicate(0).property());
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
		out.println(predicate.description(locale(), "RPU"));
	}

	@Test
	public void test13() {
		SetqlGraph graph = parseAndResolve(test13);
		assertEquals("RPU.Consumo.PromedioDiario", graph.expression().predicate(0).property());
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
//		assertEquals(36, predicate.fromNow().amount());
		assertEquals(Month, predicate.fromNow().scale());
		out.println(predicate.description(locale(), "RPU"));
	}

	@Test
	public void test14() {
		SetqlGraph graph = parseAndResolve(test14);
		assertEquals("RPU.Consumo.PromedioDiario", graph.expression().predicate(0).property());
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
		assertEquals("2016-01", predicate.timeRange().from());
		assertEquals("2017-12", predicate.timeRange().to());
		out.println(predicate.description(locale(), "RPU"));
	}

	@Test
	public void test15() {
		SetqlGraph graph = parseAndResolve("*\tRPU.Tarifa.Tipo(Media Tension.Horaria)");
		Predicate predicate = graph.expression().predicate(0);
		assertEquals("RPU.Tarifa.Tipo", predicate.property());
	}

	@Test
	public void test16() {
		SetqlGraph graph = parseAndResolve("*\tRPU.Conexion.Tipo(Especial.Alta-Alta)");
		Predicate predicate = graph.expression().predicate(0);
		assertEquals("RPU.Conexion.Tipo", predicate.property());
		assertTrue(predicate.argument(0).i$(Predicate.Enum.class));
		assertEquals("Especial.Alta-Alta", predicate.argument(0).a$(Predicate.Enum.class).value());
	}

	@Test
	public void test17() {
		SetqlGraph graph = parseAndResolve(test17);
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
		out.println(predicate.description(locale(), "RPU"));
	}

	@Test
	public void test18() {
		SetqlGraph graph = parseAndResolve(test18, spanishLocale());
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
		out.println(predicate.description(spanishLocale(), "RPU"));
	}

	@Test
	public void test19() {
		SetqlGraph graph = parseAndResolve(test19, spanishLocale());
		Predicate predicate = graph.core$().find(Predicate.class).get(0);
		out.println(predicate.description(spanishLocale(), "RPU"));
	}

	private SetqlGraph parse(String input) throws SyntaxException, SemanticException {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		SetqlParser parser = parser(input);
		parser.parse(graph);
		return graph;
	}

	private SetqlGraph parseAndResolve(String input) {
		return SetQL.parseAndResolve(input, locale(), graph());
	}

	private SetqlGraph parseAndResolve(String input, Locale locale) {
		return SetQL.parseAndResolve(input, locale, graph());
	}

	private SezzetGraph graph() {
		return new Graph().loadStashes("Sezzet", "asemed").as(SezzetGraph.class);
	}

	private SetqlParser parser(String test) {
		return new SetqlParser(test, locale());
	}

	private String translate(String key) {
		return MessageProvider.message(locale(), key);
	}

	private Locale locale() {
		return new Locale("en", "EN");
	}

	private Locale spanishLocale() {
		return new Locale("es", "ES");
	}
}
