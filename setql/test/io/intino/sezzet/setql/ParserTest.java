package io.intino.sezzet.setql;

import io.intino.magritte.framework.utils.UTF8Control;
import io.intino.sezzet.setql.exceptions.SyntaxException;
import io.intino.sezzet.setql.parser.SetqlLexicon;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.VocabularyImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.antlr.v4.runtime.CharStreams.fromString;

public class ParserTest {

	private ResourceBundle messages = ResourceBundle.getBundle("messages", locale(), new UTF8Control());


	@Test
	public void should_accept_sentence_0() {
		try {
			String input = "*\tBrowser(Chrome, Safari) :: " + textFromKey("period") + "=2d\n";
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_1() {
		String input = "*\tBrowser(Chrome, Safari) :: " + textFromKey("period") + "=2d\n" +
				"&\n" +
				"\t*\tDevice(mobile) :: " + textFromKey("period") + " = 60d\n" +
				"\t|\tImpression(banner) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + " > 20 " + textFromKey("recency") + " = 2d\n\n";
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_2() {
		String input = "*\tArea(DU)\n" +
				"&\n" +
				"\t*\tSolicitudLire(CristalRoto, Diablillo) :: " + textFromKey("period") + " = 15m\n" +
				"\t|\tAnomalia(FM) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + ">2\n\n";
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	private String textFromKey(String key) {
		return MessageProvider.message(locale(), key);
	}

	@Test
	public void should_accept_sentence_3() {
		String input = "*\tView(banner) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + " > 20 " + textFromKey("recency") + " = 1d\n" +
				"|\n" +
				"\t*\tInterest(travels) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + ">10\n" +
				"\t&\tInterest(sports) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + ">10\n" +
				"\t|\tInterest(cars) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + ">10";
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_4() {
		String input = "*\tNumerico(>20) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + "=0..10\n"
				+ "|\tValorExacto(20) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + "=0..10\n"
				+ "|\tRango(1..15) :: " + textFromKey("period") + "=20d " + textFromKey("frequency") + "<10\n";
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_5() {
		try {
			String input = "*\tBrowser(Chrome, Safari) :: " + textFromKey("period") + "=2d\n" +
					"&\tCountry(Spain) :: " + textFromKey("period") + "=50d " + textFromKey("recency") + "=10d\n";
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}


	@Test
	public void should_accept_sentence_6() {
		try {
			String input = "*\n" +
					"\t*\tBrowser(Chrome) :: " + textFromKey("period") + "=1d\n" +
					"\t&\tBrowser(Safari) :: " + textFromKey("period") + "=1d\n" +
					"|\tCountry(Germany) :: " + textFromKey("period") + "=1d";
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_7() {
		try {
			String input = "*\tBrowser(Chrome, Safari) :: " + textFromKey("period") + "=2018-09..2018-10\n";
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}


	@Test
	public void should_accept_sentence_8() {
		String input = "*\tPoblacion('tasa > 80') :: " + textFromKey("period") + "=2d\n";
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_9() {
		try {
			String input = "*\tRPU.Poblacion.Estado(San Luis Potosi) ::" + textFromKey("commons") + " " + textFromKey("period") + "=2d ";
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_10() {
		String input = "*\tPoblacion('A & B') :: " + textFromKey("period") + "=2d\n";
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_11() {
		String input = "*\tRPU.Conexion.Ruta(\"DU0000\", \"DU1000\") :: " + textFromKey("commons");
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	@Test
	public void should_accept_sentence_12() {
		String input = "*\tRPU.Tarifa.Tipo(Media Tension.Horaria) :: " + textFromKey("commons");
		try {
			checkGrammar(input);
		} catch (SyntaxException e) {
			Assert.fail("'error'   ->    " + e.errors().get(0).message());
		}
	}

	private SetqlLexicon showLexicon(String text) {
		SetqlLexicon lexer = new SetqlLexicon(fromString(text));
		lexer.reset();
		Token token;
		while ((token = lexer.nextToken()).getType() != -1) {
			String name = lexer.getVocabulary().getSymbolicName(token.getType());
			if (name.contains("LINE")) System.out.println();
			System.out.print(name + ", ");
		}
		System.out.println();
		return lexer;
	}

	private void checkGrammar(String input) throws SyntaxException {
		SetqlParser parser = new SetqlParser(input, locale());
		parser.parse();
	}

	private Locale locale() {
		return new Locale("es", "ES");
//		return new Locale("en","EN");
	}

	private String getExpectedTokens(Parser recognizer) {
		try {
			return recognizer.getExpectedTokens().toString(VocabularyImpl.fromTokenNames(recognizer.getTokenNames()));
		} catch (Exception e) {
			return "";
		}
	}
}
