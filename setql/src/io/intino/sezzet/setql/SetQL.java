package io.intino.sezzet.setql;

import io.intino.alexandria.logger.Logger;
import io.intino.magritte.framework.Graph;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.exceptions.SemanticException;
import io.intino.sezzet.setql.exceptions.SyntaxException;
import io.intino.sezzet.setql.graph.Expression.Predicate;
import io.intino.sezzet.setql.graph.Expression.Predicate.Frequency;
import io.intino.sezzet.setql.graph.Expression.Predicate.Recency;
import io.intino.sezzet.setql.graph.Expression.Predicate.VariableOperation;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.graph.rules.Scale;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Locale;

import static io.intino.sezzet.setql.MessageProvider.message;
import static io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.AbstractRecency.Range.Old;
import static java.lang.Double.*;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class SetQL {
	private static DecimalFormat formatter = formatter();

	public static SetqlGraph parseAndResolve(String input, Locale locale, SezzetGraph sezzet) {
		SetqlGraph graph = new Graph().loadStashes("Setql").as(SetqlGraph.class);
		try {
			SetqlParser parser = new SetqlParser(input, locale);
			parser.parse(graph);
			new SetqlChecker(sezzet, locale).check(graph);
			new SetqlSugarFree(sezzet).free(graph);
			return graph;
		} catch (SyntaxException | SemanticException e) {
			Logger.error(e);
			return null;
		}
	}

	public static String toString(Predicate self, Locale locale) {
		return self.property() + "(" + self.argumentList().stream().map(a -> a.toString(locale)).collect(joining(", ")) + ")" + separator(self) +
				modifier(self.modifier(), locale) +
				periodString(self, locale) +
				frequencyString(self, locale) +
				recencyString(self, locale);
	}

	public static String description(Predicate self, Locale locale, String subject) {
		String type = type(self);
		return self.property() + " " + message(locale, "description.join.first") + " " + subject +
				" " + message(locale, "description.join." + modifier(self, type)) + " " +
				values(self, locale, type) + period(self, locale) + frequency(self, locale) + recency(self, locale);
	}

	private static String type(Predicate self) {
		return self.argumentList().isEmpty() ? "singleton" : self.argumentList().get(0).getClass().getSimpleName().toLowerCase();
	}

	private static String period(Predicate self, Locale locale) {
		if (self.period() == null) return "";
		String message = message(locale, "description.join.period." + self.period().getClass().getSimpleName().toLowerCase() + (valueOf(self.period()) == 1 ? ".singular" : ""));
		return (message.isEmpty() ? "" : " " + message) + " " + periodDescription(self, locale);
	}

	private static int valueOf(Predicate.Period period) {
		return period.i$(Predicate.FromNow.class) ? period.a$(Predicate.FromNow.class).amount() : 0;
	}

	private static String values(Predicate self, Locale locale, String type) {
		return self.argumentList().stream().map(a -> a.description(locale)).collect(joining(", "));
	}

	private static String frequency(Predicate self, Locale locale) {
		return self.frequency() == null ? "" : " " + description(self.frequency(), locale);
	}

	private static String recency(Predicate self, Locale locale) {
		return self.recency() == null ? "" : " " + message(locale, "description.join.recency") + " " + recencyDescription(self, locale);
	}

	private static String modifier(Predicate self, String type) {
		if (type.equals("numeric")) return type + ".singular";
		if (type.equals("range")) {
			Predicate.Range range = (Predicate.Range) self.argumentList().get(0);
			if (range.lowBound() == range.highBound()) return "numeric.singular";
		}
		return type.equals("enum") || type.equals("text") ? type + (self.argumentList().size() == 1 ? ".singular" : "." + self.modifier().name().toLowerCase()) : type;
	}

	public static String toString(VariableOperation self, Locale locale) {
		return toString(self);
	}

	public static String toString(VariableOperation self) {
		StringBuilder builder = new StringBuilder("'");
		List<VariableOperation.Comparison> comparisons = self.comparisonList();
		for (int i = 0; i < comparisons.size(); i++) {
			if (i > 0) builder.append(" ").append(self.operators().get(i - 1).value()).append(" ");
			VariableOperation.Comparison comparision = comparisons.get(i);
			builder.append(comparision.variable()).append(" ").append(comparision.comparator()).append(" ").append(comparision.value());
		}
		return builder.append("'").toString();
	}

	public static String toString(Frequency self, Locale locale) {
		return " " + message(locale, "frequency") + valuesOf(self.lowBound(), self.highBound(), 0);
	}

	public static String description(Frequency self, Locale locale) {
		String values = frequencyValuesOf(self.lowBound(), self.highBound(), 0, locale);
		String description = (values.contains("..") ? message(locale, "between") + " " : "") + values.replace("..", " " + message(locale, "and") + " ") + " " + message(locale, "times");
		if (self.consecutives()) description += " " + message(locale, "consecutive");
		return description;
	}

	public static String toString(Recency self, Locale locale) {
		return " " + message(locale, "recency") + " = " + self.amount() + self.scale().name().toLowerCase().charAt(0) + (self.range().equals(Old) ? "" : " " + message(locale, self.range().name().toLowerCase()));
	}

	public static String toString(Predicate.Range self, Locale locale) {
		String value = valuesOf(self.lowBound(), self.highBound(), self.decimals()).trim();
		return value.startsWith("=") ? value.substring(2) : value;
	}

	public static String toString(Predicate.Numeric self, Locale locale) {
		return truncate(self.value(), self.decimals());
	}

	private static String truncate(String value, int decimals) {
		if (!value.contains(".")) return value;
		int i = value.indexOf(".");
		if (i + decimals > value.length()) return value;
		return value.substring(0, i + decimals);
	}

	public static String toString(Predicate.FromNow self, Locale locale) {
		return " " + message(locale, "period") + " = " + self.amount() + scale(self.scale(), locale, self.amount());
	}

	public static String description(Predicate.FromNow self, Locale locale) {
		return (self.amount() == 1 ? "" : self.amount() + " ") + scale(self.scale(), locale, self.amount());
	}

	public static String description(Predicate.Argument self, Locale locale) {
		return self.toString(locale);
	}

	public static String description(Predicate.Range self, Locale locale) {
		String values = descriptionValuesOf(self.lowBound(), self.highBound(), 0, locale);
		return (values.contains("..") ? message(locale, "between") + " " : "") + values.replace("..", (self.unit() == null ? "" : self.unit()) + " " + message(locale, "and") + " ") + (self.unit() == null ? "" : self.unit());
	}

	public static String description(Predicate.Numeric self, Locale locale) {
		return truncate(formatter.format(parseDouble(self.value())), self.decimals()) + (self.unit() == null ? "" : self.unit());
	}

	private static String scale(Scale self, Locale locale, int amount) {
		return message(locale, self.name().toLowerCase() + (amount > 1 ? "_plural" : "") + "_natural");
	}

	private static String modifier(Predicate.Modifier modifier, Locale locale) {
		if (modifier.equals(Predicate.Modifier.All)) return "";
		return " " + message(locale, modifier.name().toLowerCase());
	}

	public static String toString(Predicate.TimeRange self, Locale locale) {
		return " " + message(locale, "period") + " = " + self.from() + ".." + self.to();
	}

	public static String description(Predicate.TimeRange self, Locale locale) {
		return message(locale, "description.join.from") + " " + self.from() + " " + message(locale, "description.join.to") + " " + self.to();
	}

	private static String separator(Predicate self) {
		return self.modifier() != Predicate.Modifier.All || self.recency() != null || self.frequency() != null || self.period() != null ? " ::" : "";
	}

	private static String periodString(Predicate self, Locale locale) {
		return self.period() != null ? self.period().toString(locale) : "";
	}

	private static String periodDescription(Predicate self, Locale locale) {
		return self.period() != null ? self.period().description(locale) : "";
	}

	private static String frequencyString(Predicate self, Locale locale) {
		return self.frequency() != null ? self.frequency().description(locale) : "";
	}

	private static String recencyString(Predicate self, Locale locale) {
		return self.recency() != null ? self.recency().toString(locale) : "";
	}

	private static String recencyDescription(Predicate self, Locale locale) {
		Recency recency = self.recency();
		return recency.amount() + " " + scale(recency.scale(), locale, recency.amount()) + (recency.range().equals(Old) ? "" : " " + message(locale, recency.range().name().toLowerCase()));
	}

	private static String valuesOf(double lowBound, double highBound, int decimals) {
		if (lowBound == MIN_VALUE) return " <= " + highBound;
		else if (highBound == MAX_VALUE) return " >= " + lowBound;
		return " = " + truncate(format("%.2f", lowBound).replace(",", "."), decimals) + ".." + truncate(format("%.2f", highBound).replace(",", "."), decimals);
	}

	private static String descriptionValuesOf(double lowBound, double highBound, int decimals, Locale locale) {
		if (lowBound == MIN_VALUE || lowBound == Integer.MIN_VALUE)
			return message(locale, "lowerorequals") + " " + truncate(formatter.format(highBound), decimals);
		else if (highBound >= MAX_VALUE || highBound >= Integer.MAX_VALUE)
			return message(locale, "greaterorequals") + " " + truncate(formatter.format(lowBound), decimals);
		String low = truncate(formatter.format(lowBound), decimals);
		String high = truncate(formatter.format(highBound), decimals);
		if (low.equals(high))
			return message(locale, "equals") + " " + truncate(formatter.format(highBound) + "", decimals);
		return low + ".." + high;
	}

	private static String frequencyValuesOf(double lowBound, double highBound, int decimals, Locale locale) {
		if (lowBound == 0)
			return message(locale, "frequency.lower") + " " + truncate(formatter.format(highBound + 1), decimals);
		else if (highBound >= MAX_VALUE || highBound >= Integer.MAX_VALUE)
			return message(locale, "frequency.greater") + " " + truncate(formatter.format(lowBound - 1), decimals);
		String low = truncate(formatter.format(lowBound), decimals);
		String high = truncate(formatter.format(highBound), decimals);
		if (low.equals(high))
			return message(locale, "equals") + " " + truncate(formatter.format(highBound) + "", decimals);
		return low + ".." + high;
	}

	public static Instant fromInstant(Predicate.TimeRange self) {
		return instantOf(self.from());
	}

	public static Instant toInstant(Predicate.TimeRange self) {
		return instantOf(self.to());
	}

	private static Instant instantOf(String value) {
		int[] fields = toInt(value.split("-"));
		ZonedDateTime time = ZonedDateTime.of(fields[0], fields.length > 1 ? fields[1] : 1, fields.length > 2 ? fields[2] : 1, fields.length > 3 ? fields[3] : 0, fields.length > 4 ? fields[4] : 0, fields.length > 5 ? fields[5] : 0, 0, ZoneId.of("UTC"));
		return Instant.from(time);
	}

	private static int[] toInt(String[] strings) {
		int[] fields = new int[strings.length];
		for (int i = 0; i < strings.length; i++) {
			fields[i] = Integer.parseInt(strings[i]);
		}
		return fields;
	}

	private static DecimalFormat formatter() {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		DecimalFormat decimalFormat = new DecimalFormat();
		decimalFormat.setDecimalFormatSymbols(symbols);
		return decimalFormat;
	}
}