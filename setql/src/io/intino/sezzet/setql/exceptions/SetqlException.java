package io.intino.sezzet.setql.exceptions;

import java.util.ArrayList;
import java.util.List;

public abstract class SetqlException extends Exception {
	protected List<SetqlError> errors = new ArrayList<>();

	public SetqlException add(SetqlError error) {
		errors.add(error);
		return this;
	}

	public List<? extends SetqlError> errors() {
		return errors;
	}

}
