package io.intino.sezzet.setql.exceptions;

import java.util.List;

public class SemanticException extends SetqlException {
	public SemanticException add(SetqlError error) {
		errors.add(error);
		return this;
	}

	public SemanticException addAll(List<SetqlError> errors) {
		this.errors.addAll(errors);
		return this;
	}

	public static class SemanticError extends SetqlError {
		public SemanticError(int line, int column, String message) {
			super(line, column, message);
		}
	}

	@Override
	public String getMessage() {
		StringBuilder exception = new StringBuilder();
		for (SetqlError e : errors) exception.append("On line ").append(e.line).append(": ").append(e.message).append("\n");
		return exception.toString();
	}
}
