package io.intino.sezzet.setql;

import io.intino.sezzet.setql.parser.SetqlGrammar.*;
import io.intino.sezzet.setql.exceptions.SetqlError;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.Expression.Predicate;
import io.intino.sezzet.setql.graph.Expression.Predicate.Recency;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.graph.rules.Operator;
import io.intino.sezzet.setql.graph.rules.Scale;
import io.intino.sezzet.setql.parser.SetqlGrammar;
import io.intino.sezzet.setql.parser.SetqlGrammarBaseListener;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.Modifier.Commons;
import static io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.Modifier.Uncommons;
import static io.intino.sezzet.setql.graph.rules.Operator.fromText;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class SetqlModelGenerator extends SetqlGrammarBaseListener {
	private final SetqlGraph graph;
	private List<SetqlError> errors = new ArrayList<>();
	private Expression expression;
	private Stack<Expression> criterionStack = new Stack<>();
	private Predicate currentPredicate;

	SetqlModelGenerator(SetqlGraph graph) {
		this.graph = graph;
	}

	List<SetqlError> errors() {
		return errors;
	}

	Expression expression() {
		return expression;
	}

	public void enterCriterion(SetqlGrammar.CriterionContext ctx) {
		if (criterionStack.isEmpty()) {
			expression = graph.create().expression(ctx.getText());
			criterionStack.push(expression);
		} else {
			Expression.InnerExpression innerExpression = criterionStack.peek().create().innerExpression(ctx.STAR().getSymbol().getLine());
			addOperator(ctx.parent, innerExpression);
			criterionStack.push(innerExpression.create().expression(ctx.getText()));
		}
	}

	private void addOperator(RuleContext parent, Expression.InnerExpression expression) {
		expression.operator(operatorFrom(parent));
	}

	public void exitCriterion(SetqlGrammar.CriterionContext ctx) {
		criterionStack.pop();
	}

	public void enterPredicate(SetqlGrammar.PredicateContext ctx) {
		Expression expression = criterionStack.peek();
		final String feature = ctx.restriction().feature().getText();
		this.currentPredicate = expression.create().predicate(ctx.getStart().getLine(), feature);
		if (ctx.parent instanceof SetqlGrammar.InlinePredicateContext) addOperator(ctx.parent);
		enterArgument(ctx, currentPredicate);
	}

	private void addOperator(RuleContext parent) {
		this.currentPredicate.operator(operatorFrom(parent));
	}

	private void enterArgument(SetqlGrammar.PredicateContext ctx, Predicate predicate) {
		for (SetqlGrammar.ArgumentContext arg : ctx.restriction().argument())
			if (arg.range() != null) processRange(predicate, arg);
			else if (arg.comparator() != null) processRelativeRange(predicate, arg);
			else if (arg.STRING() != null) {
				String text = arg.STRING().getText();
				predicate.create().text(text.substring(1, text.length() - 1));
			} else if (arg.number() != null) predicate.create().numeric(arg.number().getText());
			else if (arg.enumerate() != null && !arg.enumerate().isEmpty())
				predicate.create().enum$(String.join(" ", textOf(arg.enumerate())));
			else if (arg.expression() != null) processVariableOperation(predicate, arg);
	}

	private String textOf(EnumerateContext arg) {
		StringBuilder enumerate = new StringBuilder();
		boolean lastWasDot = false;
		for (ParseTree child : arg.children)
			if (child.getText().equalsIgnoreCase(".")) {
				enumerate.append(child.getText());
				lastWasDot = true;
			} else {
				enumerate.append(lastWasDot || enumerate.length() == 0 ? "" : " ").append(child.getText());
				lastWasDot = false;
			}
		return enumerate.toString();
	}

	private void processVariableOperation(Predicate predicate, SetqlGrammar.ArgumentContext arg) {
		SetqlGrammar.ComputeContext expression = arg.expression().compute();
		Predicate.VariableOperation operation = predicate.create().variableOperation();
		if (expression instanceof ComparatorExpressionContext) {
			parseComparision((ComparatorExpressionContext) expression, operation);
		} else if (expression instanceof BinaryExpressionContext) {
			BinaryExpressionContext binary = (BinaryExpressionContext) expression;
			operation.operators().add(Operator.fromText(binary.op.getText()));
			parseBinary(binary, operation);
		}
	}

	private void parseComparision(ComparatorExpressionContext comparision, Predicate.VariableOperation operation) {
		if (comparision.left instanceof IdentifierExpressionContext || comparision.right instanceof IdentifierExpressionContext)
			operation.create().comparison(variableFrom(comparision), comparision.op.getText(), number(comparision));
	}

	private void parseBinary(BinaryExpressionContext binary, Predicate.VariableOperation operation) {
		parseComparision((ComparatorExpressionContext) binary.left, operation);
		parseComparision((ComparatorExpressionContext) binary.right, operation);
	}

	private String variableFrom(ComparatorExpressionContext comparision) {
		return comparision.left instanceof IdentifierExpressionContext ?
				((IdentifierExpressionContext) comparision.left).IDENTIFIER().getText() : ((IdentifierExpressionContext) comparision.right).IDENTIFIER().getText();
	}

	private String number(ComparatorExpressionContext comparision) {
		return comparision.left instanceof DecimalExpressionContext ?
				((DecimalExpressionContext) comparision.left).number().getText() : ((DecimalExpressionContext) comparision.right).number().getText();
	}

	private void processRelativeRange(Predicate predicate, SetqlGrammar.ArgumentContext arg) {
		String comparator = arg.comparator().getText();
		Predicate.Range range = predicate.create().range(rangeLowBound(arg.number().getText(), comparator), rangeHighBound(arg.number().getText(), comparator));
		if (!comparator.contains("=")) {
			if (comparator.contains("<")) range.closedHighBound(false);
			else range.closedLowBound(false);
		}
	}

	private double rangeLowBound(String number, String comparator) {
		return comparator.contains("<") ? Double.MIN_VALUE : parseDouble(number);
	}

	private double rangeHighBound(String number, String comparator) {
		return comparator.contains(">") ? Double.MAX_VALUE : parseDouble(number);
	}

	private void processRange(Predicate predicate, SetqlGrammar.ArgumentContext arg) {
		predicate.create().range(parseDouble(arg.range().rangeValue(0).getText()), parseDouble(arg.range().rangeValue(1).getText()));
	}

	public void enterPeriod(SetqlGrammar.PeriodContext ctx) {
		if (ctx.scale() != null)
			currentPredicate.create().fromNow(parseInt(ctx.NATURAL_VALUE().getText()), scale(ctx.scale()));
		else
			currentPredicate.create().timeRange(ctx.dateRange().dateValue().get(0).getText(), ctx.dateRange().dateValue().get(1).getText());
	}

	@Override
	public void enterModifier(ModifierContext ctx) {
		if (ctx.COMMONS() != null) currentPredicate.modifier(Commons);
		else if (ctx.UNCOMMONS() != null) currentPredicate.modifier(Uncommons);
	}

	public void enterFrequency(SetqlGrammar.FrequencyContext ctx) {
		Predicate.Frequency frequency;
		if (ctx.range() != null) {
			frequency = currentPredicate.create().frequency(
					parseInt(ctx.range() != null ? ctx.range().rangeValue(0).getText() : ctx.NATURAL_VALUE().getText()),
					ctx.range() != null ? parseInt(ctx.range().rangeValue(1).getText()) : Integer.MAX_VALUE);
		} else {
			double lowBound = lowBound(ctx.NATURAL_VALUE().getText(), ctx.comparator().getText());
			frequency = currentPredicate.create().frequency(lowBound < 0 ? 0 : (int) lowBound, (int) highBound(ctx.NATURAL_VALUE().getText(), ctx.comparator().getText()));
		}
		if (ctx.PERCENTAGE() != null) frequency.relative(true);
		if (ctx.CONSECUTIVE() != null) frequency.consecutives(true);
	}

	private double lowBound(String number, String comparator) {
		if (comparator.equals("=")) return parseDouble(number);
		if (comparator.contains("<")) return Double.MIN_VALUE;
		return comparator.contains("=") ? parseDouble(number) : parseDouble(number) + 1;
	}

	private double highBound(String number, String comparator) {
		if (comparator.equals("=")) return parseDouble(number);
		if (comparator.contains(">")) return Double.MAX_VALUE;
		return comparator.contains("=") ? parseDouble(number) : parseDouble(number) - 1;
	}

	private Operator operatorFrom(RuleContext parent) {
		if (parent.getParent().getParent() instanceof OperationContext)
			return fromText(((OperationContext) parent.getParent().getParent()).operator().getText());
		return Operator.OR;
	}

	public void enterRecency(SetqlGrammar.RecencyContext ctx) {
		currentPredicate.create().recency(parseInt(ctx.NATURAL_VALUE().getText()), scale(ctx.scale()), ctx.NEW() != null ? Recency.Range.New : Recency.Range.Old);
	}

	private Scale scale(SetqlGrammar.ScaleContext ctx) {
		if (ctx.DAY() != null) return Scale.Day;
		if (ctx.MONTH() != null) return Scale.Month;
		if (ctx.YEAR() != null) return Scale.Year;
		return Scale.Hour;
	}
}