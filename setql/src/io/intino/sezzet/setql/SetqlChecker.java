package io.intino.sezzet.setql;

import io.intino.magritte.framework.utils.UTF8Control;
import io.intino.sezzet.model.graph.Category;
import io.intino.sezzet.model.graph.Feature;
import io.intino.sezzet.model.graph.Range.Step;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.exceptions.SemanticException;
import io.intino.sezzet.setql.exceptions.SemanticException.SemanticError;
import io.intino.sezzet.setql.exceptions.SetqlError;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.Expression.Predicate;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.graph.rules.Scale;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static io.intino.sezzet.setql.graph.Expression.Predicate.*;

public class SetqlChecker {
	private final ResourceBundle messages;
	private List<SetqlError> errors = new ArrayList<>();
	private SezzetGraph sezzetGraph;

	public SetqlChecker(SezzetGraph graph, Locale locale) {
		this.sezzetGraph = graph;
		this.messages = ResourceBundle.getBundle("messages", locale, new UTF8Control());
	}

	public void check(SetqlGraph graph) throws SemanticException {
		check(graph.expression());
		if (!errors.isEmpty()) throw new SemanticException().addAll(errors);
	}

	private void check(Expression expression) {
		for (Expression.InnerExpression e : expression.innerExpressionList()) check(e.expression());
		for (Predicate predicate : expression.predicateList()) check(predicate);
	}

	private void check(Predicate predicate) {
		Feature feature = sezzetGraph.find(predicate.property());
		if (feature == null) {
			errors.add(new SemanticError(predicate.line(), 1, message("feature.not.found", predicate.property())));
			return;
		}
		if (feature.isSingleton()) {
			if (!predicate.argumentList().isEmpty())
				errors.add(new SemanticError(predicate.line(), 1, message("values.not.accepted", predicate.property())));
		} else {
			if (!feature.isSingleton() && predicate.argumentList().isEmpty())
				errors.add(new SemanticError(predicate.line(), 1, message("values.required", predicate.property())));
			for (Argument argument : predicate.argumentList()) check(argument, feature, predicate.line());
		}
		check(predicate.period(), feature, predicate.line());
		check(predicate.frequency(), feature, predicate.line());
		check(predicate.recency(), predicate.period(), feature, predicate.line());
	}

	private void check(Argument argument, Feature feature, int line) {
		if (argument.i$(VariableOperation.class)) {
//			errors.add(new SemanticError(line, 1, message("variable.is.out.of.range")));TODO
		} else if (feature.isEnumerate()) checkEnumerate(argument, feature, line);
		else if (feature.isNumeric()) {
			if (feature.asNumeric().unit() != null)
				if (argument instanceof Numeric)
					((Numeric) argument).unit(feature.asNumeric().unit()).decimals(feature.asNumeric().range().decimals());
				else if (argument instanceof Range)
					((Range) argument).unit(feature.asNumeric().unit()).decimals(feature.asNumeric().range().decimals());
			if (!argument.i$(Numeric.class) && !argument.i$(Range.class))
				errors.add(new SemanticError(line, 1, message("numeric.values.required", feature.label())));
			else {
				Feature.Numeric numeric = feature.asNumeric();
				if (argument.i$(Range.class)) {
					Range range = argument.a$(Range.class);
					if (range.lowBound() == Double.MIN_VALUE) range.lowBound(numeric.range().from());
					if (range.highBound() == Double.MAX_VALUE) range.highBound(numeric.range().to());
					correctDoubleRange(range, numeric);
					if (argument.a$(Range.class).lowBound() < numeric.range().from() || argument.a$(Range.class).highBound() > numeric.range().to())
						errors.add(new SemanticError(line, 1, message("numeric.range.value.out.of.bound", numeric.range().from(), numeric.range().to())));
				}
			}
		} else if (feature.isText() && !argument.i$(Text.class))
			errors.add(new SemanticError(line, 1, message("text.values.required", feature.label())));

	}

	private void correctDoubleRange(Range range, Feature.Numeric numeric) {
		if (!range.closedHighBound()) {
			range.highBound(range.highBound() - findStepValue(numeric.range(), range.highBound()));
		}
		if (!range.closedLowBound()) {
			range.lowBound(range.lowBound() + findStepValue(numeric.range(), range.lowBound()));
		}
	}

	private Double findStepValue(io.intino.sezzet.model.graph.Range range, double value) {
		if (range.stepList().isEmpty() && range.from() >= value && range.to() <= value) return range.stepSize();
		return range.stepList().stream().filter(step -> step.from() <= value && step.to() >= value).findFirst().map(Step::value).orElse(1.0);
	}

	private void variablesIn(String value) {

	}

	private void checkEnumerate(Argument argument, Feature feature, int line) {
		Category category = checkAsLeaf(argument, feature, line);
		if (category == null) category = checkAsComposite(argument, feature, line);
		if (category == null)
			errors.add(new SemanticError(line, 1, message("category.not.found", argument.a$(SingleValue.class).value())));
		else if (!isAnonymous(category)) argument.a$(SingleValue.class).value(category.label());
	}

	private boolean isAnonymous(Category category) {
		return category.name$().matches("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
	}

	private Category checkAsComposite(Argument argument, Feature feature, int line) {
		List<Category> compositeCategories = sezzetGraph.compositeCategoriesOf(feature);
		Category category = compositeCategories.stream().filter(v -> v.label().equalsIgnoreCase(argument.a$(SingleValue.class).value())).findFirst().orElse(null);
		if (category != null) argument.a$(Predicate.Enum.class).isComposedCategory(true);
		return category;
	}

	private Category checkAsLeaf(Argument argument, Feature feature, int line) {
		String value = argument.a$(SingleValue.class).value();
		return sezzetGraph.leafCategoriesOf(feature).
				stream().filter(v -> v.label().equalsIgnoreCase(value)).findFirst().orElse(null);
	}

	private void check(Period period, Feature feature, int line) {
		if (period == null && feature.isTimeless()) return;

		else if (period == null) {
			errors.add(new SemanticError(line, 1, message("period.required")));
			return;
		}
		if (period.i$(FromNow.class)) {
			checkPeriodFromNow(period.a$(FromNow.class), line);
		}
		if (period.i$(TimeRange.class)) {
			checkTimeRange(period.a$(TimeRange.class), line);
		}
	}

	private void checkTimeRange(TimeRange timeRange, int line) {
		Instant now = sezzetGraph.storeScale().scale().minus(Instant.now());
		io.intino.sezzet.model.graph.rules.Scale storeScale = sezzetGraph.storeScale().scale();

		if (timeRange.fromInstant().isAfter(now) || timeRange.toInstant().isAfter(now))
			errors.add(new SemanticError(line, 1, message("malformed.time.range")));
		String[] split = timeRange.from().split("-");
		Scale scale = Scale.values()[split.length - 1];
		if (scale.ordinal() > storeScale.ordinal())
			errors.add(new SemanticError(line, 1, message("scale.must.be.higher", storeScale.name())));
		if (storeScale.ordinal() != scale.ordinal()) transformScale(timeRange, scale, storeScale);
	}

	private void checkPeriodFromNow(FromNow period, int line) {
		Scale scale = period.scale();
		io.intino.sezzet.model.graph.rules.Scale storeScale = sezzetGraph.storeScale().scale();
		if (scale.ordinal() > storeScale.ordinal())
			errors.add(new SemanticError(line, 1, message("scale.must.be.higher", storeScale.name())));
		if (storeScale.ordinal() != scale.ordinal()) transformScale(period, scale, storeScale);
	}

	private void transformScale(FromNow period, Scale scale, io.intino.sezzet.model.graph.rules.Scale storeScale) {
		int value = period.amount();
		for (int i = scale.ordinal(); i < storeScale.ordinal(); i++)
			value = scale.toLowerScale(value);
		period.amount(value).scale(Scale.valueOf(storeScale.name()));
	}

	private void transformScale(TimeRange period, Scale scale, io.intino.sezzet.model.graph.rules.Scale storeScale) {
		String to = period.to();
		String from = period.from();
		for (int i = scale.ordinal(); i < storeScale.ordinal(); i++) {
			from = scale.toLowerScaleStart(from);
			to = scale.toLowerScaleEnding(to);
		}
		period.from(from);
		period.to(to);
	}

	private void check(Frequency frequency, Feature feature, int line) {
		if (frequency == null) return;
		if (frequency.highBound() < 0 || frequency.lowBound() < 0 || frequency.lowBound() > frequency.highBound())
			errors.add(new SemanticError(line, 1, message("malformed.frequency")));
	}

	private void check(Recency recency, Period period, Feature feature, int line) {
		if (recency == null) return;
		if (period == null) errors.add(new SemanticError(line, 1, message("recency.without.period")));
		else if (period.i$(FromNow.class) && period.a$(FromNow.class).amount() < recency.amount())
			errors.add(new SemanticError(line, 1, message("recency.out.of.period")));
		checkScale(recency, line);
	}

	private void checkScale(Recency recency, int line) {
		Scale scale = recency.scale();
		io.intino.sezzet.model.graph.rules.Scale storeScale = sezzetGraph.storeScale().scale();
		if (scale.ordinal() > storeScale.ordinal())
			errors.add(new SemanticError(line, 1, message("scale.must.be.higher", storeScale.name())));
		if (storeScale.ordinal() != scale.ordinal()) transformScale(recency, scale, storeScale);
	}

	private void transformScale(Recency recency, Scale scale, io.intino.sezzet.model.graph.rules.Scale storeScale) {
		int value = recency.amount();
		for (int i = scale.ordinal(); i < storeScale.ordinal(); i++)
			value = scale.toLowerScale(value);
		recency.amount(value).scale(Scale.valueOf(storeScale.name()));
	}

	private String message(String key, Object... parameters) {
		return MessageFormat.format(new String(messages.getString(key).getBytes(), StandardCharsets.UTF_8), parameters);
	}
}