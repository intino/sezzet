package io.intino.sezzet.setql;

import io.intino.magritte.framework.utils.UTF8Control;
import io.intino.sezzet.setql.exceptions.ErrorStrategy;
import io.intino.sezzet.setql.exceptions.GrammarErrorListener;
import io.intino.sezzet.setql.exceptions.SemanticException;
import io.intino.sezzet.setql.exceptions.SyntaxException;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.parser.SetqlENLexicon;
import io.intino.sezzet.setql.parser.SetqlESLexicon;
import io.intino.sezzet.setql.parser.SetqlGrammar;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.antlr.v4.runtime.CharStreams.fromString;

public class SetqlParser {

	private final String input;
	private final ResourceBundle messages;
	private SetqlGrammar grammar;
	private SetqlGrammar.RootContext rootContext;

	public SetqlParser(String input, Locale locale) {
		this.input = input;
		this.messages = ResourceBundle.getBundle("messages", locale, new UTF8Control());
		if (input == null || input.isEmpty()) return;
		Lexer lexer = lexer(locale);
		lexer.reset();
		this.grammar = new SetqlGrammar(new CommonTokenStream(lexer));
		this.grammar.setErrorHandler(new ErrorStrategy());
		this.grammar.addErrorListener(new GrammarErrorListener());
	}

	public void parse(SetqlGraph setqlGraph) throws SyntaxException, SemanticException {
		if (rootContext == null) parse();
		walk(new ParseTreeWalker(), new SetqlModelGenerator(setqlGraph));
	}

	public void check(SetqlGraph setqlGraph) throws SyntaxException, SemanticException {
		if (rootContext == null) parse();
		walk(new ParseTreeWalker(), new SetqlModelGenerator(setqlGraph));
	}

	private void walk(ParseTreeWalker walker, SetqlModelGenerator generator) throws SyntaxException, SemanticException {
		try {
			if (rootContext == null) return;
			walker.walk(generator, rootContext);
			if (!generator.errors().isEmpty()) throw new SemanticException().addAll(generator.errors());
			generator.expression().raw(this.input);
		} catch (RecognitionException e) {
			Token token = ((Parser) e.getRecognizer()).getCurrentToken();
			throw new SyntaxException().add(new SyntaxException.SyntaxError(messages, token.getLine(), token.getCharPositionInLine(), token.getText(), getExpectedTokens((Parser) e.getRecognizer())));
		}
	}

	void parse() throws SyntaxException {
		try {
			rootContext = grammar.root();
		} catch (RecognitionException e) {
			Parser recognizer = (Parser) e.getRecognizer();
			Token token = recognizer.getCurrentToken();
			throw new SyntaxException().add(new SyntaxException.SyntaxError(messages, token.getLine(), token.getCharPositionInLine(), token.getText(), getExpectedTokens(recognizer)));
		} catch (NullPointerException ignored) {
		}
	}

	private String getExpectedTokens(Parser recognizer) {
		try {
			return recognizer.getExpectedTokens().toString(VocabularyImpl.fromTokenNames(recognizer.getTokenNames()));
		} catch (Exception e) {
			return "";
		}
	}

	private Lexer lexer(Locale locale) {
		if (locale.getCountry().equals("ES")) return new SetqlESLexicon(fromString(input));
		else return new SetqlENLexicon(fromString(input));
	}
}
