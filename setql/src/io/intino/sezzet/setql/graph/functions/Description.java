package io.intino.sezzet.setql.graph.functions;

import java.util.Locale;

@FunctionalInterface
public interface Description {
	String toString(Locale locale, String subject);
}
