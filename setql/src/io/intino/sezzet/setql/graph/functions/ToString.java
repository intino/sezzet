package io.intino.sezzet.setql.graph.functions;

import java.util.Locale;

@FunctionalInterface
public interface ToString {

	String toString(Locale locale);
}
