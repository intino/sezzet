package io.intino.sezzet.setql.graph;

import io.intino.magritte.framework.Graph;

public class SetqlGraph extends io.intino.sezzet.setql.graph.AbstractGraph {

	public SetqlGraph(Graph graph) {
		super(graph);
	}

	public SetqlGraph(io.intino.magritte.framework.Graph graph, SetqlGraph wrapper) {
		super(graph, wrapper);
	}

	public Expression.Operand findOperand(int line) {
		return findOperand(this.expression(), line);
	}

	private Expression.Operand findOperand(Expression expression, int line) {
		for (Expression.Operand operand : expression.operandList()) {
			if (operand.line() == line) return operand;
			if (operand.i$(Expression.InnerExpression.class)) {
				Expression.Operand innerOperand = findOperand(operand.a$(Expression.InnerExpression.class).expression(), line);
				if (innerOperand != null) return innerOperand;
			}
		}
		return null;
	}
}