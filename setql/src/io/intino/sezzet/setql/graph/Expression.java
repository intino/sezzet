package io.intino.sezzet.setql.graph;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class Expression extends AbstractExpression {

	public Expression(io.intino.magritte.framework.Node node) {
		super(node);
	}

	public void replace(Operand oldOperand, Operand newOperand) {
		int operandIndex = operandList().indexOf(oldOperand);
		oldOperand.delete$();
		operandList.remove(newOperand);
		operandList.add(operandIndex, newOperand);
	}

	@Override
	public List<InnerExpression> innerExpressionList() {
		return super.operandList().stream().filter(o -> o instanceof InnerExpression).map(o -> (InnerExpression) o).collect(toList());
	}

	@Override
	public List<Predicate> predicateList() {
		return super.operandList().stream().filter(o -> o instanceof Predicate).map(o -> (Predicate) o).collect(toList());
	}

	public static abstract class Operand extends AbstractOperand {
		public Operand(io.intino.magritte.framework.Node node) {
			super(node);
		}
	}

	public static class InnerExpression extends AbstractInnerExpression {
		public InnerExpression(io.intino.magritte.framework.Node node) {
			super(node);
		}
	}

	public static class Predicate extends AbstractPredicate {
		public Predicate(io.intino.magritte.framework.Node node) {
			super(node);
		}

		public static abstract class Argument extends AbstractArgument {
			public Argument(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static abstract class Period extends AbstractPeriod {
			public Period(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class Frequency extends AbstractFrequency {
			public Frequency(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class Recency extends AbstractRecency {
			public Recency(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class Range extends AbstractRange {
			public Range(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static abstract class SingleValue extends AbstractSingleValue {
			public SingleValue(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class Numeric extends AbstractNumeric {
			public Numeric(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class Text extends AbstractText {
			public Text(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class Enum extends AbstractEnum {
			public Enum(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class VariableOperation extends AbstractVariableOperation {
			public VariableOperation(io.intino.magritte.framework.Node node) {
				super(node);
			}

			public static class Comparison extends AbstractComparison {
				public Comparison(io.intino.magritte.framework.Node node) {
					super(node);
				}
			}
		}

		public static class FromNow extends AbstractFromNow {
			public FromNow(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}

		public static class TimeRange extends AbstractTimeRange {
			public TimeRange(io.intino.magritte.framework.Node node) {
				super(node);
			}
		}
	}
}