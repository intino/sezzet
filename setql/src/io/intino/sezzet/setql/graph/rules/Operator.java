package io.intino.sezzet.setql.graph.rules;


import io.intino.magritte.lang.model.Rule;

import java.util.Arrays;

public enum Operator implements Rule<Enum> {

	OR("|"), AND("&"), DIFF("-"), NAND("%");

	String value;

	Operator(String value) {
		this.value = value;
	}

	public static Operator fromText(String text) {
		return Arrays.stream(Operator.values()).filter(value -> value.value.equalsIgnoreCase(text)).findFirst().orElse(null);
	}

	@Override
	public boolean accept(Enum value) {
		return value instanceof Operator;
	}

	public String value() {
		return value;
	}
}
