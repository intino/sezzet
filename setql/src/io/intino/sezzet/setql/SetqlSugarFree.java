package io.intino.sezzet.setql;

import io.intino.sezzet.model.graph.Category;
import io.intino.sezzet.model.graph.Feature;
import io.intino.sezzet.model.graph.SezzetGraph;
import io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.Modifier;
import io.intino.sezzet.setql.graph.Expression;
import io.intino.sezzet.setql.graph.Expression.InnerExpression;
import io.intino.sezzet.setql.graph.Expression.Operand;
import io.intino.sezzet.setql.graph.Expression.Predicate;
import io.intino.sezzet.setql.graph.Expression.Predicate.Enum;
import io.intino.sezzet.setql.graph.Expression.Predicate.*;
import io.intino.sezzet.setql.graph.SetqlGraph;
import io.intino.sezzet.setql.graph.rules.Operator;
import io.intino.sezzet.setql.graph.rules.Scale;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.AbstractRecency.Range.Old;
import static io.intino.sezzet.setql.graph.AbstractExpression.AbstractPredicate.Modifier.*;
import static java.util.Collections.singletonList;

class SetqlSugarFree {
	private SezzetGraph sezzet;

	SetqlSugarFree(SezzetGraph sezzet) {
		this.sezzet = sezzet;
	}

	public void free(SetqlGraph graph) {
		free(graph.expression());
	}

	private void free(Expression expression) {
		for (Expression.InnerExpression e : expression.innerExpressionList()) free(e.expression());
		for (Predicate predicate : new ArrayList<>(expression.predicateList())) free(predicate);
	}

	private void free(Predicate predicate) {
		Feature feature = sezzet.find(predicate.property());
		ArrayList<Enum> list = new ArrayList<>(predicate.enumList());
		for (Enum argument : list)
			if (argument.isComposedCategory()) freeComposedValue(argument, categoryFrom(feature, argument));
		for (Enum argument : list)
			if (argument.isComposedCategory()) argument.delete$();
		if (!predicate.modifier().equals(All)) freeModifier(predicate);
		if (predicate.recency() != null) freeRecency(predicate);
	}

	private void freeComposedValue(Argument argument, Category value) {
		List<Category> categories = sezzet.leafCategoriesOf(singletonList(value));
		Predicate predicate = argument.core$().ownerAs(Predicate.class);
		for (Category c : categories) predicate.create().enum$(c.label());
	}

	private void freeModifier(Predicate predicate) {
		Modifier modifier = predicate.modifier();
		if (modifier.equals(Commons)) predicate.argumentOperator(Operator.AND);
		else if (modifier.equals(Uncommons)) predicate.argumentOperator(Operator.NAND);
		predicate.modifier(All);
	}

	private void freeRecency(Predicate predicate) {
		substitute(predicate.core$().ownerAs(Expression.class), predicate, createInnerExpression(predicate, predicate.core$().ownerAs(Expression.class)));
	}

	private void substitute(Expression expression, Operand predicate, Operand newExpression) {
		expression.replace(predicate, newExpression);
	}

	private InnerExpression createInnerExpression(Predicate predicate, Expression expression) {
		Recency recency = predicate.recency();
		InnerExpression innerExpression = expression.create().innerExpression(predicate.line());
		Expression wrappedExpression = innerExpression.create().expression("");
		Predicate p1 = wrappedExpression.create().predicate(predicate.line(), predicate.property());
		Predicate p2 = wrappedExpression.create().predicate(predicate.line(), predicate.property());
		copyArguments(predicate, p1, p2);
		Scale scale = Scale.valueOf(sezzet.storeScale().scale().name());
		if (predicate.period().i$(Predicate.FromNow.class)) {
			Predicate.FromNow fromNow = predicate.period().a$(Predicate.FromNow.class);
			p1.create().timeRange(scale.label(bound(fromNow.amount() - 1, scale)), scale.label(scale.minus(Instant.now())));
			if (recency.range().equals(Old)) p2.create().fromNow(recency.amount(), scale);
			else
				p2.create().timeRange(scale.label(bound(recency.amount(), scale)), scale.label(bound(1, scale)));
		} else {
			TimeRange timeRange = predicate.period().a$(TimeRange.class);
			p1.create().timeRange(timeRange.from(), timeRange.to());
			if (recency.range().equals(Old))
				p2.create().timeRange(scale.label(bound(recency.amount(), scale, timeRange.toInstant())), timeRange.to());
			else
				p2.create().timeRange(timeRange.from(), scale.label(bound(recency.amount(), scale, timeRange.toInstant())));
		}
		p1.operator(predicate.operator());
		p2.operator(Operator.DIFF);
		return innerExpression;
	}

	private Instant bound(int amount, Scale scale) {
		Instant now = Instant.now();
		for (int i = 0; i <= amount; i++) now = scale.minus(now);
		return now;
	}

	private Instant bound(int amount, Scale scale, Instant reference) {
		Instant instant = reference;
		for (int i = 0; i <= amount; i++) instant = scale.minus(instant);
		return instant;
	}

	private void copyArguments(Predicate predicate, Predicate p1, Predicate p2) {
		for (Argument argument : predicate.argumentList()) copyArgument(argument, Arrays.asList(p1, p2));
	}

	private void copyArgument(Argument argument, List<Predicate> destinations) {
		if (argument.i$(Predicate.Numeric.class))
			destinations.forEach(d -> d.create().numeric(argument.a$(Predicate.Numeric.class).value()));
		else if (argument.i$(Text.class)) destinations.forEach(d -> d.create().text(argument.a$(Text.class).value()));
		else if (argument.i$(Enum.class)) destinations.forEach(d -> d.create().enum$(argument.a$(Enum.class).value()));
		else if (argument.i$(Range.class))
			destinations.forEach(d -> d.create().range(argument.a$(Range.class).lowBound(), argument.a$(Range.class).highBound()));
	}

	private Category categoryFrom(Feature feature, Enum argument) {
		return sezzet.compositeCategoriesOf(feature).stream().filter(v -> v.label().equalsIgnoreCase(argument.value())).findFirst().orElse(null);
	}
}