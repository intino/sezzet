lexer grammar SetqlLexicon;

@lexer::members {
public static class BlockManager {

	private int level;
	private int tabSize;
	private Token[] tokens;

	public BlockManager() {
		this.tokens = new Token[]{};
		this.level = 0;
		this.tabSize = 4;
	}

	public void reset() {
		this.tokens = new Token[]{};
		this.level = 0;
	}

	public void newlineAndSpaces(String text) {
		int newLevel = spacesLength(text) / this.tabSize;
		this.tokens = indentationTokens(newLevel - level, true);
		this.level = newLevel;
	}

	private int spacesLength(String text) {
		int value = 0;
		for (int i = 0; i < text.length(); i++)
			value += text.charAt(i) == '\t' ? this.tabSize : 1;
		return value;
	}

	private Token[] indentationTokens(int size, boolean addLastNewline) {
		if (size > 0)
			return create(Token.NEWLINE_INDENT);
		else {
			int length = !addLastNewline ? Math.abs(size * 2) : Math.abs(size * 2) + 1;
			return createDedents(length);
		}
	}

	private Token[] createDedents(int size) {
		Token[] actions = new Token[size];
		for (int i = 0; i < actions.length; i++)
			actions[i] = i % 2 == 0 ? Token.NEWLINE : Token.DEDENT;
		return actions;
	}

	private Token[] create(Token token) {
		return new Token[]{token};
	}

	public Token[] actions() {
		return java.util.Arrays.copyOf(tokens, tokens.length);
	}

	public void openBracket(int size) {
		this.tokens = indentationTokens(size, false);
		this.level += size;
	}

	public void semicolon(int size) {
		if (size == 1)
			this.tokens = create(Token.NEWLINE);
		else
			this.tokens = create(Token.ERROR);
	}

	public void eof() {
		this.tokens = indentationTokens(-level, false);
		this.level--;
	}

	public enum Token {
		NEWLINE_INDENT, DEDENT, NEWLINE, ERROR
	}
}

BlockManager blockManager = new BlockManager();
private static java.util.Queue<Token> queue = new java.util.LinkedList<>();

@Override
public void reset() {
    super.reset();
    queue.clear();
    blockManager.reset();
}

@Override
public void emit(Token token) {
    if (token.getType() == EOF) eof();
    queue.offer(token);
    setToken(token);
}

@Override
public Token nextToken() {
    super.nextToken();
    return queue.isEmpty()? emitEOF() : queue.poll();
}

private void emitToken(int ttype) {
    setType(ttype);
    emit();
}

private boolean isWhiteLineOrEOF() {
    int character = _input.LA(1);
    return (character == -1 || (char) character == '\n');
}

private void newlinesAndSpaces() {
    if (!isWhiteLineOrEOF()) {
        blockManager.newlineAndSpaces(getTextSpaces(getText()));
        sendTokens();
    }
    else skip();
}

private String getTextSpaces(String text) {
    int index = (text.indexOf(' ') == -1)? text.indexOf('\t') : text.indexOf(' ');
    return (index == -1)? "" : text.substring(index);
}

private void inline() {
    blockManager.openBracket(getText().length());
    sendTokens();
}

private void semicolon() {
    blockManager.semicolon(getText().length());
    sendTokens();
}

private void eof() {
    blockManager.eof();
    sendTokens();
}

private void sendTokens() {
    blockManager.actions();
    for (BlockManager.Token token : blockManager.actions())
        emitToken(translate(token));
}

private int translate(BlockManager.Token token) {
    if (token.toString().equals("NEWLINE")) return NEWLINE;
    if (token.toString().equals("DEDENT")) return DEDENT;
    if (token.toString().equals("NEWLINE_INDENT")) return NEW_LINE_INDENT;
    return UNKNOWN_TOKEN;
}

}
PERIOD              : 'period';
FREQUENCY           : 'frequency';
RECENCY             : 'recency';
CONSECUTIVE         : 'consecutives';

COMMONS             : 'commons';
UNCOMMONS           : 'uncommons';

YEAR                : 'y';
MONTH               : 'm';
DAY                 : 'd';
HOUR                : 'h';

LPAREN    			: '(';
RPAREN   			: ')';

HASHTAG             : '#';
COLON               : ':';
COMMA               : ',';
DOT                 : '.';
STAR                : '*';

NEW                : 'new';
OLD                : 'old';

DASH                : '-';
AND                 : '&';
OR                  : '|';
PLUS                : '+';
PERCENTAGE          : '%';

TRUE       : 'true' ;
FALSE      : 'false' ;
NOT        : '!' ;
GT         : '>' ;
GE         : '>=' ;
LT         : '<' ;
LE         : '<=';
EQ         : '=';

EXPRESSION_TOKEN	:'\'';

NATURAL_VALUE       : PLUS? DIGIT+;
NEGATIVE_VALUE      : DASH DIGIT+;
DOUBLE_VALUE        : (PLUS | DASH)? DIGIT+ DOT DIGIT+;
STRING         		: '"' (~('"' | '\\') | '\\' ('"' | '\\'))* '"';
IDENTIFIER          : (LETTER | UNDERDASH) (DIGIT | LETTER | DASH | UNDERDASH)*;
UNDERDASH           : '_';


DIGIT               : [0-9];
LETTER              : 'a'..'z' | 'A'..'Z' | 'ñ' | 'Ñ' | 'á'| 'é'| 'í' | 'ó' | 'ú'| 'Á'| 'É'| 'Í' | 'Ó' | 'Ú';

INDENT              :'\t';
NEWLINE             : NL+ SP* { newlinesAndSpaces(); };
SPACES: SP+ EOF? 	-> channel(HIDDEN);
SP                  : (' ' | INDENT);
NL                  : ('\r'? '\n' | '\r');

NEW_LINE_INDENT: 'indent';
DEDENT         : 'dedent';
UNKNOWN_TOKEN: . ;

QUOTE_BEGIN        : '%QUOTE_BEGIN%';
QUOTE_END          : '%QUOTE_END%';

fragment DOLLAR              : '$';
fragment EURO                : '€';
fragment GRADE               : 'º'| '°';
fragment BY                  : '·';

