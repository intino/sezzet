// Generated from /Users/oroncal/workspace/sezzet/setql/src/io/intino/sezzet/setql/SetqlGrammar.g4 by ANTLR 4.8
package io.intino.sezzet.setql.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SetqlGrammar}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 *            operations with no return type.
 */
public interface SetqlGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#root}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoot(SetqlGrammar.RootContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#criterion}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCriterion(SetqlGrammar.CriterionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#operation}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation(SetqlGrammar.OperationContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#operand}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperand(SetqlGrammar.OperandContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#wrappedCriterion}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWrappedCriterion(SetqlGrammar.WrappedCriterionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#inlinePredicate}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInlinePredicate(SetqlGrammar.InlinePredicateContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#predicate}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(SetqlGrammar.PredicateContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#restriction}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestriction(SetqlGrammar.RestrictionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#argument}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(SetqlGrammar.ArgumentContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#enumerate}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumerate(SetqlGrammar.EnumerateContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#expression}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(SetqlGrammar.ExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code binaryExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryExpression(SetqlGrammar.BinaryExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code decimalExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimalExpression(SetqlGrammar.DecimalExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpression(SetqlGrammar.BoolExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierExpression(SetqlGrammar.IdentifierExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpression(SetqlGrammar.NotExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpression(SetqlGrammar.ParenExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code comparatorExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparatorExpression(SetqlGrammar.ComparatorExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#comparator}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparator(SetqlGrammar.ComparatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#binary}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary(SetqlGrammar.BinaryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#bool}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool(SetqlGrammar.BoolContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#operator}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(SetqlGrammar.OperatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#feature}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFeature(SetqlGrammar.FeatureContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#constrains}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstrains(SetqlGrammar.ConstrainsContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#frequency}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrequency(SetqlGrammar.FrequencyContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#recency}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecency(SetqlGrammar.RecencyContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#period}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPeriod(SetqlGrammar.PeriodContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#modifier}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModifier(SetqlGrammar.ModifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#range}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRange(SetqlGrammar.RangeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#dateRange}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateRange(SetqlGrammar.DateRangeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#dateValue}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateValue(SetqlGrammar.DateValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#rangeValue}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRangeValue(SetqlGrammar.RangeValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#number}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(SetqlGrammar.NumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#scale}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScale(SetqlGrammar.ScaleContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#integerValue}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerValue(SetqlGrammar.IntegerValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link SetqlGrammar#doubleValue}.
	 *
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoubleValue(SetqlGrammar.DoubleValueContext ctx);
}