// Generated from /Users/oroncal/workspace/sezzet/setql/src/io/intino/sezzet/setql/SetqlGrammar.g4 by ANTLR 4.8
package io.intino.sezzet.setql.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SetqlGrammar}.
 */
public interface SetqlGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#root}.
	 *
	 * @param ctx the parse tree
	 */
	void enterRoot(SetqlGrammar.RootContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#root}.
	 *
	 * @param ctx the parse tree
	 */
	void exitRoot(SetqlGrammar.RootContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#criterion}.
	 *
	 * @param ctx the parse tree
	 */
	void enterCriterion(SetqlGrammar.CriterionContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#criterion}.
	 *
	 * @param ctx the parse tree
	 */
	void exitCriterion(SetqlGrammar.CriterionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#operation}.
	 *
	 * @param ctx the parse tree
	 */
	void enterOperation(SetqlGrammar.OperationContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#operation}.
	 *
	 * @param ctx the parse tree
	 */
	void exitOperation(SetqlGrammar.OperationContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#operand}.
	 *
	 * @param ctx the parse tree
	 */
	void enterOperand(SetqlGrammar.OperandContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#operand}.
	 *
	 * @param ctx the parse tree
	 */
	void exitOperand(SetqlGrammar.OperandContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#wrappedCriterion}.
	 *
	 * @param ctx the parse tree
	 */
	void enterWrappedCriterion(SetqlGrammar.WrappedCriterionContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#wrappedCriterion}.
	 *
	 * @param ctx the parse tree
	 */
	void exitWrappedCriterion(SetqlGrammar.WrappedCriterionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#inlinePredicate}.
	 *
	 * @param ctx the parse tree
	 */
	void enterInlinePredicate(SetqlGrammar.InlinePredicateContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#inlinePredicate}.
	 *
	 * @param ctx the parse tree
	 */
	void exitInlinePredicate(SetqlGrammar.InlinePredicateContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#predicate}.
	 *
	 * @param ctx the parse tree
	 */
	void enterPredicate(SetqlGrammar.PredicateContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#predicate}.
	 *
	 * @param ctx the parse tree
	 */
	void exitPredicate(SetqlGrammar.PredicateContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#restriction}.
	 *
	 * @param ctx the parse tree
	 */
	void enterRestriction(SetqlGrammar.RestrictionContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#restriction}.
	 *
	 * @param ctx the parse tree
	 */
	void exitRestriction(SetqlGrammar.RestrictionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#argument}.
	 *
	 * @param ctx the parse tree
	 */
	void enterArgument(SetqlGrammar.ArgumentContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#argument}.
	 *
	 * @param ctx the parse tree
	 */
	void exitArgument(SetqlGrammar.ArgumentContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#enumerate}.
	 *
	 * @param ctx the parse tree
	 */
	void enterEnumerate(SetqlGrammar.EnumerateContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#enumerate}.
	 *
	 * @param ctx the parse tree
	 */
	void exitEnumerate(SetqlGrammar.EnumerateContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#expression}.
	 *
	 * @param ctx the parse tree
	 */
	void enterExpression(SetqlGrammar.ExpressionContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#expression}.
	 *
	 * @param ctx the parse tree
	 */
	void exitExpression(SetqlGrammar.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code binaryExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterBinaryExpression(SetqlGrammar.BinaryExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code binaryExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitBinaryExpression(SetqlGrammar.BinaryExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code decimalExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterDecimalExpression(SetqlGrammar.DecimalExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code decimalExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitDecimalExpression(SetqlGrammar.DecimalExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(SetqlGrammar.BoolExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(SetqlGrammar.BoolExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(SetqlGrammar.IdentifierExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(SetqlGrammar.IdentifierExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterNotExpression(SetqlGrammar.NotExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitNotExpression(SetqlGrammar.NotExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterParenExpression(SetqlGrammar.ParenExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitParenExpression(SetqlGrammar.ParenExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code comparatorExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void enterComparatorExpression(SetqlGrammar.ComparatorExpressionContext ctx);

	/**
	 * Exit a parse tree produced by the {@code comparatorExpression}
	 * labeled alternative in {@link SetqlGrammar#compute}.
	 *
	 * @param ctx the parse tree
	 */
	void exitComparatorExpression(SetqlGrammar.ComparatorExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#comparator}.
	 *
	 * @param ctx the parse tree
	 */
	void enterComparator(SetqlGrammar.ComparatorContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#comparator}.
	 *
	 * @param ctx the parse tree
	 */
	void exitComparator(SetqlGrammar.ComparatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#binary}.
	 *
	 * @param ctx the parse tree
	 */
	void enterBinary(SetqlGrammar.BinaryContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#binary}.
	 *
	 * @param ctx the parse tree
	 */
	void exitBinary(SetqlGrammar.BinaryContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#bool}.
	 *
	 * @param ctx the parse tree
	 */
	void enterBool(SetqlGrammar.BoolContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#bool}.
	 *
	 * @param ctx the parse tree
	 */
	void exitBool(SetqlGrammar.BoolContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#operator}.
	 *
	 * @param ctx the parse tree
	 */
	void enterOperator(SetqlGrammar.OperatorContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#operator}.
	 *
	 * @param ctx the parse tree
	 */
	void exitOperator(SetqlGrammar.OperatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#feature}.
	 *
	 * @param ctx the parse tree
	 */
	void enterFeature(SetqlGrammar.FeatureContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#feature}.
	 *
	 * @param ctx the parse tree
	 */
	void exitFeature(SetqlGrammar.FeatureContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#constrains}.
	 *
	 * @param ctx the parse tree
	 */
	void enterConstrains(SetqlGrammar.ConstrainsContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#constrains}.
	 *
	 * @param ctx the parse tree
	 */
	void exitConstrains(SetqlGrammar.ConstrainsContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#frequency}.
	 *
	 * @param ctx the parse tree
	 */
	void enterFrequency(SetqlGrammar.FrequencyContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#frequency}.
	 *
	 * @param ctx the parse tree
	 */
	void exitFrequency(SetqlGrammar.FrequencyContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#recency}.
	 *
	 * @param ctx the parse tree
	 */
	void enterRecency(SetqlGrammar.RecencyContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#recency}.
	 *
	 * @param ctx the parse tree
	 */
	void exitRecency(SetqlGrammar.RecencyContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#period}.
	 *
	 * @param ctx the parse tree
	 */
	void enterPeriod(SetqlGrammar.PeriodContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#period}.
	 *
	 * @param ctx the parse tree
	 */
	void exitPeriod(SetqlGrammar.PeriodContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#modifier}.
	 *
	 * @param ctx the parse tree
	 */
	void enterModifier(SetqlGrammar.ModifierContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#modifier}.
	 *
	 * @param ctx the parse tree
	 */
	void exitModifier(SetqlGrammar.ModifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#range}.
	 *
	 * @param ctx the parse tree
	 */
	void enterRange(SetqlGrammar.RangeContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#range}.
	 *
	 * @param ctx the parse tree
	 */
	void exitRange(SetqlGrammar.RangeContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#dateRange}.
	 *
	 * @param ctx the parse tree
	 */
	void enterDateRange(SetqlGrammar.DateRangeContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#dateRange}.
	 *
	 * @param ctx the parse tree
	 */
	void exitDateRange(SetqlGrammar.DateRangeContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#dateValue}.
	 *
	 * @param ctx the parse tree
	 */
	void enterDateValue(SetqlGrammar.DateValueContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#dateValue}.
	 *
	 * @param ctx the parse tree
	 */
	void exitDateValue(SetqlGrammar.DateValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#rangeValue}.
	 *
	 * @param ctx the parse tree
	 */
	void enterRangeValue(SetqlGrammar.RangeValueContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#rangeValue}.
	 *
	 * @param ctx the parse tree
	 */
	void exitRangeValue(SetqlGrammar.RangeValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#number}.
	 *
	 * @param ctx the parse tree
	 */
	void enterNumber(SetqlGrammar.NumberContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#number}.
	 *
	 * @param ctx the parse tree
	 */
	void exitNumber(SetqlGrammar.NumberContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#scale}.
	 *
	 * @param ctx the parse tree
	 */
	void enterScale(SetqlGrammar.ScaleContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#scale}.
	 *
	 * @param ctx the parse tree
	 */
	void exitScale(SetqlGrammar.ScaleContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#integerValue}.
	 *
	 * @param ctx the parse tree
	 */
	void enterIntegerValue(SetqlGrammar.IntegerValueContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#integerValue}.
	 *
	 * @param ctx the parse tree
	 */
	void exitIntegerValue(SetqlGrammar.IntegerValueContext ctx);

	/**
	 * Enter a parse tree produced by {@link SetqlGrammar#doubleValue}.
	 *
	 * @param ctx the parse tree
	 */
	void enterDoubleValue(SetqlGrammar.DoubleValueContext ctx);

	/**
	 * Exit a parse tree produced by {@link SetqlGrammar#doubleValue}.
	 *
	 * @param ctx the parse tree
	 */
	void exitDoubleValue(SetqlGrammar.DoubleValueContext ctx);
}