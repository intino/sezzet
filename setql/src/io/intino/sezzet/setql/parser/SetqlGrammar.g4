parser grammar SetqlGrammar;

options { tokenVocab=SetqlLexicon; }


root: criterion NEWLINE* EOF;

criterion: STAR operand operation*;
operation: NEWLINE operator operand;
operand: wrappedCriterion | inlinePredicate;

wrappedCriterion: NEW_LINE_INDENT criterion NEWLINE? DEDENT;

inlinePredicate: INDENT predicate;

predicate: restriction constrains?;

restriction: feature (LPAREN (argument (COMMA argument)*)? RPAREN)?;
argument: enumerate | range | (comparator? number) | STRING | expression;

enumerate: (IDENTIFIER | DOT)+;
expression: EXPRESSION_TOKEN compute EXPRESSION_TOKEN;

compute
	: LPAREN compute RPAREN						#parenExpression
	| NOT compute								#notExpression
	| left=compute op=comparator right=compute  #comparatorExpression
	| left=compute op=binary right=compute 		#binaryExpression
	| bool										#boolExpression
	| IDENTIFIER								#identifierExpression
	| number									#decimalExpression
	;

comparator: GT | GE | LT | LE | EQ;

binary: AND | OR;

bool: TRUE | FALSE;

operator: binary | DASH | PERCENTAGE;

feature: IDENTIFIER (DOT IDENTIFIER)*;

constrains: COLON COLON modifier? (period | frequency | recency)*;

frequency: FREQUENCY ((EQ range) | (comparator NATURAL_VALUE)) PERCENTAGE? CONSECUTIVE?;
recency: RECENCY EQ NATURAL_VALUE scale (NEW | OLD)?;
period: PERIOD EQ ((NATURAL_VALUE scale) | dateRange);

modifier: (COMMONS | UNCOMMONS)?;

range: rangeValue DOT DOT rangeValue;
dateRange: dateValue DOT DOT dateValue;
dateValue: NATURAL_VALUE NEGATIVE_VALUE? NEGATIVE_VALUE?;

rangeValue: number | STAR;
number: doubleValue | integerValue;
scale: YEAR | MONTH | DAY | HOUR;

integerValue : NATURAL_VALUE | NEGATIVE_VALUE;
doubleValue  : (NATURAL_VALUE | NEGATIVE_VALUE | DOUBLE_VALUE);
