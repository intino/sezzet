// Generated from /Users/oroncal/workspace/sezzet/setql/src/io/intino/sezzet/setql/parser/SetqlENLexicon.g4 by ANTLR 4.8
package io.intino.sezzet.setql.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SetqlENLexicon extends Lexer {
	static {
		RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION);
	}

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
			new PredictionContextCache();
	public static final int
			PERIOD = 1, FREQUENCY = 2, RECENCY = 3, CONSECUTIVE = 4, COMMONS = 5, UNCOMMONS = 6,
			YEAR = 7, MONTH = 8, DAY = 9, HOUR = 10, LPAREN = 11, RPAREN = 12, HASHTAG = 13, COLON = 14,
			COMMA = 15, DOT = 16, STAR = 17, NEW = 18, OLD = 19, DASH = 20, AND = 21, OR = 22, PLUS = 23,
			PERCENTAGE = 24, TRUE = 25, FALSE = 26, NOT = 27, GT = 28, GE = 29, LT = 30, LE = 31,
			EQ = 32, EXPRESSION_TOKEN = 33, NATURAL_VALUE = 34, NEGATIVE_VALUE = 35, DOUBLE_VALUE = 36,
			STRING = 37, IDENTIFIER = 38, UNDERDASH = 39, DIGIT = 40, LETTER = 41, INDENT = 42,
			NEWLINE = 43, SPACES = 44, SP = 45, NL = 46, NEW_LINE_INDENT = 47, DEDENT = 48, UNKNOWN_TOKEN = 49,
			QUOTE_BEGIN = 50, QUOTE_END = 51;
	public static String[] channelNames = {
			"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
			"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[]{
				"PERIOD", "FREQUENCY", "RECENCY", "CONSECUTIVE", "COMMONS", "UNCOMMONS",
				"YEAR", "MONTH", "DAY", "HOUR", "LPAREN", "RPAREN", "HASHTAG", "COLON",
				"COMMA", "DOT", "STAR", "NEW", "OLD", "DASH", "AND", "OR", "PLUS", "PERCENTAGE",
				"TRUE", "FALSE", "NOT", "GT", "GE", "LT", "LE", "EQ", "EXPRESSION_TOKEN",
				"NATURAL_VALUE", "NEGATIVE_VALUE", "DOUBLE_VALUE", "STRING", "IDENTIFIER",
				"UNDERDASH", "DIGIT", "LETTER", "INDENT", "NEWLINE", "SPACES", "SP",
				"NL", "NEW_LINE_INDENT", "DEDENT", "UNKNOWN_TOKEN", "QUOTE_BEGIN", "QUOTE_END",
				"DOLLAR", "EURO", "GRADE", "BY"
		};
	}

	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[]{
				null, "'period'", "'frequency'", "'recency'", "'consecutives'", "'commons'",
				"'uncommons'", null, null, null, null, "'('", "')'", "'#'", "':'", "','",
				"'.'", "'*'", "'new'", "'old'", "'-'", "'&'", "'|'", "'+'", "'%'", "'true'",
				"'false'", "'!'", "'>'", "'>='", "'<'", "'<='", "'='", "'''", null, null,
				null, null, null, "'_'", null, null, "'\t'", null, null, null, null,
				"'indent'", "'dedent'", null, "'%QUOTE_BEGIN%'", "'%QUOTE_END%'"
		};
	}

	private static final String[] _LITERAL_NAMES = makeLiteralNames();

	private static String[] makeSymbolicNames() {
		return new String[]{
				null, "PERIOD", "FREQUENCY", "RECENCY", "CONSECUTIVE", "COMMONS", "UNCOMMONS",
				"YEAR", "MONTH", "DAY", "HOUR", "LPAREN", "RPAREN", "HASHTAG", "COLON",
				"COMMA", "DOT", "STAR", "NEW", "OLD", "DASH", "AND", "OR", "PLUS", "PERCENTAGE",
				"TRUE", "FALSE", "NOT", "GT", "GE", "LT", "LE", "EQ", "EXPRESSION_TOKEN",
				"NATURAL_VALUE", "NEGATIVE_VALUE", "DOUBLE_VALUE", "STRING", "IDENTIFIER",
				"UNDERDASH", "DIGIT", "LETTER", "INDENT", "NEWLINE", "SPACES", "SP",
				"NL", "NEW_LINE_INDENT", "DEDENT", "UNKNOWN_TOKEN", "QUOTE_BEGIN", "QUOTE_END"
		};
	}

	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;

	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public static class BlockManager {

		private int level;
		private int tabSize;
		private Token[] tokens;

		public BlockManager() {
			this.tokens = new Token[]{};
			this.level = 0;
			this.tabSize = 4;
		}

		public void reset() {
			this.tokens = new Token[]{};
			this.level = 0;
		}

		public void newlineAndSpaces(String text) {
			int newLevel = spacesLength(text) / this.tabSize;
			this.tokens = indentationTokens(newLevel - level, true);
			this.level = newLevel;
		}

		private int spacesLength(String text) {
			int value = 0;
			for (int i = 0; i < text.length(); i++)
				value += text.charAt(i) == '\t' ? this.tabSize : 1;
			return value;
		}

		private Token[] indentationTokens(int size, boolean addLastNewline) {
			if (size > 0)
				return create(Token.NEWLINE_INDENT);
			else {
				int length = !addLastNewline ? Math.abs(size * 2) : Math.abs(size * 2) + 1;
				return createDedents(length);
			}
		}

		private Token[] createDedents(int size) {
			Token[] actions = new Token[size];
			for (int i = 0; i < actions.length; i++)
				actions[i] = i % 2 == 0 ? Token.NEWLINE : Token.DEDENT;
			return actions;
		}

		private Token[] create(Token token) {
			return new Token[]{token};
		}

		public Token[] actions() {
			return java.util.Arrays.copyOf(tokens, tokens.length);
		}

		public void openBracket(int size) {
			this.tokens = indentationTokens(size, false);
			this.level += size;
		}

		public void semicolon(int size) {
			if (size == 1)
				this.tokens = create(Token.NEWLINE);
			else
				this.tokens = create(Token.ERROR);
		}

		public void eof() {
			this.tokens = indentationTokens(-level, false);
			this.level--;
		}

		public enum Token {
			NEWLINE_INDENT, DEDENT, NEWLINE, ERROR
		}
	}

	BlockManager blockManager = new BlockManager();
	private static java.util.Queue<Token> queue = new java.util.LinkedList<>();

	@Override
	public void reset() {
		super.reset();
		queue.clear();
		blockManager.reset();
	}

	@Override
	public void emit(Token token) {
		if (token.getType() == EOF) eof();
		queue.offer(token);
		setToken(token);
	}

	@Override
	public Token nextToken() {
		super.nextToken();
		return queue.isEmpty() ? emitEOF() : queue.poll();
	}

	private void emitToken(int ttype) {
		setType(ttype);
		emit();
	}

	private boolean isWhiteLineOrEOF() {
		int character = _input.LA(1);
		return (character == -1 || (char) character == '\n');
	}

	private void newlinesAndSpaces() {
		if (!isWhiteLineOrEOF()) {
			blockManager.newlineAndSpaces(getTextSpaces(getText()));
			sendTokens();
		} else skip();
	}

	private String getTextSpaces(String text) {
		int index = (text.indexOf(' ') == -1) ? text.indexOf('\t') : text.indexOf(' ');
		return (index == -1) ? "" : text.substring(index);
	}

	private void inline() {
		blockManager.openBracket(getText().length());
		sendTokens();
	}

	private void semicolon() {
		blockManager.semicolon(getText().length());
		sendTokens();
	}

	private void eof() {
		blockManager.eof();
		sendTokens();
	}

	private void sendTokens() {
		blockManager.actions();
		for (BlockManager.Token token : blockManager.actions())
			emitToken(translate(token));
	}

	private int translate(BlockManager.Token token) {
		if (token.toString().equals("NEWLINE")) return NEWLINE;
		if (token.toString().equals("DEDENT")) return DEDENT;
		if (token.toString().equals("NEWLINE_INDENT")) return NEW_LINE_INDENT;
		return UNKNOWN_TOKEN;
	}


	public SetqlENLexicon(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
	}

	@Override
	public String getGrammarFileName() {
		return "SetqlENLexicon.g4";
	}

	@Override
	public String[] getRuleNames() {
		return ruleNames;
	}

	@Override
	public String getSerializedATN() {
		return _serializedATN;
	}

	@Override
	public String[] getChannelNames() {
		return channelNames;
	}

	@Override
	public String[] getModeNames() {
		return modeNames;
	}

	@Override
	public ATN getATN() {
		return _ATN;
	}

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
			case 42:
				NEWLINE_action((RuleContext) _localctx, actionIndex);
				break;
		}
	}

	private void NEWLINE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
			case 0:
				newlinesAndSpaces();
				break;
		}
	}

	public static final String _serializedATN =
			"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\65\u01a5\b\1\4\2" +
					"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4" +
					"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22" +
					"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31" +
					"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t" +
					" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t" +
					"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64" +
					"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\3\2\3\2\3\2\3\2\3\2\3\2\3\2" +
					"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3" +
					"\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6" +
					"\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3" +
					"\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00b4\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3" +
					"\t\3\t\3\t\3\t\3\t\5\t\u00c2\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00cc" +
					"\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00d8\n\13" +
					"\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23" +
					"\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30" +
					"\3\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33" +
					"\3\34\3\34\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3" +
					"#\5#\u0116\n#\3#\6#\u0119\n#\r#\16#\u011a\3$\3$\6$\u011f\n$\r$\16$\u0120" +
					"\3%\3%\5%\u0125\n%\3%\6%\u0128\n%\r%\16%\u0129\3%\3%\6%\u012e\n%\r%\16" +
					"%\u012f\3&\3&\3&\3&\7&\u0136\n&\f&\16&\u0139\13&\3&\3&\3\'\3\'\5\'\u013f" +
					"\n\'\3\'\3\'\3\'\3\'\7\'\u0145\n\'\f\'\16\'\u0148\13\'\3(\3(\3)\3)\3*" +
					"\3*\3+\3+\3,\6,\u0153\n,\r,\16,\u0154\3,\7,\u0158\n,\f,\16,\u015b\13," +
					"\3,\3,\3-\6-\u0160\n-\r-\16-\u0161\3-\5-\u0165\n-\3-\3-\3.\3.\5.\u016b" +
					"\n.\3/\5/\u016e\n/\3/\3/\5/\u0172\n/\3\60\3\60\3\60\3\60\3\60\3\60\3\60" +
					"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\63\3\63\3\63\3\63\3\63" +
					"\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\64\3\64\3\64\3\64\3\64" +
					"\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67\38\3" +
					"8\2\29\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17" +
					"\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\35" +
					"9\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\2" +
					"k\2m\2o\2\3\2\6\4\2$$^^\3\2\62;\20\2C\\c|\u00c3\u00c3\u00cb\u00cb\u00cf" +
					"\u00cf\u00d3\u00d3\u00d5\u00d5\u00dc\u00dc\u00e3\u00e3\u00eb\u00eb\u00ef" +
					"\u00ef\u00f3\u00f3\u00f5\u00f5\u00fc\u00fc\4\2\u00b2\u00b2\u00bc\u00bc" +
					"\2\u01bd\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2" +
					"\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27" +
					"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2" +
					"\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2" +
					"\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2" +
					"\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2" +
					"\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S" +
					"\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2" +
					"\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\3q\3\2\2\2\5x\3\2\2\2" +
					"\7\u0082\3\2\2\2\t\u008a\3\2\2\2\13\u0097\3\2\2\2\r\u009f\3\2\2\2\17\u00b3" +
					"\3\2\2\2\21\u00c1\3\2\2\2\23\u00cb\3\2\2\2\25\u00d7\3\2\2\2\27\u00d9\3" +
					"\2\2\2\31\u00db\3\2\2\2\33\u00dd\3\2\2\2\35\u00df\3\2\2\2\37\u00e1\3\2" +
					"\2\2!\u00e3\3\2\2\2#\u00e5\3\2\2\2%\u00e7\3\2\2\2\'\u00eb\3\2\2\2)\u00ef" +
					"\3\2\2\2+\u00f1\3\2\2\2-\u00f3\3\2\2\2/\u00f5\3\2\2\2\61\u00f7\3\2\2\2" +
					"\63\u00f9\3\2\2\2\65\u00fe\3\2\2\2\67\u0104\3\2\2\29\u0106\3\2\2\2;\u0108" +
					"\3\2\2\2=\u010b\3\2\2\2?\u010d\3\2\2\2A\u0110\3\2\2\2C\u0112\3\2\2\2E" +
					"\u0115\3\2\2\2G\u011c\3\2\2\2I\u0124\3\2\2\2K\u0131\3\2\2\2M\u013e\3\2" +
					"\2\2O\u0149\3\2\2\2Q\u014b\3\2\2\2S\u014d\3\2\2\2U\u014f\3\2\2\2W\u0152" +
					"\3\2\2\2Y\u015f\3\2\2\2[\u016a\3\2\2\2]\u0171\3\2\2\2_\u0173\3\2\2\2a" +
					"\u017a\3\2\2\2c\u0181\3\2\2\2e\u0183\3\2\2\2g\u0191\3\2\2\2i\u019d\3\2" +
					"\2\2k\u019f\3\2\2\2m\u01a1\3\2\2\2o\u01a3\3\2\2\2qr\7r\2\2rs\7g\2\2st" +
					"\7t\2\2tu\7k\2\2uv\7q\2\2vw\7f\2\2w\4\3\2\2\2xy\7h\2\2yz\7t\2\2z{\7g\2" +
					"\2{|\7s\2\2|}\7w\2\2}~\7g\2\2~\177\7p\2\2\177\u0080\7e\2\2\u0080\u0081" +
					"\7{\2\2\u0081\6\3\2\2\2\u0082\u0083\7t\2\2\u0083\u0084\7g\2\2\u0084\u0085" +
					"\7e\2\2\u0085\u0086\7g\2\2\u0086\u0087\7p\2\2\u0087\u0088\7e\2\2\u0088" +
					"\u0089\7{\2\2\u0089\b\3\2\2\2\u008a\u008b\7e\2\2\u008b\u008c\7q\2\2\u008c" +
					"\u008d\7p\2\2\u008d\u008e\7u\2\2\u008e\u008f\7g\2\2\u008f\u0090\7e\2\2" +
					"\u0090\u0091\7w\2\2\u0091\u0092\7v\2\2\u0092\u0093\7k\2\2\u0093\u0094" +
					"\7x\2\2\u0094\u0095\7g\2\2\u0095\u0096\7u\2\2\u0096\n\3\2\2\2\u0097\u0098" +
					"\7e\2\2\u0098\u0099\7q\2\2\u0099\u009a\7o\2\2\u009a\u009b\7o\2\2\u009b" +
					"\u009c\7q\2\2\u009c\u009d\7p\2\2\u009d\u009e\7u\2\2\u009e\f\3\2\2\2\u009f" +
					"\u00a0\7w\2\2\u00a0\u00a1\7p\2\2\u00a1\u00a2\7e\2\2\u00a2\u00a3\7q\2\2" +
					"\u00a3\u00a4\7o\2\2\u00a4\u00a5\7o\2\2\u00a5\u00a6\7q\2\2\u00a6\u00a7" +
					"\7p\2\2\u00a7\u00a8\7u\2\2\u00a8\16\3\2\2\2\u00a9\u00b4\7{\2\2\u00aa\u00ab" +
					"\7{\2\2\u00ab\u00ac\7g\2\2\u00ac\u00ad\7c\2\2\u00ad\u00b4\7t\2\2\u00ae" +
					"\u00af\7{\2\2\u00af\u00b0\7g\2\2\u00b0\u00b1\7c\2\2\u00b1\u00b2\7t\2\2" +
					"\u00b2\u00b4\7u\2\2\u00b3\u00a9\3\2\2\2\u00b3\u00aa\3\2\2\2\u00b3\u00ae" +
					"\3\2\2\2\u00b4\20\3\2\2\2\u00b5\u00c2\7o\2\2\u00b6\u00b7\7o\2\2\u00b7" +
					"\u00b8\7q\2\2\u00b8\u00b9\7p\2\2\u00b9\u00ba\7v\2\2\u00ba\u00c2\7j\2\2" +
					"\u00bb\u00bc\7o\2\2\u00bc\u00bd\7q\2\2\u00bd\u00be\7p\2\2\u00be\u00bf" +
					"\7v\2\2\u00bf\u00c0\7j\2\2\u00c0\u00c2\7u\2\2\u00c1\u00b5\3\2\2\2\u00c1" +
					"\u00b6\3\2\2\2\u00c1\u00bb\3\2\2\2\u00c2\22\3\2\2\2\u00c3\u00cc\7f\2\2" +
					"\u00c4\u00c5\7f\2\2\u00c5\u00c6\7c\2\2\u00c6\u00cc\7{\2\2\u00c7\u00c8" +
					"\7f\2\2\u00c8\u00c9\7c\2\2\u00c9\u00ca\7{\2\2\u00ca\u00cc\7u\2\2\u00cb" +
					"\u00c3\3\2\2\2\u00cb\u00c4\3\2\2\2\u00cb\u00c7\3\2\2\2\u00cc\24\3\2\2" +
					"\2\u00cd\u00d8\7j\2\2\u00ce\u00cf\7j\2\2\u00cf\u00d0\7q\2\2\u00d0\u00d1" +
					"\7w\2\2\u00d1\u00d8\7t\2\2\u00d2\u00d3\7j\2\2\u00d3\u00d4\7q\2\2\u00d4" +
					"\u00d5\7w\2\2\u00d5\u00d6\7t\2\2\u00d6\u00d8\7u\2\2\u00d7\u00cd\3\2\2" +
					"\2\u00d7\u00ce\3\2\2\2\u00d7\u00d2\3\2\2\2\u00d8\26\3\2\2\2\u00d9\u00da" +
					"\7*\2\2\u00da\30\3\2\2\2\u00db\u00dc\7+\2\2\u00dc\32\3\2\2\2\u00dd\u00de" +
					"\7%\2\2\u00de\34\3\2\2\2\u00df\u00e0\7<\2\2\u00e0\36\3\2\2\2\u00e1\u00e2" +
					"\7.\2\2\u00e2 \3\2\2\2\u00e3\u00e4\7\60\2\2\u00e4\"\3\2\2\2\u00e5\u00e6" +
					"\7,\2\2\u00e6$\3\2\2\2\u00e7\u00e8\7p\2\2\u00e8\u00e9\7g\2\2\u00e9\u00ea" +
					"\7y\2\2\u00ea&\3\2\2\2\u00eb\u00ec\7q\2\2\u00ec\u00ed\7n\2\2\u00ed\u00ee" +
					"\7f\2\2\u00ee(\3\2\2\2\u00ef\u00f0\7/\2\2\u00f0*\3\2\2\2\u00f1\u00f2\7" +
					"(\2\2\u00f2,\3\2\2\2\u00f3\u00f4\7~\2\2\u00f4.\3\2\2\2\u00f5\u00f6\7-" +
					"\2\2\u00f6\60\3\2\2\2\u00f7\u00f8\7\'\2\2\u00f8\62\3\2\2\2\u00f9\u00fa" +
					"\7v\2\2\u00fa\u00fb\7t\2\2\u00fb\u00fc\7w\2\2\u00fc\u00fd\7g\2\2\u00fd" +
					"\64\3\2\2\2\u00fe\u00ff\7h\2\2\u00ff\u0100\7c\2\2\u0100\u0101\7n\2\2\u0101" +
					"\u0102\7u\2\2\u0102\u0103\7g\2\2\u0103\66\3\2\2\2\u0104\u0105\7#\2\2\u0105" +
					"8\3\2\2\2\u0106\u0107\7@\2\2\u0107:\3\2\2\2\u0108\u0109\7@\2\2\u0109\u010a" +
					"\7?\2\2\u010a<\3\2\2\2\u010b\u010c\7>\2\2\u010c>\3\2\2\2\u010d\u010e\7" +
					">\2\2\u010e\u010f\7?\2\2\u010f@\3\2\2\2\u0110\u0111\7?\2\2\u0111B\3\2" +
					"\2\2\u0112\u0113\7)\2\2\u0113D\3\2\2\2\u0114\u0116\5/\30\2\u0115\u0114" +
					"\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0118\3\2\2\2\u0117\u0119\5Q)\2\u0118" +
					"\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011a\u0118\3\2\2\2\u011a\u011b\3\2" +
					"\2\2\u011bF\3\2\2\2\u011c\u011e\5)\25\2\u011d\u011f\5Q)\2\u011e\u011d" +
					"\3\2\2\2\u011f\u0120\3\2\2\2\u0120\u011e\3\2\2\2\u0120\u0121\3\2\2\2\u0121" +
					"H\3\2\2\2\u0122\u0125\5/\30\2\u0123\u0125\5)\25\2\u0124\u0122\3\2\2\2" +
					"\u0124\u0123\3\2\2\2\u0124\u0125\3\2\2\2\u0125\u0127\3\2\2\2\u0126\u0128" +
					"\5Q)\2\u0127\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129\u0127\3\2\2\2\u0129" +
					"\u012a\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u012d\5!\21\2\u012c\u012e\5Q" +
					")\2\u012d\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u012d\3\2\2\2\u012f" +
					"\u0130\3\2\2\2\u0130J\3\2\2\2\u0131\u0137\7$\2\2\u0132\u0136\n\2\2\2\u0133" +
					"\u0134\7^\2\2\u0134\u0136\t\2\2\2\u0135\u0132\3\2\2\2\u0135\u0133\3\2" +
					"\2\2\u0136\u0139\3\2\2\2\u0137\u0135\3\2\2\2\u0137\u0138\3\2\2\2\u0138" +
					"\u013a\3\2\2\2\u0139\u0137\3\2\2\2\u013a\u013b\7$\2\2\u013bL\3\2\2\2\u013c" +
					"\u013f\5S*\2\u013d\u013f\5O(\2\u013e\u013c\3\2\2\2\u013e\u013d\3\2\2\2" +
					"\u013f\u0146\3\2\2\2\u0140\u0145\5Q)\2\u0141\u0145\5S*\2\u0142\u0145\5" +
					")\25\2\u0143\u0145\5O(\2\u0144\u0140\3\2\2\2\u0144\u0141\3\2\2\2\u0144" +
					"\u0142\3\2\2\2\u0144\u0143\3\2\2\2\u0145\u0148\3\2\2\2\u0146\u0144\3\2" +
					"\2\2\u0146\u0147\3\2\2\2\u0147N\3\2\2\2\u0148\u0146\3\2\2\2\u0149\u014a" +
					"\7a\2\2\u014aP\3\2\2\2\u014b\u014c\t\3\2\2\u014cR\3\2\2\2\u014d\u014e" +
					"\t\4\2\2\u014eT\3\2\2\2\u014f\u0150\7\13\2\2\u0150V\3\2\2\2\u0151\u0153" +
					"\5]/\2\u0152\u0151\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0152\3\2\2\2\u0154" +
					"\u0155\3\2\2\2\u0155\u0159\3\2\2\2\u0156\u0158\5[.\2\u0157\u0156\3\2\2" +
					"\2\u0158\u015b\3\2\2\2\u0159\u0157\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u015c" +
					"\3\2\2\2\u015b\u0159\3\2\2\2\u015c\u015d\b,\2\2\u015dX\3\2\2\2\u015e\u0160" +
					"\5[.\2\u015f\u015e\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u015f\3\2\2\2\u0161" +
					"\u0162\3\2\2\2\u0162\u0164\3\2\2\2\u0163\u0165\7\2\2\3\u0164\u0163\3\2" +
					"\2\2\u0164\u0165\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0167\b-\3\2\u0167" +
					"Z\3\2\2\2\u0168\u016b\7\"\2\2\u0169\u016b\5U+\2\u016a\u0168\3\2\2\2\u016a" +
					"\u0169\3\2\2\2\u016b\\\3\2\2\2\u016c\u016e\7\17\2\2\u016d\u016c\3\2\2" +
					"\2\u016d\u016e\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u0172\7\f\2\2\u0170\u0172" +
					"\7\17\2\2\u0171\u016d\3\2\2\2\u0171\u0170\3\2\2\2\u0172^\3\2\2\2\u0173" +
					"\u0174\7k\2\2\u0174\u0175\7p\2\2\u0175\u0176\7f\2\2\u0176\u0177\7g\2\2" +
					"\u0177\u0178\7p\2\2\u0178\u0179\7v\2\2\u0179`\3\2\2\2\u017a\u017b\7f\2" +
					"\2\u017b\u017c\7g\2\2\u017c\u017d\7f\2\2\u017d\u017e\7g\2\2\u017e\u017f" +
					"\7p\2\2\u017f\u0180\7v\2\2\u0180b\3\2\2\2\u0181\u0182\13\2\2\2\u0182d" +
					"\3\2\2\2\u0183\u0184\7\'\2\2\u0184\u0185\7S\2\2\u0185\u0186\7W\2\2\u0186" +
					"\u0187\7Q\2\2\u0187\u0188\7V\2\2\u0188\u0189\7G\2\2\u0189\u018a\7a\2\2" +
					"\u018a\u018b\7D\2\2\u018b\u018c\7G\2\2\u018c\u018d\7I\2\2\u018d\u018e" +
					"\7K\2\2\u018e\u018f\7P\2\2\u018f\u0190\7\'\2\2\u0190f\3\2\2\2\u0191\u0192" +
					"\7\'\2\2\u0192\u0193\7S\2\2\u0193\u0194\7W\2\2\u0194\u0195\7Q\2\2\u0195" +
					"\u0196\7V\2\2\u0196\u0197\7G\2\2\u0197\u0198\7a\2\2\u0198\u0199\7G\2\2" +
					"\u0199\u019a\7P\2\2\u019a\u019b\7F\2\2\u019b\u019c\7\'\2\2\u019ch\3\2" +
					"\2\2\u019d\u019e\7&\2\2\u019ej\3\2\2\2\u019f\u01a0\7\u20ae\2\2\u01a0l" +
					"\3\2\2\2\u01a1\u01a2\t\5\2\2\u01a2n\3\2\2\2\u01a3\u01a4\7\u00b9\2\2\u01a4" +
					"p\3\2\2\2\31\2\u00b3\u00c1\u00cb\u00d7\u0115\u011a\u0120\u0124\u0129\u012f" +
					"\u0135\u0137\u013e\u0144\u0146\u0154\u0159\u0161\u0164\u016a\u016d\u0171" +
					"\4\3,\2\2\3\2";
	public static final ATN _ATN =
			new ATNDeserializer().deserialize(_serializedATN.toCharArray());

	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}