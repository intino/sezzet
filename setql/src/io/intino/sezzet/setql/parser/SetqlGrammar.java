// Generated from /Users/oroncal/workspace/sezzet/setql/src/io/intino/sezzet/setql/SetqlGrammar.g4 by ANTLR 4.8
package io.intino.sezzet.setql.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SetqlGrammar extends Parser {
	static {
		RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION);
	}

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
			new PredictionContextCache();
	public static final int
			PERIOD = 1, FREQUENCY = 2, RECENCY = 3, CONSECUTIVE = 4, COMMONS = 5, UNCOMMONS = 6,
			YEAR = 7, MONTH = 8, DAY = 9, HOUR = 10, LPAREN = 11, RPAREN = 12, HASHTAG = 13, COLON = 14,
			COMMA = 15, DOT = 16, STAR = 17, NEW = 18, OLD = 19, DASH = 20, AND = 21, OR = 22, PLUS = 23,
			PERCENTAGE = 24, TRUE = 25, FALSE = 26, NOT = 27, GT = 28, GE = 29, LT = 30, LE = 31,
			EQ = 32, EXPRESSION_TOKEN = 33, NATURAL_VALUE = 34, NEGATIVE_VALUE = 35, DOUBLE_VALUE = 36,
			STRING = 37, IDENTIFIER = 38, UNDERDASH = 39, DIGIT = 40, LETTER = 41, INDENT = 42,
			NEWLINE = 43, SPACES = 44, SP = 45, NL = 46, NEW_LINE_INDENT = 47, DEDENT = 48, UNKNOWN_TOKEN = 49,
			QUOTE_BEGIN = 50, QUOTE_END = 51;
	public static final int
			RULE_root = 0, RULE_criterion = 1, RULE_operation = 2, RULE_operand = 3,
			RULE_wrappedCriterion = 4, RULE_inlinePredicate = 5, RULE_predicate = 6,
			RULE_restriction = 7, RULE_argument = 8, RULE_enumerate = 9, RULE_expression = 10,
			RULE_compute = 11, RULE_comparator = 12, RULE_binary = 13, RULE_bool = 14,
			RULE_operator = 15, RULE_feature = 16, RULE_constrains = 17, RULE_frequency = 18,
			RULE_recency = 19, RULE_period = 20, RULE_modifier = 21, RULE_range = 22,
			RULE_dateRange = 23, RULE_dateValue = 24, RULE_rangeValue = 25, RULE_number = 26,
			RULE_scale = 27, RULE_integerValue = 28, RULE_doubleValue = 29;

	private static String[] makeRuleNames() {
		return new String[]{
				"root", "criterion", "operation", "operand", "wrappedCriterion", "inlinePredicate",
				"predicate", "restriction", "argument", "enumerate", "expression", "compute",
				"comparator", "binary", "bool", "operator", "feature", "constrains",
				"frequency", "recency", "period", "modifier", "range", "dateRange", "dateValue",
				"rangeValue", "number", "scale", "integerValue", "doubleValue"
		};
	}

	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[]{
				null, "'period'", "'frequency'", "'recency'", "'consecutives'", "'commons'",
				"'uncommons'", "'y'", "'m'", "'d'", "'h'", "'('", "')'", "'#'", "':'",
				"','", "'.'", "'*'", "'new'", "'old'", "'-'", "'&'", "'|'", "'+'", "'%'",
				"'true'", "'false'", "'!'", "'>'", "'>='", "'<'", "'<='", "'='", "'''",
				null, null, null, null, null, "'_'", null, null, "'\t'", null, null,
				null, null, "'indent'", "'dedent'", null, "'%QUOTE_BEGIN%'", "'%QUOTE_END%'"
		};
	}

	private static final String[] _LITERAL_NAMES = makeLiteralNames();

	private static String[] makeSymbolicNames() {
		return new String[]{
				null, "PERIOD", "FREQUENCY", "RECENCY", "CONSECUTIVE", "COMMONS", "UNCOMMONS",
				"YEAR", "MONTH", "DAY", "HOUR", "LPAREN", "RPAREN", "HASHTAG", "COLON",
				"COMMA", "DOT", "STAR", "NEW", "OLD", "DASH", "AND", "OR", "PLUS", "PERCENTAGE",
				"TRUE", "FALSE", "NOT", "GT", "GE", "LT", "LE", "EQ", "EXPRESSION_TOKEN",
				"NATURAL_VALUE", "NEGATIVE_VALUE", "DOUBLE_VALUE", "STRING", "IDENTIFIER",
				"UNDERDASH", "DIGIT", "LETTER", "INDENT", "NEWLINE", "SPACES", "SP",
				"NL", "NEW_LINE_INDENT", "DEDENT", "UNKNOWN_TOKEN", "QUOTE_BEGIN", "QUOTE_END"
		};
	}

	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;

	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() {
		return "SetqlGrammar.g4";
	}

	@Override
	public String[] getRuleNames() {
		return ruleNames;
	}

	@Override
	public String getSerializedATN() {
		return _serializedATN;
	}

	@Override
	public ATN getATN() {
		return _ATN;
	}

	public SetqlGrammar(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
	}

	public static class RootContext extends ParserRuleContext {
		public CriterionContext criterion() {
			return getRuleContext(CriterionContext.class, 0);
		}

		public TerminalNode EOF() {
			return getToken(SetqlGrammar.EOF, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(SetqlGrammar.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(SetqlGrammar.NEWLINE, i);
		}

		public RootContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_root;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterRoot(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitRoot(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitRoot(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RootContext root() throws RecognitionException {
		RootContext _localctx = new RootContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_root);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(60);
				criterion();
				setState(64);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == NEWLINE) {
					{
						{
							setState(61);
							match(NEWLINE);
						}
					}
					setState(66);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(67);
				match(EOF);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CriterionContext extends ParserRuleContext {
		public TerminalNode STAR() {
			return getToken(SetqlGrammar.STAR, 0);
		}

		public OperandContext operand() {
			return getRuleContext(OperandContext.class, 0);
		}

		public List<OperationContext> operation() {
			return getRuleContexts(OperationContext.class);
		}

		public OperationContext operation(int i) {
			return getRuleContext(OperationContext.class, i);
		}

		public CriterionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_criterion;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterCriterion(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitCriterion(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitCriterion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CriterionContext criterion() throws RecognitionException {
		CriterionContext _localctx = new CriterionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_criterion);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(69);
				match(STAR);
				setState(70);
				operand();
				setState(74);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 1, _ctx);
				while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
					if (_alt == 1) {
						{
							{
								setState(71);
								operation();
							}
						}
					}
					setState(76);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 1, _ctx);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationContext extends ParserRuleContext {
		public TerminalNode NEWLINE() {
			return getToken(SetqlGrammar.NEWLINE, 0);
		}

		public OperatorContext operator() {
			return getRuleContext(OperatorContext.class, 0);
		}

		public OperandContext operand() {
			return getRuleContext(OperandContext.class, 0);
		}

		public OperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_operation;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterOperation(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitOperation(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperationContext operation() throws RecognitionException {
		OperationContext _localctx = new OperationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(77);
				match(NEWLINE);
				setState(78);
				operator();
				setState(79);
				operand();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperandContext extends ParserRuleContext {
		public WrappedCriterionContext wrappedCriterion() {
			return getRuleContext(WrappedCriterionContext.class, 0);
		}

		public InlinePredicateContext inlinePredicate() {
			return getRuleContext(InlinePredicateContext.class, 0);
		}

		public OperandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_operand;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterOperand(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitOperand(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitOperand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperandContext operand() throws RecognitionException {
		OperandContext _localctx = new OperandContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_operand);
		try {
			setState(83);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
				case NEW_LINE_INDENT:
					enterOuterAlt(_localctx, 1);
				{
					setState(81);
					wrappedCriterion();
				}
				break;
				case INDENT:
					enterOuterAlt(_localctx, 2);
				{
					setState(82);
					inlinePredicate();
				}
				break;
				default:
					throw new NoViableAltException(this);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WrappedCriterionContext extends ParserRuleContext {
		public TerminalNode NEW_LINE_INDENT() {
			return getToken(SetqlGrammar.NEW_LINE_INDENT, 0);
		}

		public CriterionContext criterion() {
			return getRuleContext(CriterionContext.class, 0);
		}

		public TerminalNode DEDENT() {
			return getToken(SetqlGrammar.DEDENT, 0);
		}

		public TerminalNode NEWLINE() {
			return getToken(SetqlGrammar.NEWLINE, 0);
		}

		public WrappedCriterionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_wrappedCriterion;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterWrappedCriterion(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitWrappedCriterion(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitWrappedCriterion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WrappedCriterionContext wrappedCriterion() throws RecognitionException {
		WrappedCriterionContext _localctx = new WrappedCriterionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_wrappedCriterion);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(85);
				match(NEW_LINE_INDENT);
				setState(86);
				criterion();
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == NEWLINE) {
					{
						setState(87);
						match(NEWLINE);
					}
				}

				setState(90);
				match(DEDENT);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InlinePredicateContext extends ParserRuleContext {
		public TerminalNode INDENT() {
			return getToken(SetqlGrammar.INDENT, 0);
		}

		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class, 0);
		}

		public InlinePredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_inlinePredicate;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterInlinePredicate(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitInlinePredicate(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitInlinePredicate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InlinePredicateContext inlinePredicate() throws RecognitionException {
		InlinePredicateContext _localctx = new InlinePredicateContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_inlinePredicate);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(92);
				match(INDENT);
				setState(93);
				predicate();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateContext extends ParserRuleContext {
		public RestrictionContext restriction() {
			return getRuleContext(RestrictionContext.class, 0);
		}

		public ConstrainsContext constrains() {
			return getRuleContext(ConstrainsContext.class, 0);
		}

		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_predicate;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterPredicate(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitPredicate(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitPredicate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_predicate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(95);
				restriction();
				setState(97);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == COLON) {
					{
						setState(96);
						constrains();
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RestrictionContext extends ParserRuleContext {
		public FeatureContext feature() {
			return getRuleContext(FeatureContext.class, 0);
		}

		public TerminalNode LPAREN() {
			return getToken(SetqlGrammar.LPAREN, 0);
		}

		public TerminalNode RPAREN() {
			return getToken(SetqlGrammar.RPAREN, 0);
		}

		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}

		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class, i);
		}

		public List<TerminalNode> COMMA() {
			return getTokens(SetqlGrammar.COMMA);
		}

		public TerminalNode COMMA(int i) {
			return getToken(SetqlGrammar.COMMA, i);
		}

		public RestrictionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_restriction;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterRestriction(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitRestriction(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitRestriction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RestrictionContext restriction() throws RecognitionException {
		RestrictionContext _localctx = new RestrictionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_restriction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(99);
				feature();
				setState(112);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == LPAREN) {
					{
						setState(100);
						match(LPAREN);
						setState(109);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOT) | (1L << STAR) | (1L << GT) | (1L << GE) | (1L << LT) | (1L << LE) | (1L << EQ) | (1L << EXPRESSION_TOKEN) | (1L << NATURAL_VALUE) | (1L << NEGATIVE_VALUE) | (1L << DOUBLE_VALUE) | (1L << STRING) | (1L << IDENTIFIER))) != 0)) {
							{
								setState(101);
								argument();
								setState(106);
								_errHandler.sync(this);
								_la = _input.LA(1);
								while (_la == COMMA) {
									{
										{
											setState(102);
											match(COMMA);
											setState(103);
											argument();
										}
									}
									setState(108);
									_errHandler.sync(this);
									_la = _input.LA(1);
								}
							}
						}

						setState(111);
						match(RPAREN);
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentContext extends ParserRuleContext {
		public EnumerateContext enumerate() {
			return getRuleContext(EnumerateContext.class, 0);
		}

		public RangeContext range() {
			return getRuleContext(RangeContext.class, 0);
		}

		public NumberContext number() {
			return getRuleContext(NumberContext.class, 0);
		}

		public ComparatorContext comparator() {
			return getRuleContext(ComparatorContext.class, 0);
		}

		public TerminalNode STRING() {
			return getToken(SetqlGrammar.STRING, 0);
		}

		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class, 0);
		}

		public ArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_argument;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterArgument(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitArgument(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitArgument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentContext argument() throws RecognitionException {
		ArgumentContext _localctx = new ArgumentContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_argument);
		int _la;
		try {
			setState(122);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 9, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(114);
					enumerate();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(115);
					range();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					{
						setState(117);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << GT) | (1L << GE) | (1L << LT) | (1L << LE) | (1L << EQ))) != 0)) {
							{
								setState(116);
								comparator();
							}
						}

						setState(119);
						number();
					}
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(120);
					match(STRING);
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(121);
					expression();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumerateContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() {
			return getTokens(SetqlGrammar.IDENTIFIER);
		}

		public TerminalNode IDENTIFIER(int i) {
			return getToken(SetqlGrammar.IDENTIFIER, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(SetqlGrammar.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(SetqlGrammar.DOT, i);
		}

		public EnumerateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_enumerate;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterEnumerate(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitEnumerate(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitEnumerate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EnumerateContext enumerate() throws RecognitionException {
		EnumerateContext _localctx = new EnumerateContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_enumerate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(125);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						{
							setState(124);
							_la = _input.LA(1);
							if (!(_la == DOT || _la == IDENTIFIER)) {
								_errHandler.recoverInline(this);
							} else {
								if (_input.LA(1) == Token.EOF) matchedEOF = true;
								_errHandler.reportMatch(this);
								consume();
							}
						}
					}
					setState(127);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while (_la == DOT || _la == IDENTIFIER);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public List<TerminalNode> EXPRESSION_TOKEN() {
			return getTokens(SetqlGrammar.EXPRESSION_TOKEN);
		}

		public TerminalNode EXPRESSION_TOKEN(int i) {
			return getToken(SetqlGrammar.EXPRESSION_TOKEN, i);
		}

		public ComputeContext compute() {
			return getRuleContext(ComputeContext.class, 0);
		}

		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_expression;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(129);
				match(EXPRESSION_TOKEN);
				setState(130);
				compute(0);
				setState(131);
				match(EXPRESSION_TOKEN);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComputeContext extends ParserRuleContext {
		public ComputeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_compute;
		}

		public ComputeContext() {
		}

		public void copyFrom(ComputeContext ctx) {
			super.copyFrom(ctx);
		}
	}

	public static class BinaryExpressionContext extends ComputeContext {
		public ComputeContext left;
		public BinaryContext op;
		public ComputeContext right;

		public List<ComputeContext> compute() {
			return getRuleContexts(ComputeContext.class);
		}

		public ComputeContext compute(int i) {
			return getRuleContext(ComputeContext.class, i);
		}

		public BinaryContext binary() {
			return getRuleContext(BinaryContext.class, 0);
		}

		public BinaryExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterBinaryExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitBinaryExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitBinaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class DecimalExpressionContext extends ComputeContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class, 0);
		}

		public DecimalExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener)
				((SetqlGrammarListener) listener).enterDecimalExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitDecimalExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitDecimalExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class BoolExpressionContext extends ComputeContext {
		public BoolContext bool() {
			return getRuleContext(BoolContext.class, 0);
		}

		public BoolExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterBoolExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitBoolExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitBoolExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class IdentifierExpressionContext extends ComputeContext {
		public TerminalNode IDENTIFIER() {
			return getToken(SetqlGrammar.IDENTIFIER, 0);
		}

		public IdentifierExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener)
				((SetqlGrammarListener) listener).enterIdentifierExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener)
				((SetqlGrammarListener) listener).exitIdentifierExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitIdentifierExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class NotExpressionContext extends ComputeContext {
		public TerminalNode NOT() {
			return getToken(SetqlGrammar.NOT, 0);
		}

		public ComputeContext compute() {
			return getRuleContext(ComputeContext.class, 0);
		}

		public NotExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterNotExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitNotExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitNotExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ParenExpressionContext extends ComputeContext {
		public TerminalNode LPAREN() {
			return getToken(SetqlGrammar.LPAREN, 0);
		}

		public ComputeContext compute() {
			return getRuleContext(ComputeContext.class, 0);
		}

		public TerminalNode RPAREN() {
			return getToken(SetqlGrammar.RPAREN, 0);
		}

		public ParenExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterParenExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitParenExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitParenExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ComparatorExpressionContext extends ComputeContext {
		public ComputeContext left;
		public ComparatorContext op;
		public ComputeContext right;

		public List<ComputeContext> compute() {
			return getRuleContexts(ComputeContext.class);
		}

		public ComputeContext compute(int i) {
			return getRuleContext(ComputeContext.class, i);
		}

		public ComparatorContext comparator() {
			return getRuleContext(ComparatorContext.class, 0);
		}

		public ComparatorExpressionContext(ComputeContext ctx) {
			copyFrom(ctx);
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener)
				((SetqlGrammarListener) listener).enterComparatorExpression(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener)
				((SetqlGrammarListener) listener).exitComparatorExpression(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitComparatorExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComputeContext compute() throws RecognitionException {
		return compute(0);
	}

	private ComputeContext compute(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ComputeContext _localctx = new ComputeContext(_ctx, _parentState);
		ComputeContext _prevctx = _localctx;
		int _startState = 22;
		enterRecursionRule(_localctx, 22, RULE_compute, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(143);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case LPAREN: {
						_localctx = new ParenExpressionContext(_localctx);
						_ctx = _localctx;
						_prevctx = _localctx;

						setState(134);
						match(LPAREN);
						setState(135);
						compute(0);
						setState(136);
						match(RPAREN);
					}
					break;
					case NOT: {
						_localctx = new NotExpressionContext(_localctx);
						_ctx = _localctx;
						_prevctx = _localctx;
						setState(138);
						match(NOT);
						setState(139);
						compute(6);
					}
					break;
					case TRUE:
					case FALSE: {
						_localctx = new BoolExpressionContext(_localctx);
						_ctx = _localctx;
						_prevctx = _localctx;
						setState(140);
						bool();
					}
					break;
					case IDENTIFIER: {
						_localctx = new IdentifierExpressionContext(_localctx);
						_ctx = _localctx;
						_prevctx = _localctx;
						setState(141);
						match(IDENTIFIER);
					}
					break;
					case NATURAL_VALUE:
					case NEGATIVE_VALUE:
					case DOUBLE_VALUE: {
						_localctx = new DecimalExpressionContext(_localctx);
						_ctx = _localctx;
						_prevctx = _localctx;
						setState(142);
						number();
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
				_ctx.stop = _input.LT(-1);
				setState(155);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 13, _ctx);
				while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
					if (_alt == 1) {
						if (_parseListeners != null) triggerExitRuleEvent();
						_prevctx = _localctx;
						{
							setState(153);
							_errHandler.sync(this);
							switch (getInterpreter().adaptivePredict(_input, 12, _ctx)) {
								case 1: {
									_localctx = new ComparatorExpressionContext(new ComputeContext(_parentctx, _parentState));
									((ComparatorExpressionContext) _localctx).left = _prevctx;
									pushNewRecursionContext(_localctx, _startState, RULE_compute);
									setState(145);
									if (!(precpred(_ctx, 5)))
										throw new FailedPredicateException(this, "precpred(_ctx, 5)");
									setState(146);
									((ComparatorExpressionContext) _localctx).op = comparator();
									setState(147);
									((ComparatorExpressionContext) _localctx).right = compute(6);
								}
								break;
								case 2: {
									_localctx = new BinaryExpressionContext(new ComputeContext(_parentctx, _parentState));
									((BinaryExpressionContext) _localctx).left = _prevctx;
									pushNewRecursionContext(_localctx, _startState, RULE_compute);
									setState(149);
									if (!(precpred(_ctx, 4)))
										throw new FailedPredicateException(this, "precpred(_ctx, 4)");
									setState(150);
									((BinaryExpressionContext) _localctx).op = binary();
									setState(151);
									((BinaryExpressionContext) _localctx).right = compute(5);
								}
								break;
							}
						}
					}
					setState(157);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 13, _ctx);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ComparatorContext extends ParserRuleContext {
		public TerminalNode GT() {
			return getToken(SetqlGrammar.GT, 0);
		}

		public TerminalNode GE() {
			return getToken(SetqlGrammar.GE, 0);
		}

		public TerminalNode LT() {
			return getToken(SetqlGrammar.LT, 0);
		}

		public TerminalNode LE() {
			return getToken(SetqlGrammar.LE, 0);
		}

		public TerminalNode EQ() {
			return getToken(SetqlGrammar.EQ, 0);
		}

		public ComparatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_comparator;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterComparator(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitComparator(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitComparator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparatorContext comparator() throws RecognitionException {
		ComparatorContext _localctx = new ComparatorContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_comparator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(158);
				_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << GT) | (1L << GE) | (1L << LT) | (1L << LE) | (1L << EQ))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryContext extends ParserRuleContext {
		public TerminalNode AND() {
			return getToken(SetqlGrammar.AND, 0);
		}

		public TerminalNode OR() {
			return getToken(SetqlGrammar.OR, 0);
		}

		public BinaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_binary;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterBinary(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitBinary(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitBinary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryContext binary() throws RecognitionException {
		BinaryContext _localctx = new BinaryContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_binary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(160);
				_la = _input.LA(1);
				if (!(_la == AND || _la == OR)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public TerminalNode TRUE() {
			return getToken(SetqlGrammar.TRUE, 0);
		}

		public TerminalNode FALSE() {
			return getToken(SetqlGrammar.FALSE, 0);
		}

		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_bool;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterBool(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitBool(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitBool(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		BoolContext _localctx = new BoolContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_bool);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(162);
				_la = _input.LA(1);
				if (!(_la == TRUE || _la == FALSE)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public BinaryContext binary() {
			return getRuleContext(BinaryContext.class, 0);
		}

		public TerminalNode DASH() {
			return getToken(SetqlGrammar.DASH, 0);
		}

		public TerminalNode PERCENTAGE() {
			return getToken(SetqlGrammar.PERCENTAGE, 0);
		}

		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_operator;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterOperator(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitOperator(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_operator);
		try {
			setState(167);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
				case AND:
				case OR:
					enterOuterAlt(_localctx, 1);
				{
					setState(164);
					binary();
				}
				break;
				case DASH:
					enterOuterAlt(_localctx, 2);
				{
					setState(165);
					match(DASH);
				}
				break;
				case PERCENTAGE:
					enterOuterAlt(_localctx, 3);
				{
					setState(166);
					match(PERCENTAGE);
				}
				break;
				default:
					throw new NoViableAltException(this);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FeatureContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() {
			return getTokens(SetqlGrammar.IDENTIFIER);
		}

		public TerminalNode IDENTIFIER(int i) {
			return getToken(SetqlGrammar.IDENTIFIER, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(SetqlGrammar.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(SetqlGrammar.DOT, i);
		}

		public FeatureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_feature;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterFeature(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitFeature(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitFeature(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FeatureContext feature() throws RecognitionException {
		FeatureContext _localctx = new FeatureContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_feature);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(169);
				match(IDENTIFIER);
				setState(174);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == DOT) {
					{
						{
							setState(170);
							match(DOT);
							setState(171);
							match(IDENTIFIER);
						}
					}
					setState(176);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstrainsContext extends ParserRuleContext {
		public List<TerminalNode> COLON() {
			return getTokens(SetqlGrammar.COLON);
		}

		public TerminalNode COLON(int i) {
			return getToken(SetqlGrammar.COLON, i);
		}

		public ModifierContext modifier() {
			return getRuleContext(ModifierContext.class, 0);
		}

		public List<PeriodContext> period() {
			return getRuleContexts(PeriodContext.class);
		}

		public PeriodContext period(int i) {
			return getRuleContext(PeriodContext.class, i);
		}

		public List<FrequencyContext> frequency() {
			return getRuleContexts(FrequencyContext.class);
		}

		public FrequencyContext frequency(int i) {
			return getRuleContext(FrequencyContext.class, i);
		}

		public List<RecencyContext> recency() {
			return getRuleContexts(RecencyContext.class);
		}

		public RecencyContext recency(int i) {
			return getRuleContext(RecencyContext.class, i);
		}

		public ConstrainsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_constrains;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterConstrains(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitConstrains(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitConstrains(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstrainsContext constrains() throws RecognitionException {
		ConstrainsContext _localctx = new ConstrainsContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_constrains);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(177);
				match(COLON);
				setState(178);
				match(COLON);
				setState(180);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 16, _ctx)) {
					case 1: {
						setState(179);
						modifier();
					}
					break;
				}
				setState(187);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PERIOD) | (1L << FREQUENCY) | (1L << RECENCY))) != 0)) {
					{
						setState(185);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
							case PERIOD: {
								setState(182);
								period();
							}
							break;
							case FREQUENCY: {
								setState(183);
								frequency();
							}
							break;
							case RECENCY: {
								setState(184);
								recency();
							}
							break;
							default:
								throw new NoViableAltException(this);
						}
					}
					setState(189);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrequencyContext extends ParserRuleContext {
		public TerminalNode FREQUENCY() {
			return getToken(SetqlGrammar.FREQUENCY, 0);
		}

		public TerminalNode PERCENTAGE() {
			return getToken(SetqlGrammar.PERCENTAGE, 0);
		}

		public TerminalNode CONSECUTIVE() {
			return getToken(SetqlGrammar.CONSECUTIVE, 0);
		}

		public TerminalNode EQ() {
			return getToken(SetqlGrammar.EQ, 0);
		}

		public RangeContext range() {
			return getRuleContext(RangeContext.class, 0);
		}

		public ComparatorContext comparator() {
			return getRuleContext(ComparatorContext.class, 0);
		}

		public TerminalNode NATURAL_VALUE() {
			return getToken(SetqlGrammar.NATURAL_VALUE, 0);
		}

		public FrequencyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_frequency;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterFrequency(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitFrequency(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitFrequency(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrequencyContext frequency() throws RecognitionException {
		FrequencyContext _localctx = new FrequencyContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_frequency);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(190);
				match(FREQUENCY);
				setState(196);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 19, _ctx)) {
					case 1: {
						{
							setState(191);
							match(EQ);
							setState(192);
							range();
						}
					}
					break;
					case 2: {
						{
							setState(193);
							comparator();
							setState(194);
							match(NATURAL_VALUE);
						}
					}
					break;
				}
				setState(199);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == PERCENTAGE) {
					{
						setState(198);
						match(PERCENTAGE);
					}
				}

				setState(202);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == CONSECUTIVE) {
					{
						setState(201);
						match(CONSECUTIVE);
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecencyContext extends ParserRuleContext {
		public TerminalNode RECENCY() {
			return getToken(SetqlGrammar.RECENCY, 0);
		}

		public TerminalNode EQ() {
			return getToken(SetqlGrammar.EQ, 0);
		}

		public TerminalNode NATURAL_VALUE() {
			return getToken(SetqlGrammar.NATURAL_VALUE, 0);
		}

		public ScaleContext scale() {
			return getRuleContext(ScaleContext.class, 0);
		}

		public TerminalNode NEW() {
			return getToken(SetqlGrammar.NEW, 0);
		}

		public TerminalNode OLD() {
			return getToken(SetqlGrammar.OLD, 0);
		}

		public RecencyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_recency;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterRecency(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitRecency(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitRecency(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RecencyContext recency() throws RecognitionException {
		RecencyContext _localctx = new RecencyContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_recency);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(204);
				match(RECENCY);
				setState(205);
				match(EQ);
				setState(206);
				match(NATURAL_VALUE);
				setState(207);
				scale();
				setState(209);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == NEW || _la == OLD) {
					{
						setState(208);
						_la = _input.LA(1);
						if (!(_la == NEW || _la == OLD)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PeriodContext extends ParserRuleContext {
		public TerminalNode PERIOD() {
			return getToken(SetqlGrammar.PERIOD, 0);
		}

		public TerminalNode EQ() {
			return getToken(SetqlGrammar.EQ, 0);
		}

		public DateRangeContext dateRange() {
			return getRuleContext(DateRangeContext.class, 0);
		}

		public TerminalNode NATURAL_VALUE() {
			return getToken(SetqlGrammar.NATURAL_VALUE, 0);
		}

		public ScaleContext scale() {
			return getRuleContext(ScaleContext.class, 0);
		}

		public PeriodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_period;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterPeriod(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitPeriod(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitPeriod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PeriodContext period() throws RecognitionException {
		PeriodContext _localctx = new PeriodContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_period);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(211);
				match(PERIOD);
				setState(212);
				match(EQ);
				setState(216);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 23, _ctx)) {
					case 1: {
						{
							setState(213);
							match(NATURAL_VALUE);
							setState(214);
							scale();
						}
					}
					break;
					case 2: {
						setState(215);
						dateRange();
					}
					break;
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifierContext extends ParserRuleContext {
		public TerminalNode COMMONS() {
			return getToken(SetqlGrammar.COMMONS, 0);
		}

		public TerminalNode UNCOMMONS() {
			return getToken(SetqlGrammar.UNCOMMONS, 0);
		}

		public ModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_modifier;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterModifier(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitModifier(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitModifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModifierContext modifier() throws RecognitionException {
		ModifierContext _localctx = new ModifierContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_modifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(219);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == COMMONS || _la == UNCOMMONS) {
					{
						setState(218);
						_la = _input.LA(1);
						if (!(_la == COMMONS || _la == UNCOMMONS)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangeContext extends ParserRuleContext {
		public List<RangeValueContext> rangeValue() {
			return getRuleContexts(RangeValueContext.class);
		}

		public RangeValueContext rangeValue(int i) {
			return getRuleContext(RangeValueContext.class, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(SetqlGrammar.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(SetqlGrammar.DOT, i);
		}

		public RangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_range;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterRange(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitRange(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitRange(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RangeContext range() throws RecognitionException {
		RangeContext _localctx = new RangeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_range);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(221);
				rangeValue();
				setState(222);
				match(DOT);
				setState(223);
				match(DOT);
				setState(224);
				rangeValue();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateRangeContext extends ParserRuleContext {
		public List<DateValueContext> dateValue() {
			return getRuleContexts(DateValueContext.class);
		}

		public DateValueContext dateValue(int i) {
			return getRuleContext(DateValueContext.class, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(SetqlGrammar.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(SetqlGrammar.DOT, i);
		}

		public DateRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_dateRange;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterDateRange(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitDateRange(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitDateRange(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateRangeContext dateRange() throws RecognitionException {
		DateRangeContext _localctx = new DateRangeContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_dateRange);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(226);
				dateValue();
				setState(227);
				match(DOT);
				setState(228);
				match(DOT);
				setState(229);
				dateValue();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateValueContext extends ParserRuleContext {
		public TerminalNode NATURAL_VALUE() {
			return getToken(SetqlGrammar.NATURAL_VALUE, 0);
		}

		public List<TerminalNode> NEGATIVE_VALUE() {
			return getTokens(SetqlGrammar.NEGATIVE_VALUE);
		}

		public TerminalNode NEGATIVE_VALUE(int i) {
			return getToken(SetqlGrammar.NEGATIVE_VALUE, i);
		}

		public DateValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_dateValue;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterDateValue(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitDateValue(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitDateValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateValueContext dateValue() throws RecognitionException {
		DateValueContext _localctx = new DateValueContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_dateValue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(231);
				match(NATURAL_VALUE);
				setState(233);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 25, _ctx)) {
					case 1: {
						setState(232);
						match(NEGATIVE_VALUE);
					}
					break;
				}
				setState(236);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == NEGATIVE_VALUE) {
					{
						setState(235);
						match(NEGATIVE_VALUE);
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangeValueContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class, 0);
		}

		public TerminalNode STAR() {
			return getToken(SetqlGrammar.STAR, 0);
		}

		public RangeValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_rangeValue;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterRangeValue(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitRangeValue(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitRangeValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RangeValueContext rangeValue() throws RecognitionException {
		RangeValueContext _localctx = new RangeValueContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_rangeValue);
		try {
			setState(240);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
				case NATURAL_VALUE:
				case NEGATIVE_VALUE:
				case DOUBLE_VALUE:
					enterOuterAlt(_localctx, 1);
				{
					setState(238);
					number();
				}
				break;
				case STAR:
					enterOuterAlt(_localctx, 2);
				{
					setState(239);
					match(STAR);
				}
				break;
				default:
					throw new NoViableAltException(this);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public DoubleValueContext doubleValue() {
			return getRuleContext(DoubleValueContext.class, 0);
		}

		public IntegerValueContext integerValue() {
			return getRuleContext(IntegerValueContext.class, 0);
		}

		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_number;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterNumber(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitNumber(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_number);
		try {
			setState(244);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 28, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(242);
					doubleValue();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(243);
					integerValue();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScaleContext extends ParserRuleContext {
		public TerminalNode YEAR() {
			return getToken(SetqlGrammar.YEAR, 0);
		}

		public TerminalNode MONTH() {
			return getToken(SetqlGrammar.MONTH, 0);
		}

		public TerminalNode DAY() {
			return getToken(SetqlGrammar.DAY, 0);
		}

		public TerminalNode HOUR() {
			return getToken(SetqlGrammar.HOUR, 0);
		}

		public ScaleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_scale;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterScale(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitScale(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitScale(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ScaleContext scale() throws RecognitionException {
		ScaleContext _localctx = new ScaleContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_scale);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(246);
				_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << YEAR) | (1L << MONTH) | (1L << DAY) | (1L << HOUR))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerValueContext extends ParserRuleContext {
		public TerminalNode NATURAL_VALUE() {
			return getToken(SetqlGrammar.NATURAL_VALUE, 0);
		}

		public TerminalNode NEGATIVE_VALUE() {
			return getToken(SetqlGrammar.NEGATIVE_VALUE, 0);
		}

		public IntegerValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_integerValue;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterIntegerValue(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitIntegerValue(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitIntegerValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerValueContext integerValue() throws RecognitionException {
		IntegerValueContext _localctx = new IntegerValueContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_integerValue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(248);
				_la = _input.LA(1);
				if (!(_la == NATURAL_VALUE || _la == NEGATIVE_VALUE)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoubleValueContext extends ParserRuleContext {
		public TerminalNode NATURAL_VALUE() {
			return getToken(SetqlGrammar.NATURAL_VALUE, 0);
		}

		public TerminalNode NEGATIVE_VALUE() {
			return getToken(SetqlGrammar.NEGATIVE_VALUE, 0);
		}

		public TerminalNode DOUBLE_VALUE() {
			return getToken(SetqlGrammar.DOUBLE_VALUE, 0);
		}

		public DoubleValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		@Override
		public int getRuleIndex() {
			return RULE_doubleValue;
		}

		@Override
		public void enterRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).enterDoubleValue(this);
		}

		@Override
		public void exitRule(ParseTreeListener listener) {
			if (listener instanceof SetqlGrammarListener) ((SetqlGrammarListener) listener).exitDoubleValue(this);
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof SetqlGrammarVisitor)
				return ((SetqlGrammarVisitor<? extends T>) visitor).visitDoubleValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DoubleValueContext doubleValue() throws RecognitionException {
		DoubleValueContext _localctx = new DoubleValueContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_doubleValue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(250);
				_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NATURAL_VALUE) | (1L << NEGATIVE_VALUE) | (1L << DOUBLE_VALUE))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
			case 11:
				return compute_sempred((ComputeContext) _localctx, predIndex);
		}
		return true;
	}

	private boolean compute_sempred(ComputeContext _localctx, int predIndex) {
		switch (predIndex) {
			case 0:
				return precpred(_ctx, 5);
			case 1:
				return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
			"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\65\u00ff\4\2\t\2" +
					"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13" +
					"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22" +
					"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31" +
					"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\3\2\3\2\7" +
					"\2A\n\2\f\2\16\2D\13\2\3\2\3\2\3\3\3\3\3\3\7\3K\n\3\f\3\16\3N\13\3\3\4" +
					"\3\4\3\4\3\4\3\5\3\5\5\5V\n\5\3\6\3\6\3\6\5\6[\n\6\3\6\3\6\3\7\3\7\3\7" +
					"\3\b\3\b\5\bd\n\b\3\t\3\t\3\t\3\t\3\t\7\tk\n\t\f\t\16\tn\13\t\5\tp\n\t" +
					"\3\t\5\ts\n\t\3\n\3\n\3\n\5\nx\n\n\3\n\3\n\3\n\5\n}\n\n\3\13\6\13\u0080" +
					"\n\13\r\13\16\13\u0081\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r" +
					"\3\r\3\r\5\r\u0092\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u009c\n\r\f" +
					"\r\16\r\u009f\13\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\21\5\21\u00aa" +
					"\n\21\3\22\3\22\3\22\7\22\u00af\n\22\f\22\16\22\u00b2\13\22\3\23\3\23" +
					"\3\23\5\23\u00b7\n\23\3\23\3\23\3\23\7\23\u00bc\n\23\f\23\16\23\u00bf" +
					"\13\23\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00c7\n\24\3\24\5\24\u00ca\n" +
					"\24\3\24\5\24\u00cd\n\24\3\25\3\25\3\25\3\25\3\25\5\25\u00d4\n\25\3\26" +
					"\3\26\3\26\3\26\3\26\5\26\u00db\n\26\3\27\5\27\u00de\n\27\3\30\3\30\3" +
					"\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32\5\32\u00ec\n\32\3\32" +
					"\5\32\u00ef\n\32\3\33\3\33\5\33\u00f3\n\33\3\34\3\34\5\34\u00f7\n\34\3" +
					"\35\3\35\3\36\3\36\3\37\3\37\3\37\2\3\30 \2\4\6\b\n\f\16\20\22\24\26\30" +
					"\32\34\36 \"$&(*,.\60\62\64\668:<\2\13\4\2\22\22((\3\2\36\"\3\2\27\30" +
					"\3\2\33\34\3\2\24\25\3\2\7\b\3\2\t\f\3\2$%\3\2$&\2\u0105\2>\3\2\2\2\4" +
					"G\3\2\2\2\6O\3\2\2\2\bU\3\2\2\2\nW\3\2\2\2\f^\3\2\2\2\16a\3\2\2\2\20e" +
					"\3\2\2\2\22|\3\2\2\2\24\177\3\2\2\2\26\u0083\3\2\2\2\30\u0091\3\2\2\2" +
					"\32\u00a0\3\2\2\2\34\u00a2\3\2\2\2\36\u00a4\3\2\2\2 \u00a9\3\2\2\2\"\u00ab" +
					"\3\2\2\2$\u00b3\3\2\2\2&\u00c0\3\2\2\2(\u00ce\3\2\2\2*\u00d5\3\2\2\2," +
					"\u00dd\3\2\2\2.\u00df\3\2\2\2\60\u00e4\3\2\2\2\62\u00e9\3\2\2\2\64\u00f2" +
					"\3\2\2\2\66\u00f6\3\2\2\28\u00f8\3\2\2\2:\u00fa\3\2\2\2<\u00fc\3\2\2\2" +
					">B\5\4\3\2?A\7-\2\2@?\3\2\2\2AD\3\2\2\2B@\3\2\2\2BC\3\2\2\2CE\3\2\2\2" +
					"DB\3\2\2\2EF\7\2\2\3F\3\3\2\2\2GH\7\23\2\2HL\5\b\5\2IK\5\6\4\2JI\3\2\2" +
					"\2KN\3\2\2\2LJ\3\2\2\2LM\3\2\2\2M\5\3\2\2\2NL\3\2\2\2OP\7-\2\2PQ\5 \21" +
					"\2QR\5\b\5\2R\7\3\2\2\2SV\5\n\6\2TV\5\f\7\2US\3\2\2\2UT\3\2\2\2V\t\3\2" +
					"\2\2WX\7\61\2\2XZ\5\4\3\2Y[\7-\2\2ZY\3\2\2\2Z[\3\2\2\2[\\\3\2\2\2\\]\7" +
					"\62\2\2]\13\3\2\2\2^_\7,\2\2_`\5\16\b\2`\r\3\2\2\2ac\5\20\t\2bd\5$\23" +
					"\2cb\3\2\2\2cd\3\2\2\2d\17\3\2\2\2er\5\"\22\2fo\7\r\2\2gl\5\22\n\2hi\7" +
					"\21\2\2ik\5\22\n\2jh\3\2\2\2kn\3\2\2\2lj\3\2\2\2lm\3\2\2\2mp\3\2\2\2n" +
					"l\3\2\2\2og\3\2\2\2op\3\2\2\2pq\3\2\2\2qs\7\16\2\2rf\3\2\2\2rs\3\2\2\2" +
					"s\21\3\2\2\2t}\5\24\13\2u}\5.\30\2vx\5\32\16\2wv\3\2\2\2wx\3\2\2\2xy\3" +
					"\2\2\2y}\5\66\34\2z}\7\'\2\2{}\5\26\f\2|t\3\2\2\2|u\3\2\2\2|w\3\2\2\2" +
					"|z\3\2\2\2|{\3\2\2\2}\23\3\2\2\2~\u0080\t\2\2\2\177~\3\2\2\2\u0080\u0081" +
					"\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082\3\2\2\2\u0082\25\3\2\2\2\u0083" +
					"\u0084\7#\2\2\u0084\u0085\5\30\r\2\u0085\u0086\7#\2\2\u0086\27\3\2\2\2" +
					"\u0087\u0088\b\r\1\2\u0088\u0089\7\r\2\2\u0089\u008a\5\30\r\2\u008a\u008b" +
					"\7\16\2\2\u008b\u0092\3\2\2\2\u008c\u008d\7\35\2\2\u008d\u0092\5\30\r" +
					"\b\u008e\u0092\5\36\20\2\u008f\u0092\7(\2\2\u0090\u0092\5\66\34\2\u0091" +
					"\u0087\3\2\2\2\u0091\u008c\3\2\2\2\u0091\u008e\3\2\2\2\u0091\u008f\3\2" +
					"\2\2\u0091\u0090\3\2\2\2\u0092\u009d\3\2\2\2\u0093\u0094\f\7\2\2\u0094" +
					"\u0095\5\32\16\2\u0095\u0096\5\30\r\b\u0096\u009c\3\2\2\2\u0097\u0098" +
					"\f\6\2\2\u0098\u0099\5\34\17\2\u0099\u009a\5\30\r\7\u009a\u009c\3\2\2" +
					"\2\u009b\u0093\3\2\2\2\u009b\u0097\3\2\2\2\u009c\u009f\3\2\2\2\u009d\u009b" +
					"\3\2\2\2\u009d\u009e\3\2\2\2\u009e\31\3\2\2\2\u009f\u009d\3\2\2\2\u00a0" +
					"\u00a1\t\3\2\2\u00a1\33\3\2\2\2\u00a2\u00a3\t\4\2\2\u00a3\35\3\2\2\2\u00a4" +
					"\u00a5\t\5\2\2\u00a5\37\3\2\2\2\u00a6\u00aa\5\34\17\2\u00a7\u00aa\7\26" +
					"\2\2\u00a8\u00aa\7\32\2\2\u00a9\u00a6\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9" +
					"\u00a8\3\2\2\2\u00aa!\3\2\2\2\u00ab\u00b0\7(\2\2\u00ac\u00ad\7\22\2\2" +
					"\u00ad\u00af\7(\2\2\u00ae\u00ac\3\2\2\2\u00af\u00b2\3\2\2\2\u00b0\u00ae" +
					"\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1#\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b3" +
					"\u00b4\7\20\2\2\u00b4\u00b6\7\20\2\2\u00b5\u00b7\5,\27\2\u00b6\u00b5\3" +
					"\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00bd\3\2\2\2\u00b8\u00bc\5*\26\2\u00b9" +
					"\u00bc\5&\24\2\u00ba\u00bc\5(\25\2\u00bb\u00b8\3\2\2\2\u00bb\u00b9\3\2" +
					"\2\2\u00bb\u00ba\3\2\2\2\u00bc\u00bf\3\2\2\2\u00bd\u00bb\3\2\2\2\u00bd" +
					"\u00be\3\2\2\2\u00be%\3\2\2\2\u00bf\u00bd\3\2\2\2\u00c0\u00c6\7\4\2\2" +
					"\u00c1\u00c2\7\"\2\2\u00c2\u00c7\5.\30\2\u00c3\u00c4\5\32\16\2\u00c4\u00c5" +
					"\7$\2\2\u00c5\u00c7\3\2\2\2\u00c6\u00c1\3\2\2\2\u00c6\u00c3\3\2\2\2\u00c7" +
					"\u00c9\3\2\2\2\u00c8\u00ca\7\32\2\2\u00c9\u00c8\3\2\2\2\u00c9\u00ca\3" +
					"\2\2\2\u00ca\u00cc\3\2\2\2\u00cb\u00cd\7\6\2\2\u00cc\u00cb\3\2\2\2\u00cc" +
					"\u00cd\3\2\2\2\u00cd\'\3\2\2\2\u00ce\u00cf\7\5\2\2\u00cf\u00d0\7\"\2\2" +
					"\u00d0\u00d1\7$\2\2\u00d1\u00d3\58\35\2\u00d2\u00d4\t\6\2\2\u00d3\u00d2" +
					"\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4)\3\2\2\2\u00d5\u00d6\7\3\2\2\u00d6" +
					"\u00da\7\"\2\2\u00d7\u00d8\7$\2\2\u00d8\u00db\58\35\2\u00d9\u00db\5\60" +
					"\31\2\u00da\u00d7\3\2\2\2\u00da\u00d9\3\2\2\2\u00db+\3\2\2\2\u00dc\u00de" +
					"\t\7\2\2\u00dd\u00dc\3\2\2\2\u00dd\u00de\3\2\2\2\u00de-\3\2\2\2\u00df" +
					"\u00e0\5\64\33\2\u00e0\u00e1\7\22\2\2\u00e1\u00e2\7\22\2\2\u00e2\u00e3" +
					"\5\64\33\2\u00e3/\3\2\2\2\u00e4\u00e5\5\62\32\2\u00e5\u00e6\7\22\2\2\u00e6" +
					"\u00e7\7\22\2\2\u00e7\u00e8\5\62\32\2\u00e8\61\3\2\2\2\u00e9\u00eb\7$" +
					"\2\2\u00ea\u00ec\7%\2\2\u00eb\u00ea\3\2\2\2\u00eb\u00ec\3\2\2\2\u00ec" +
					"\u00ee\3\2\2\2\u00ed\u00ef\7%\2\2\u00ee\u00ed\3\2\2\2\u00ee\u00ef\3\2" +
					"\2\2\u00ef\63\3\2\2\2\u00f0\u00f3\5\66\34\2\u00f1\u00f3\7\23\2\2\u00f2" +
					"\u00f0\3\2\2\2\u00f2\u00f1\3\2\2\2\u00f3\65\3\2\2\2\u00f4\u00f7\5<\37" +
					"\2\u00f5\u00f7\5:\36\2\u00f6\u00f4\3\2\2\2\u00f6\u00f5\3\2\2\2\u00f7\67" +
					"\3\2\2\2\u00f8\u00f9\t\b\2\2\u00f99\3\2\2\2\u00fa\u00fb\t\t\2\2\u00fb" +
					";\3\2\2\2\u00fc\u00fd\t\n\2\2\u00fd=\3\2\2\2\37BLUZclorw|\u0081\u0091" +
					"\u009b\u009d\u00a9\u00b0\u00b6\u00bb\u00bd\u00c6\u00c9\u00cc\u00d3\u00da" +
					"\u00dd\u00eb\u00ee\u00f2\u00f6";
	public static final ATN _ATN =
			new ATNDeserializer().deserialize(_serializedATN.toCharArray());

	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}