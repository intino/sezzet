package io.intino.sezzet.setql;

import io.intino.magritte.framework.utils.UTF8Control;

import java.text.MessageFormat;
import java.util.*;

public class MessageProvider {

	private static final String BUNDLE = "messages";
	private static Map<Locale, PropertyResourceBundle> messages = new HashMap<>();

	private MessageProvider() {
	}

	public static String message(Locale locale, String key, Object... params) {
		try {
			if (!messages.containsKey(locale))
				messages.put(locale, (PropertyResourceBundle) PropertyResourceBundle.getBundle(BUNDLE, locale, new UTF8Control()));
			if (!messages.get(locale).containsKey(key)) return "";
			return MessageFormat.format(messages.get(locale).getString(key), params);
		} catch (MissingResourceException e) {
			return key;
		}
	}

}
