package io.intino.sezzet.editor;

import io.intino.alexandria.core.Box;
import io.intino.sezzet.editor.box.EditorBox;
import io.intino.tara.magritte.Graph;
import io.intino.tara.magritte.stores.FileSystemStore;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		Graph graph = new Graph(new FileSystemStore(new File("./store"))).loadStashes("Sezzet");
		Box box = new EditorBox(args).put(graph).open();
		Runtime.getRuntime().addShutdownHook(new Thread(box::close));
	}

}